package com.herd.divineherd_back.unitTest.service;

import com.herd.divineherd_back.entity.*;
import com.herd.divineherd_back.enums.FoodType;
import com.herd.divineherd_back.repository.BullRepository;
import com.herd.divineherd_back.service.BullService;
import com.herd.divineherd_back.service.implement.BullServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class BullServiceTest {

    private final BullRepository bullRepositoryMock;

    private final BullService bullServiceInstance;

    public BullServiceTest() {
        this.bullRepositoryMock = Mockito.mock(BullRepository.class);
        this.bullServiceInstance = new BullServiceImpl(bullRepositoryMock);
    }

    @Before
    public void setUp() throws ParseException {
        Race race = new Race(1L, "Race Test", "description Test", new ArrayList<>());
        Farm farm = new Farm(1L, "123456789", "12345678912345", "3 rue de la promenade","64000","Pau", new ArrayList<>());
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date dateOfBirth = format.parse("2023-11-02");
        Bull bull = new Bull(null, "bull Test", "white", dateOfBirth, null,true,"1234", "5678941234",List.of(race),farm);
        Bull bull1Saved = new Bull(1L, "bull Test", "white", dateOfBirth, null,true,"1234", "5678941234",List.of(race),farm);

        // mock PotionRepository findAll
        Mockito.when(bullRepositoryMock.findAll()).thenReturn(List.of(bull1Saved));

        // mock PotionRepository save
        Mockito.when(bullRepositoryMock.save(bull)).thenReturn(bull1Saved);

    }

    @Test
    public void whenGetAll_thenReturnListOfBull() {
        List<Bull> bull = bullServiceInstance.readBull();

        assertEquals(1, bull.size());
    }

    @Test
    public void whenCreate_thenReturnNewBull() throws ParseException {
        Race race = new Race(1L, "Race Test", "description Test", new ArrayList<>());
        Farm farm = new Farm(1L, "123456789", "12345678912345", "3 rue de la promenade","64000","Pau", new ArrayList<>());
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date dateOfBirth = format.parse("2023-11-02");
        Bull bull = new Bull(null, "Bull Test", "Bull Test",dateOfBirth, null, true, "1324", "12345434", List.of(race), farm);

        Bull bullCreated = bullServiceInstance.createBull(bull);

        assertNotNull(bullCreated);
        assertNotNull(bullCreated.getId());
    }



}
