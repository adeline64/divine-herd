package com.herd.divineherd_back.unitTest.service;

import com.herd.divineherd_back.entity.*;
import com.herd.divineherd_back.enums.CalfStatus;
import com.herd.divineherd_back.repository.CalfRepository;
import com.herd.divineherd_back.service.CalfService;
import com.herd.divineherd_back.service.implement.CalfServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class CalfServiceTest {

    private final CalfRepository calfRepositoryMock;

    private final CalfService calfServiceInstance;

    public CalfServiceTest() {
        this.calfRepositoryMock = Mockito.mock(CalfRepository.class);
        this.calfServiceInstance = new CalfServiceImpl(calfRepositoryMock);
    }

    @Before
    public void setUp() throws ParseException {

        Race race = new Race(1L, "Race Test", "description Test", new ArrayList<>());
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date dateOfBirth = format.parse("2022-11-02");
        Farm farm = new Farm(1L, "123456789", "12345678912345", "3 rue de la promenade","64000","Pau", new ArrayList<>());
        Cow cow = new Cow(1L, "test Cow", "test Color", dateOfBirth, null, false, "4356", "1324564356", List.of(race),farm );
        dateOfBirth = format.parse("2023-11-02");
        Calf calf = new Calf(null, "bull Test", "white", dateOfBirth, null,true,"1234", "5678941234", List.of(race),farm, CalfStatus.CALVES,cow);
        Calf calfSaved = new Calf(1L, "bull Test", "white", dateOfBirth, null,true,"1234", "5678941234", List.of(race),farm, CalfStatus.CALVES,cow);

        // mock PotionRepository findAll
        Mockito.when(calfRepositoryMock.findAll()).thenReturn(List.of(calfSaved));

        // mock PotionRepository save
        Mockito.when(calfRepositoryMock.save(calf)).thenReturn(calfSaved);

    }

    @Test
    public void whenGetAll_thenReturnListOfCalf() {
        List<Calf> calf = calfServiceInstance.readCalf();

        assertEquals(1, calf.size());
    }

    @Test
    public void whenCreate_thenReturnNewBull() throws ParseException {

        Race race = new Race(1L, "Race Test", "description Test", new ArrayList<>());
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date dateOfBirth = format.parse("2022-11-02");
        Farm farm = new Farm(1L, "123456789", "12345678912345", "3 rue de la promenade","64000","Pau", new ArrayList<>());
        Cow cow = new Cow(1L, "test Cow", "test Color", dateOfBirth, null, false, "4356", "1324564356", List.of(race),farm );
        dateOfBirth = format.parse("2023-11-02");
        Calf calf = new Calf(null, "bull Test", "white", dateOfBirth, null,true,"1234", "5678941234", List.of(race),farm, CalfStatus.CALVES,cow);

        Calf calfCreated = calfServiceInstance.createCalf(calf);

        assertNotNull(calfCreated);
        assertNotNull(calfCreated.getId());
    }

}
