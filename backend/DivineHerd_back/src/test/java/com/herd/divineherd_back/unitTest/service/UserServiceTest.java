package com.herd.divineherd_back.unitTest.service;


import com.herd.divineherd_back.entity.Farm;
import com.herd.divineherd_back.entity.Role;
import com.herd.divineherd_back.entity.User;
import com.herd.divineherd_back.repository.RoleRepository;
import com.herd.divineherd_back.repository.UserRepository;
import com.herd.divineherd_back.service.RoleService;
import com.herd.divineherd_back.service.UserService;
import com.herd.divineherd_back.service.implement.RoleServiceImpl;
import com.herd.divineherd_back.service.implement.UserServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class UserServiceTest {

    private final UserRepository userRepositoryMock;
    private final RoleRepository roleRepositoryMock;
    private final UserService userServiceInstance;
    private final RoleService roleServiceInstance;

    public UserServiceTest() {
        this.userRepositoryMock = Mockito.mock(UserRepository.class);
        this.roleRepositoryMock = Mockito.mock(RoleRepository.class);
        this.userServiceInstance = new UserServiceImpl(userRepositoryMock, roleRepositoryMock);
        this.roleServiceInstance = new RoleServiceImpl(roleRepositoryMock);
    }

    @Before
    public void setUp() throws ParseException {
        Role role = new Role(1L, "Role Test", new ArrayList<>());
        Farm farm = new Farm(1L, "123456789", "12345678912345", "3 rue de la promenade","64000","Pau", new ArrayList<>());
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date dateOfBirth = format.parse("2023-11-02");
        User user = new User(null, "name test", "user Test", dateOfBirth, "password", "0989787677","toto@to.te","3 rue des cedres", "64000", "test",List.of(role),farm);
        User userSaved = new User(1L,"name test", "user Test", dateOfBirth, "password", "0989787677","toto@to.te","3 rue des cedres", "64000", "test",List.of(role),farm);

        // mock RoleRepository findByName
        Role userRole = new Role(1L, "USER", new ArrayList<>());
        Mockito.when(roleRepositoryMock.findByName("USER")).thenReturn(userRole);

        // mock UserRepository findAll
        Mockito.when(userRepositoryMock.findAll()).thenReturn(List.of(userSaved));

        // mock UserRepository save
        Mockito.when(userRepositoryMock.save(user)).thenReturn(userSaved);

    }

    @Test
    public void whenGetAll_thenReturnListOfUser() {
        List<User> user = userServiceInstance.readUser();

        assertEquals(1, user.size());
    }
}
