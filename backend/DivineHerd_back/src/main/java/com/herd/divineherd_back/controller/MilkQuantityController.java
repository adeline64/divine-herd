package com.herd.divineherd_back.controller;

import com.herd.divineherd_back.entity.Cow;
import com.herd.divineherd_back.entity.MilkQuantity;
import com.herd.divineherd_back.service.CowService;
import com.herd.divineherd_back.service.MilkQuantityService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api/milkQuantities")
public class MilkQuantityController {

    private MilkQuantityService milkQuantityService;

    private CowService cowService;

    public MilkQuantityController(MilkQuantityService milkQuantityService, CowService cowService) {
        this.milkQuantityService = milkQuantityService;
        this.cowService = cowService;
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @GetMapping()
    public Iterable<MilkQuantity> readMilkQuantity() {
        return milkQuantityService.readMilkQuantity();
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @GetMapping("/{id}")
    public Optional<MilkQuantity> readOneMilkQuantity(@PathVariable Long id) {
        return milkQuantityService.readOneMilkQuantity(id);
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @PostMapping()
    public MilkQuantity createMilkQuantity(@RequestBody MilkQuantity milkQuantity) {
        return milkQuantityService.createMilkQuantity(milkQuantity);
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @DeleteMapping("/{id}")
    public void deleteMilkQuantity(@PathVariable Long id) {
        milkQuantityService.deleteMilkQuantity(id);
    }

    @PutMapping("/{id}")
    public MilkQuantity updateMilkQuantity(@PathVariable Long id, @RequestBody MilkQuantity milkQuantity) {
        return milkQuantityService.updateMilkQuantity(id, milkQuantity);
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @PutMapping("/{idMilkQuantity}/cows/{idCow}")
    private MilkQuantity milkQuantityCow(@PathVariable("idMilkQuantity") final Long idMilkQuantity, @PathVariable("idCow") final Long idCow) {
        Optional<MilkQuantity> milkQuantityOptional = milkQuantityService.readOneMilkQuantity(idMilkQuantity);
        Optional <Cow>cowOptional = cowService.readOneCow(idCow);

        if (cowOptional.isPresent() && milkQuantityOptional.isPresent()){
            Cow cow = cowOptional.get();
            MilkQuantity milkQuantity = milkQuantityOptional.get();
            milkQuantity.setCow(cow);
            cow.getMilkQuantityList().add(milkQuantity);
            return milkQuantityService.updateMilkQuantity(idMilkQuantity, milkQuantity);
        } else {
            return null;
        }
    }
}
