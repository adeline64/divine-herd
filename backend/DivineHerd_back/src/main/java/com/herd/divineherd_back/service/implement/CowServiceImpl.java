package com.herd.divineherd_back.service.implement;

import com.herd.divineherd_back.entity.Cow;
import com.herd.divineherd_back.repository.CowRepository;
import com.herd.divineherd_back.service.CowService;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CowServiceImpl implements CowService {

    private CowRepository cowRepository;

    public CowServiceImpl(CowRepository cowRepository) {
        this.cowRepository = cowRepository;
    }

    @Override
    public Iterable<Cow> readCow() {
        return cowRepository.findAll();
    }

    @Override
    public Optional<Cow> readOneCow(Long id) {
        return cowRepository.findById(id);
    }

    @Override
    public Cow createCow(Cow cow) {
        return cowRepository.save(cow);
    }

    @Override
    public void deleteCow(Long id) {
        cowRepository.deleteById(id);
    }

    @Override
    public Cow updateCow(Long id, Cow cow) {
        var c = cowRepository.findById(id);
        if (c.isPresent()){
            var courentCow = c.get();
            courentCow.setNumberOfCalfBorn(cow.getNumberOfCalfBorn());
            courentCow.setNumberOfDeadCalf(cow.getNumberOfDeadCalf());
            courentCow.setName(cow.getName());
            courentCow.setColor(cow.getColor());
            courentCow.setDateOfBirth(cow.getDateOfBirth());
            courentCow.setDateOfDeath(cow.getDateOfDeath());
            courentCow.setHorn(cow.getHorn());
            courentCow.setNumberJob(cow.getNumberJob());
            courentCow.setNumberPaper(cow.getNumberPaper());
            courentCow.setFarm(cow.getFarm());
            courentCow.setRaceList(cow.getRaceList());
            courentCow.setCattleVaccineList(cow.getCattleVaccineList());
            courentCow.setFoodList(cow.getFoodList());
            courentCow.setMilkQualityList(cow.getMilkQualityList());
            courentCow.setMilkQuantityList(cow.getMilkQuantityList());
            return cowRepository.save(courentCow);
        }else {
            return null;
        }
    }
}
