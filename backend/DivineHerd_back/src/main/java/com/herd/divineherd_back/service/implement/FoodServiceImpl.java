package com.herd.divineherd_back.service.implement;

import com.herd.divineherd_back.entity.Food;
import com.herd.divineherd_back.repository.FoodRepository;
import com.herd.divineherd_back.service.FoodService;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class FoodServiceImpl implements FoodService {

    private FoodRepository foodRepository;

    public FoodServiceImpl(FoodRepository foodRepository) {
        this.foodRepository = foodRepository;
    }

    @Override
    public Iterable<Food> readFood() {
        return foodRepository.findAll();
    }

    @Override
    public Optional<Food> readOneFood(Long id) {
        return foodRepository.findById(id);
    }

    @Override
    public Food createFood(Food food) {
        return foodRepository.save(food);
    }

    @Override
    public void deleteFood(Long id) {
        foodRepository.deleteById(id);
    }

    @Override
    public Food updateFood(Long id, Food food) {
        var fo = foodRepository.findById(id);
        if (fo.isPresent()){
            var courentFood = fo.get();
            courentFood.setName(food.getName());
            courentFood.setFoodType(food.getFoodType());
            courentFood.setDescription(food.getDescription());
            courentFood.setCattleList(food.getCattleList());
            courentFood.setUnit(food.getUnit());
            return foodRepository.save(courentFood);
        }else {
            return null;
        }
    }
}
