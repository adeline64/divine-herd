package com.herd.divineherd_back.service;

import com.herd.divineherd_back.entity.User;

import java.util.List;
import java.util.Optional;

public interface UserService {
    List<User> readUser();

    Optional<User> readOneUser(Long id);

    User createUser(User user);

    void deleteUser(Long id);

    User updateUser(Long id, User user);
}
