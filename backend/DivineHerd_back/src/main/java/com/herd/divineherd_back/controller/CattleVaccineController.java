package com.herd.divineherd_back.controller;

import com.herd.divineherd_back.entity.CattleVaccine;
import com.herd.divineherd_back.service.CattleVaccineService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api/cattleVaccine")
public class CattleVaccineController {

    private CattleVaccineService cattleVaccineService;

    public CattleVaccineController(CattleVaccineService cattleVaccineService) {
        this.cattleVaccineService = cattleVaccineService;
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @GetMapping()
    public Iterable<CattleVaccine> readCattleVaccine() {
        return cattleVaccineService.readCattleVaccine();
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @GetMapping("/{id}")
    public Optional<CattleVaccine> readOneCattleVaccine(@PathVariable Long id) {
        return cattleVaccineService.readOneCattleVaccine(id);
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @PostMapping()
    public CattleVaccine createCattleVaccine(@RequestBody CattleVaccine cattleVaccine) {
        return cattleVaccineService.createCattleVaccine(cattleVaccine);
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @DeleteMapping("/{id}")
    public void deleteCattleVaccine(@PathVariable Long id) {
        cattleVaccineService.deleteCattleVaccine(id);
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @PutMapping("/{id}")
    public CattleVaccine updateCattleVaccine(@PathVariable Long id, @RequestBody CattleVaccine cattleVaccine) {
        return cattleVaccineService.updateCattleVaccine(id, cattleVaccine);
    }
}
