package com.herd.divineherd_back.controller;

import com.herd.divineherd_back.entity.Vaccine;
import com.herd.divineherd_back.service.VaccineService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api/vaccines")
public class VaccineController {

    private VaccineService vaccineService;

    public VaccineController(VaccineService vaccineService) {
        this.vaccineService = vaccineService;
    }
    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @GetMapping()
    public Iterable<Vaccine> readVaccine() {
        return vaccineService.readVaccine();
    }
    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @GetMapping("/{id}")
    public Optional<Vaccine> readOneVaccine(@PathVariable Long id) {
        return vaccineService.readOneVaccine(id);
    }
    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @PostMapping()
    public Vaccine createVaccine(@RequestBody Vaccine vaccine) {
        return vaccineService.createVaccine(vaccine);
    }
    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @DeleteMapping("/{id}")
    public void deleteVaccine(@PathVariable Long id) {
        vaccineService.deleteVaccine(id);
    }
    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @PutMapping("/{id}")
    public Vaccine updateVaccine(@PathVariable Long id, @RequestBody Vaccine vaccine) {
        return vaccineService.updateVaccine(id, vaccine);
    }

}
