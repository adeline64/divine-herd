package com.herd.divineherd_back.service;

import com.herd.divineherd_back.entity.Bull;

import java.util.List;
import java.util.Optional;

public interface BullService {
    List<Bull> readBull();

    Optional<Bull> readOneBull(Long id);

    Bull createBull(Bull bull);

    void deleteBull(Long id);

    Bull updateBull(Long id, Bull bull);
}
