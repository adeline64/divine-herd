package com.herd.divineherd_back.controller;

import com.herd.divineherd_back.entity.Unit;
import com.herd.divineherd_back.service.UnitService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RequestMapping("api/units")
@RestController
public class UnitController {

    private UnitService unitService;

    public UnitController(UnitService unitService) {
        this.unitService = unitService;
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @GetMapping()
    public Iterable<Unit> readUnit() {
        return unitService.readUnit();
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @GetMapping("/{id}")
    public Optional<Unit> readOneUnit(@PathVariable Long id) {
        return unitService.readOneUnit(id);
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @PostMapping()
    public Unit createUnit(@RequestBody Unit unit) {
        return unitService.createUnit(unit);
    }

}
