package com.herd.divineherd_back.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.herd.divineherd_back.enums.UnitName;
import jakarta.persistence.*;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name = "unit")
public class Unit {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="unit_name", nullable = false)
    @Enumerated(EnumType.STRING)
    UnitName unitName;

    @JsonIgnore
    @OneToMany(mappedBy = "unit")
    private List<Food> foodList = new ArrayList<>();

    public Unit() {
    }

}
