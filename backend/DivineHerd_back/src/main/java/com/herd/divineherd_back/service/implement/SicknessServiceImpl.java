package com.herd.divineherd_back.service.implement;

import com.herd.divineherd_back.entity.Sickness;
import com.herd.divineherd_back.repository.SicknessRepository;
import com.herd.divineherd_back.service.SicknessService;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class SicknessServiceImpl implements SicknessService {

    private SicknessRepository sicknessRepository;

    public SicknessServiceImpl(SicknessRepository sicknessRepository) {
        this.sicknessRepository = sicknessRepository;
    }

    @Override
    public Iterable<Sickness> readSickness() {
        return sicknessRepository.findAll();
    }

    @Override
    public Optional<Sickness> readOneSickness(Long id) {
        return sicknessRepository.findById(id);
    }

    @Override
    public Sickness createSickness(Sickness sickness) {
        return sicknessRepository.save(sickness);
    }

    @Override
    public void deleteSickness(Long id) {
        sicknessRepository.deleteById(id);
    }

    @Override
    public Sickness updateSickness(Long id, Sickness sickness) {
        var sic = sicknessRepository.findById(id);
        if (sic.isPresent()) {
            var courentSickness = sic.get();
            courentSickness.setEndDate(sickness.getEndDate());
            courentSickness.setDescription(sickness.getDescription());
            courentSickness.setStartDate(sickness.getStartDate());
            courentSickness.setCattle(sickness.getCattle());
            courentSickness.setDiseaseName(sickness.getDiseaseName());
            courentSickness.setDiscardMilk(sickness.getDiscardMilk());
            courentSickness.setMedicationList(sickness.getMedicationList());
            return sicknessRepository.save(courentSickness);
        } else {
            return null;
        }
    }
}
