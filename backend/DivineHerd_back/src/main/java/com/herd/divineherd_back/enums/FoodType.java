package com.herd.divineherd_back.enums;

public enum FoodType {

    LIQUID,
    SOLID;

}
