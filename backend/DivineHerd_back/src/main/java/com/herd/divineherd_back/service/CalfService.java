package com.herd.divineherd_back.service;

import com.herd.divineherd_back.entity.Calf;

import java.util.List;
import java.util.Optional;

public interface CalfService {
    List<Calf> readCalf();

    Optional<Calf> readOneCalf(Long id);

    Calf createCalf(Calf calf);

    void deleteCalf(Long id);

    Calf updateCalf(Long id, Calf calf);
}
