package com.herd.divineherd_back.service;

import com.herd.divineherd_back.entity.Farm;

import java.util.Optional;

public interface FarmService {


    Iterable<Farm> readFarm();

    Optional<Farm> readOneFarm(Long id);

    Farm createFarm(Farm farm);

    void deleteFarm(Long id);

    Farm updateFarm(Long id, Farm farm);
}
