package com.herd.divineherd_back.controller;

import com.herd.divineherd_back.entity.*;
import com.herd.divineherd_back.service.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RequestMapping("api/cattles/cows")
@RestController
public class CowController {

    private CowService cowService;

    private FarmService farmService;

    private FoodService foodService;

    private RaceService raceService;

    private MedicationService medicationService;

    private SicknessService sicknessService;

    public CowController(CowService cowService, FarmService farmService, FoodService foodService, RaceService raceService, MedicationService medicationService, SicknessService sicknessService) {
        this.cowService = cowService;
        this.farmService = farmService;
        this.foodService = foodService;
        this.raceService = raceService;
        this.medicationService = medicationService;
        this.sicknessService = sicknessService;
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @GetMapping()
    public Iterable<Cow> readCow() {
        return cowService.readCow();
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @GetMapping("/{id}")
    public Optional<Cow> readOneCow(@PathVariable Long id) {
        return cowService.readOneCow(id);
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @PostMapping()
    public Cow createCow(@RequestBody Cow cow) {
        return cowService.createCow(cow);
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @DeleteMapping("/{id}")
    public void deleteCow(@PathVariable Long id) {
        cowService.deleteCow(id);
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @PutMapping("/{id}")
    public Cow updateCow(@PathVariable Long id, @RequestBody Cow cow) {
        return cowService.updateCow(id, cow);
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @PutMapping("/{idCow}/farms/{idFarm}")
    private Cow farmCow(@PathVariable("idCow") final Long idCow, @PathVariable("idFarm") final Long idFarm) {
        Optional<Cow> cowOptional = cowService.readOneCow(idCow);
        Optional <Farm>farmOptional = farmService.readOneFarm(idFarm);

        if (cowOptional.isPresent() && farmOptional.isPresent()){
            Cow cow = cowOptional.get();
            Farm farm = farmOptional.get();
            farm.getCattleList().add(cow);
            cow.setFarm(farm);
            return cowService.updateCow(idCow, cow);
        } else {
            return null;
        }
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @PutMapping("/{idCow}/foods/{idFood}")
    private Cow foodCow(@PathVariable("idCow") final Long idCow, @PathVariable("idFood") final Long idFood) {
        Optional<Cow> cowOptional = cowService.readOneCow(idCow);
        Optional <Food>foodOptional = foodService.readOneFood(idFood);

        if (cowOptional.isPresent() && foodOptional.isPresent()){
            Cow cow = cowOptional.get();
            Food food = foodOptional.get();
            cow.getFoodList().add(food);
            food.getCattleList().add(cow);
            return cowService.updateCow(idCow, cow);
        } else {
            return null;
        }
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @PutMapping("/{idCow}/races/{idRace}")
    private Cow raceCow(@PathVariable("idCow") final Long idCow, @PathVariable("idRace") final Long idRace) {
        Optional<Cow> cowOptional = cowService.readOneCow(idCow);
        Optional <Race>raceOptional = raceService.readOneRace(idRace);

        if (cowOptional.isPresent() && raceOptional.isPresent()){
            Cow cow = cowOptional.get();
            Race race = raceOptional.get();
            race.getCattleList().add(cow);
            cow.getRaceList().add(race);
            return cowService.updateCow(idCow, cow);
        } else {
            return null;
        }
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @PutMapping("/{idCow}/sickness/{idSickness}")
    private Cow sicknessCow(@PathVariable("idCow") final Long idCow, @PathVariable("idSickness") final Long idSickness) {
        Optional<Cow> cowOptional = cowService.readOneCow(idCow);
        Optional <Sickness>sicknessOptional = sicknessService.readOneSickness(idSickness);

        if (cowOptional.isPresent() && sicknessOptional.isPresent()){
            Cow cow = cowOptional.get();
            Sickness sickness = sicknessOptional.get();
            sickness.setCattle(cow);
            cow.getSicknessList().add(sickness);
            return cowService.updateCow(idCow, cow);
        } else {
            return null;
        }
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @PutMapping("/{idCow}/medications/{idMedication}")
    private Cow medicationsCalf(@PathVariable("idCow") final Long idCow, @PathVariable("idMedication") final Long idMedication) {
        Optional<Cow> cowOptional = cowService.readOneCow(idCow);
        Optional <Medication>medicationOptional = medicationService.readOneMedication(idMedication);

        if (cowOptional.isPresent() && medicationOptional.isPresent()){
            Cow cow = cowOptional.get();
            Medication medication = medicationOptional.get();
            medication.getCattleList().add(cow);
            cow.getMedicationList().add(medication);
            return cowService.updateCow(idCow, cow);
        } else {
            return null;
        }
    }

}
