package com.herd.divineherd_back.controller;

import com.herd.divineherd_back.entity.DairyTechnician;
import com.herd.divineherd_back.service.DairyTechnicianService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RequestMapping("api/personals/dairyTechnicians")
@RestController
public class DairyTechnicianController {

    private DairyTechnicianService dairyTechnicianService;

    public DairyTechnicianController(DairyTechnicianService dairyTechnicianService) {
        this.dairyTechnicianService = dairyTechnicianService;
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @GetMapping()
    public Iterable<DairyTechnician> readDairyTechnician() {
        return dairyTechnicianService.readDairyTechnician();
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @GetMapping("/{id}")
    public Optional<DairyTechnician> readOneDairyTechnician(@PathVariable Long id) {
        return dairyTechnicianService.readOneDairyTechnician(id);
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @PostMapping()
    public DairyTechnician createDairyTechnician(@RequestBody DairyTechnician dairyTechnician) {
        return dairyTechnicianService.createDairyTechnician(dairyTechnician);
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @DeleteMapping("/{id}")
    public void deleteDairyTechnician(@PathVariable Long id) {
        dairyTechnicianService.deleteDairyTechnician(id);
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @PutMapping("/{id}")
    public DairyTechnician updateDairyTechnician(@PathVariable Long id, @RequestBody DairyTechnician dairyTechnician) {
        return dairyTechnicianService.updateDairyTechnician(id, dairyTechnician);
    }
}
