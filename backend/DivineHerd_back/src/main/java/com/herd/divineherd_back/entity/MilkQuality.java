package com.herd.divineherd_back.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "milk_quality")
@Data
public class MilkQuality {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "fat")
    private float fat;
    @Column(name = "protein")
    private float protein;
    @Temporal(TemporalType.DATE)
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Column(nullable = false, name = "date_milk_quality")
    private Date dateMilkQuality;

    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "cow_id")
    private Cow cow;

    public MilkQuality() {
    }

}
