package com.herd.divineherd_back.controller;

import com.herd.divineherd_back.entity.Role;
import com.herd.divineherd_back.service.RoleService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api/role")
public class RoleController {

    private RoleService roleService;

    public RoleController(RoleService roleService) {
        this.roleService = roleService;
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @GetMapping()
    public Iterable<Role> readRole() {
        return roleService.readRole();
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @GetMapping("/{id}")
    public Optional<Role> readOneRole(@PathVariable Long id) {
        return roleService.readOneRole(id);
    }
}
