package com.herd.divineherd_back.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.herd.divineherd_back.enums.FoodType;
import jakarta.persistence.*;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "food")
@Data
public class Food {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "name", nullable = false)
    private String name;
    @Column(name="food_type", nullable = false)
    @Enumerated(EnumType.STRING)
    FoodType foodType;
    @Column(name="description", columnDefinition="TEXT")
    private String description;
    @JsonIgnore
    @ManyToMany(mappedBy = "foodList")
    private List<Cattle> cattleList = new ArrayList<>();

    @ManyToOne
    @JoinColumn(name = "unit_id")
    private Unit unit;
    public Food() {
    }

    public Food(Long id, String name, FoodType foodType, String description, List<Cattle> cattleList) {
        this.id = id;
        this.name = name;
        this.foodType = foodType;
        this.description = description;
        this.cattleList = cattleList;
    }
}
