package com.herd.divineherd_back.service.implement;

import com.herd.divineherd_back.entity.Medication;
import com.herd.divineherd_back.repository.MedicationRepository;
import com.herd.divineherd_back.service.MedicationService;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class MedicationServiceImpl implements MedicationService {

    private MedicationRepository medicationRepository;

    public MedicationServiceImpl(MedicationRepository medicationRepository) {
        this.medicationRepository = medicationRepository;
    }

    @Override
    public Iterable<Medication> readMedication() {
        return medicationRepository.findAll();
    }

    @Override
    public Optional<Medication> readOneMedication(Long id) {
        return medicationRepository.findById(id);
    }

    @Override
    public Medication createMedication(Medication medication) {
        return medicationRepository.save(medication);
    }

    @Override
    public void deleteMedication(Long id) {
        medicationRepository.deleteById(id);
    }

    @Override
    public Medication updateMedication(Long id, Medication medication) {
        var med = medicationRepository.findById(id);
        if (med.isPresent()) {
            var courentMedication = med.get();
            courentMedication.setName(medication.getName());
            courentMedication.setDescription(medication.getDescription());
            courentMedication.setSicknessesList(medication.getSicknessesList());
            courentMedication.setCattleList(medication.getCattleList());
            return medicationRepository.save(courentMedication);
        } else {
            return null;
        }
    }
}
