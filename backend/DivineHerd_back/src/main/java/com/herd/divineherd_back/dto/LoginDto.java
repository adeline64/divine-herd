package com.herd.divineherd_back.dto;

import lombok.Data;

@Data
public class LoginDto {

    private String email;

    private String password;

    public LoginDto() {
    }
}
