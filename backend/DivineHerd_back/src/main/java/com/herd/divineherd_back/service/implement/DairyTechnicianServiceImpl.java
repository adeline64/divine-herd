package com.herd.divineherd_back.service.implement;

import com.herd.divineherd_back.entity.DairyTechnician;
import com.herd.divineherd_back.repository.DairyTechnicianRepository;
import com.herd.divineherd_back.service.DairyTechnicianService;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class DairyTechnicianServiceImpl implements DairyTechnicianService {

    private DairyTechnicianRepository dairyTechnicianRepository;

    public DairyTechnicianServiceImpl(DairyTechnicianRepository dairyTechnicianRepository) {
        this.dairyTechnicianRepository = dairyTechnicianRepository;
    }


    @Override
    public Iterable<DairyTechnician> readDairyTechnician() {
        return dairyTechnicianRepository.findAll();
    }

    @Override
    public Optional<DairyTechnician> readOneDairyTechnician(Long id) {
        return dairyTechnicianRepository.findById(id);
    }

    @Override
    public DairyTechnician createDairyTechnician(DairyTechnician dairyTechnician) {
        return dairyTechnicianRepository.save(dairyTechnician);
    }

    @Override
    public void deleteDairyTechnician(Long id) {
        dairyTechnicianRepository.deleteById(id);
    }

    @Override
    public DairyTechnician updateDairyTechnician(Long id, DairyTechnician dairyTechnician) {
        var d = dairyTechnicianRepository.findById(id);
        if (d.isPresent()){
            var courentDairyTechnician = d.get();
            courentDairyTechnician.setDairyName(dairyTechnician.getDairyName());
            courentDairyTechnician.setNamePersonal(dairyTechnician.getNamePersonal());
            courentDairyTechnician.setAddress(dairyTechnician.getAddress());
            courentDairyTechnician.setZipCode(dairyTechnician.getZipCode());
            courentDairyTechnician.setCity(dairyTechnician.getCity());
            courentDairyTechnician.setPassageDate(dairyTechnician.getPassageDate());
            courentDairyTechnician.setPassingReason(dairyTechnician.getPassingReason());
            courentDairyTechnician.setFarmList(dairyTechnician.getFarmList());
            return dairyTechnicianRepository.save(courentDairyTechnician);
        }else {
            return null;
        }
    }
}
