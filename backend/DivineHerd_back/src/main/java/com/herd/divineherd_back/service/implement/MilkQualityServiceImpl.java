package com.herd.divineherd_back.service.implement;

import com.herd.divineherd_back.entity.MilkQuality;
import com.herd.divineherd_back.repository.MilkQualityRepository;
import com.herd.divineherd_back.service.MilkQualityService;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class MilkQualityServiceImpl implements MilkQualityService {

    private MilkQualityRepository milkQualityRepository;

    public MilkQualityServiceImpl(MilkQualityRepository milkQualityRepository) {
        this.milkQualityRepository = milkQualityRepository;
    }

    @Override
    public Iterable<MilkQuality> readMilkQuality() {
        return milkQualityRepository.findAll();
    }

    @Override
    public Optional<MilkQuality> readOneMilkQuantity(Long id) {
        return milkQualityRepository.findById(id);
    }

    @Override
    public MilkQuality createMilkQuality(MilkQuality milkQuality) {
        return milkQualityRepository.save(milkQuality);
    }

    @Override
    public void deleteMilkQuality(Long id) {
        milkQualityRepository.deleteById(id);
    }

    @Override
    public MilkQuality updateMilkQuality(Long id, MilkQuality milkQuality) {
        var mq = milkQualityRepository.findById(id);
        if (mq.isPresent()){
            var courentQuality = mq.get();
            courentQuality.setFat(milkQuality.getFat());
            courentQuality.setProtein(milkQuality.getProtein());
            courentQuality.setDateMilkQuality(milkQuality.getDateMilkQuality());
            courentQuality.setCow(milkQuality.getCow());
            return milkQualityRepository.save(courentQuality);
        }else {
            return null;
        }
    }
}
