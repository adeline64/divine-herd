package com.herd.divineherd_back.service;

import com.herd.divineherd_back.entity.Race;

import java.util.Optional;

public interface RaceService {
    Iterable<Race> readRace();

    Optional<Race> readOneRace(Long id);

    Race createRace(Race race);

    void deleteRace(Long id);

    Race updateRace(Long id, Race race);
}
