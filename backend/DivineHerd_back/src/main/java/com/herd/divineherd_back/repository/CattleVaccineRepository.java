package com.herd.divineherd_back.repository;

import com.herd.divineherd_back.entity.CattleVaccine;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CattleVaccineRepository extends JpaRepository<CattleVaccine, Long> {
}
