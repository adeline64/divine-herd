package com.herd.divineherd_back.service;

import com.herd.divineherd_back.entity.MilkQuantity;

import java.util.Optional;

public interface MilkQuantityService {
    Iterable<MilkQuantity> readMilkQuantity();

    Optional<MilkQuantity> readOneMilkQuantity(Long id);

    MilkQuantity createMilkQuantity(MilkQuantity milkQuantity);

    void deleteMilkQuantity(Long id);

    MilkQuantity updateMilkQuantity(Long id, MilkQuantity milkQuantity);
}
