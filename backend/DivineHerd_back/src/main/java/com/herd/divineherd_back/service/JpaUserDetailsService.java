package com.herd.divineherd_back.service;

import com.herd.divineherd_back.model.SecurityUser;
import com.herd.divineherd_back.entity.User;
import com.herd.divineherd_back.repository.UserRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class JpaUserDetailsService implements UserDetailsService {

    private final UserRepository userRepository;

    public JpaUserDetailsService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepository
                .findByEmail(username)
                .map((User user) -> new SecurityUser(user))
                .orElseThrow(() -> new UsernameNotFoundException("Username not found: " + username));
    }


}
