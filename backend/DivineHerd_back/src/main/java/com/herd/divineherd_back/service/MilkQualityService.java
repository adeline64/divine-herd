package com.herd.divineherd_back.service;

import com.herd.divineherd_back.entity.MilkQuality;

import java.util.Optional;

public interface MilkQualityService {
    Iterable<MilkQuality> readMilkQuality();

    Optional<MilkQuality> readOneMilkQuantity(Long id);

    MilkQuality createMilkQuality(MilkQuality milkQuality);

    void deleteMilkQuality(Long id);

    MilkQuality updateMilkQuality(Long id, MilkQuality milkQuality);
}
