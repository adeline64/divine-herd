package com.herd.divineherd_back.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "race")
@Data
public class Race {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, name = "name")
    private String name;

    @Column(name="description", columnDefinition="TEXT")
    private String description;
    @JsonIgnore
    @ManyToMany(mappedBy = "raceList")
    private List<Cattle> cattleList = new ArrayList<>();

    public Race() {
    }

    public Race(Long id, String name, String description, List<Cattle> cattleList) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.cattleList = cattleList;
    }
}
