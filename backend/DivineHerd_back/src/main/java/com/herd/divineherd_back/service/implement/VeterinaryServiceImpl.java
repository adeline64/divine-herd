package com.herd.divineherd_back.service.implement;

import com.herd.divineherd_back.entity.Veterinary;
import com.herd.divineherd_back.repository.VeterinaryRepository;
import com.herd.divineherd_back.service.VeterinaryService;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class VeterinaryServiceImpl implements VeterinaryService {

    private VeterinaryRepository veterinaryRepository;

    public VeterinaryServiceImpl(VeterinaryRepository veterinaryRepository) {
        this.veterinaryRepository = veterinaryRepository;
    }

    @Override
    public Iterable<Veterinary> readVeterinary() {
        return veterinaryRepository.findAll();
    }

    @Override
    public Optional<Veterinary> readOneVeterinary(Long id) {
        return veterinaryRepository.findById(id);
    }

    @Override
    public Veterinary createVeterinary(Veterinary veterinary) {
        return veterinaryRepository.save(veterinary);
    }

    @Override
    public void deleteVeterinary(Long id) {
        veterinaryRepository.deleteById(id);
    }

    @Override
    public Veterinary updateVeterinary(Long id, Veterinary veterinary) {
        var v = veterinaryRepository.findById(id);
        if (v.isPresent()){
            var courentVetenary = v.get();
            courentVetenary.setClinicName(veterinary.getClinicName());
            courentVetenary.setNamePersonal(veterinary.getNamePersonal());
            courentVetenary.setAddress(veterinary.getAddress());
            courentVetenary.setZipCode(veterinary.getZipCode());
            courentVetenary.setCity(veterinary.getCity());
            courentVetenary.setPassageDate(veterinary.getPassageDate());
            courentVetenary.setPassingReason(veterinary.getPassingReason());
            courentVetenary.setFarmList(veterinary.getFarmList());
            return veterinaryRepository.save(courentVetenary);
        }else {
            return null;
        }
    }
}
