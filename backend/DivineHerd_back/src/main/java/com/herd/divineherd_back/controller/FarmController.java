package com.herd.divineherd_back.controller;

import com.herd.divineherd_back.entity.DairyTechnician;
import com.herd.divineherd_back.entity.Farm;
import com.herd.divineherd_back.entity.Veterinary;
import com.herd.divineherd_back.service.DairyTechnicianService;
import com.herd.divineherd_back.service.FarmService;
import com.herd.divineherd_back.service.VeterinaryService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api/farms")
public class FarmController {

    private FarmService farmService;
    private VeterinaryService veterinaryService;
    private DairyTechnicianService dairyTechnicianService;

    public FarmController(FarmService farmService, VeterinaryService veterinaryService, DairyTechnicianService dairyTechnicianService) {
        this.farmService = farmService;
        this.veterinaryService = veterinaryService;
        this.dairyTechnicianService = dairyTechnicianService;
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @GetMapping()
    public Iterable<Farm> readFarm() {
        return farmService.readFarm();
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @GetMapping("/{id}")
    public Optional<Farm> readOneFarm(@PathVariable Long id) {
        return farmService.readOneFarm(id);
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @PostMapping()
    public Farm createFarm(@RequestBody Farm farm) {
        return farmService.createFarm(farm);
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @DeleteMapping("/{id}")
    public void deleteFarm(@PathVariable Long id) {
        farmService.deleteFarm(id);
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @PutMapping("/{id}")
    public Farm updateFarm(@PathVariable Long id, @RequestBody Farm farm) {
        return farmService.updateFarm(id, farm);
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @PutMapping("/{idFarm}/veterinary/{idVeterinary}")
    private Farm VeterinaryFarm(@PathVariable("idFarm") final Long idFarm, @PathVariable("idVeterinary") final Long idVeterinary) {
        Optional<Farm> farmOptional = farmService.readOneFarm(idFarm);
        Optional <Veterinary> veterinaryOptional = veterinaryService.readOneVeterinary(idVeterinary);

        if (farmOptional.isPresent() && veterinaryOptional.isPresent()){
            Veterinary veterinary = veterinaryOptional.get();
            Farm farm = farmOptional.get();
            farm.getPersonalList().add(veterinary);
            veterinary.getFarmList().add(farm);
            return farmService.updateFarm(idFarm, farm);
        } else {
            return null;
        }
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @PutMapping("/{idFarm}/dairyTechnician/{idDairyTechnician}")
    private Farm DairyTechnicianFarm(@PathVariable("idFarm") final Long idFarm, @PathVariable("idDairyTechnician") final Long idDairyTechnician) {
        Optional<Farm> farmOptional = farmService.readOneFarm(idFarm);
        Optional <DairyTechnician> dairyTechnicianOptional = dairyTechnicianService.readOneDairyTechnician(idDairyTechnician);

        if (farmOptional.isPresent() && dairyTechnicianOptional.isPresent()){
            DairyTechnician dairyTechnician = dairyTechnicianOptional.get();
            Farm farm = farmOptional.get();
            farm.getPersonalList().add(dairyTechnician);
            dairyTechnician.getFarmList().add(farm);
            return farmService.updateFarm(idFarm, farm);
        } else {
            return null;
        }
    }
}
