package com.herd.divineherd_back.controller;

import com.herd.divineherd_back.entity.*;
import com.herd.divineherd_back.service.FoodService;
import com.herd.divineherd_back.service.UnitService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api/foods")
public class FoodController {

    private FoodService foodService;

    private UnitService unitService;

    public FoodController(FoodService foodService, UnitService unitService) {
        this.foodService = foodService;
        this.unitService = unitService;
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @GetMapping()
    public Iterable<Food> readFood() {
        return foodService.readFood();
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @GetMapping("/{id}")
    public Optional<Food> readOneFood(@PathVariable Long id) {
        return foodService.readOneFood(id);
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @PostMapping()
    public Food createFood(@RequestBody Food food) {
        return foodService.createFood(food);
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @DeleteMapping("/{id}")
    public void deleteFood(@PathVariable Long id) {
        foodService.deleteFood(id);
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @PutMapping("/{id}")
    public Food updateFood(@PathVariable Long id, @RequestBody Food food) {
        return foodService.updateFood(id, food);
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @PutMapping("/{idFood}/units/{idUnit}")
    private Food foodUnit(@PathVariable("idFood") final Long idFood, @PathVariable("idUnit") final Long idUnit) {
        Optional<Food> foodOptional = foodService.readOneFood(idFood);
        Optional <Unit>unitOptional = unitService.readOneUnit(idUnit);

        if (foodOptional.isPresent() && unitOptional.isPresent()){
            Food food = foodOptional.get();
            Unit unit = unitOptional.get();
            food.setUnit(unit);
            return foodService.updateFood(idFood, food);
        } else {
            return null;
        }
    }


}
