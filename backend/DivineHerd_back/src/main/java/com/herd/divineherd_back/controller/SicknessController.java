package com.herd.divineherd_back.controller;

import com.herd.divineherd_back.entity.Medication;
import com.herd.divineherd_back.entity.Sickness;
import com.herd.divineherd_back.service.MedicationService;
import com.herd.divineherd_back.service.SicknessService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api/sickness")
public class SicknessController {

    private SicknessService sicknessService;

    private MedicationService medicationService;

    public SicknessController(SicknessService sicknessService, MedicationService medicationService) {
        this.sicknessService = sicknessService;
        this.medicationService = medicationService;
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @GetMapping()
    public Iterable<Sickness> readSickness() {
        return sicknessService.readSickness();
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @GetMapping("/{id}")
    public Optional<Sickness> readOneSickness(@PathVariable Long id) {
        return sicknessService.readOneSickness(id);
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @PostMapping()
    public Sickness createSickness(@RequestBody Sickness sickness) {
        return sicknessService.createSickness(sickness);
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @DeleteMapping("/{id}")
    public void deleteSickness(@PathVariable Long id) {
        sicknessService.deleteSickness(id);
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @PutMapping("/{id}")
    public Sickness updateSickness(@PathVariable Long id, @RequestBody Sickness sickness) {
        return sicknessService.updateSickness(id, sickness);
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @PutMapping("/{idSickness}/medications/{idMedication}")
    private Sickness medicationsSickness(@PathVariable("idSickness") final Long idSickness, @PathVariable("idMedication") final Long idMedication) {
        Optional<Sickness> sicknessOptional = sicknessService.readOneSickness(idSickness);
        Optional <Medication>medicationOptional = medicationService.readOneMedication(idMedication);

        if (sicknessOptional.isPresent() && medicationOptional.isPresent()){
            Sickness sickness = sicknessOptional.get();
            Medication medication = medicationOptional.get();
            medication.getSicknessesList().add(sickness);
            sickness.getMedicationList().add(medication);
            return sicknessService.updateSickness(idSickness, sickness);
        } else {
            return null;
        }
    }
}
