package com.herd.divineherd_back.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Data;

@Entity
@Table(name = "dairy_technician")
@Data
public class DairyTechnician extends Personal {

    @Column(name = "dairy_name", nullable = false)
    private String dairyName;

    public DairyTechnician() {}


}
