package com.herd.divineherd_back.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.persistence.*;
import lombok.Data;
import java.util.Date;

@Entity
@Table(name = "cattle_vaccine")
@Data
public class CattleVaccine {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Temporal(TemporalType.DATE)
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Column(nullable = false, name = "date_vaccine")
    private Date dateVaccine;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_cattle")
    private Cattle cattle;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_vaccine")
    private  Vaccine vaccine;

    public CattleVaccine() {
    }

    public CattleVaccine(Long id, Date dateVaccine, Cattle cattle, Vaccine vaccine) {
        this.id = id;
        this.dateVaccine = dateVaccine;
        this.cattle = cattle;
        this.vaccine = vaccine;
    }
}
