package com.herd.divineherd_back.service.implement;

import com.herd.divineherd_back.entity.Race;
import com.herd.divineherd_back.repository.RaceRepository;
import com.herd.divineherd_back.service.RaceService;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class RaceServiceImpl implements RaceService {

    private RaceRepository raceRepository;

    public RaceServiceImpl(RaceRepository raceRepository) {
        this.raceRepository = raceRepository;
    }


    @Override
    public Iterable<Race> readRace() {
        return raceRepository.findAll();
    }

    @Override
    public Optional<Race> readOneRace(Long id) {
        return raceRepository.findById(id);
    }

    @Override
    public Race createRace(Race race) {
        return raceRepository.save(race);
    }

    @Override
    public void deleteRace(Long id) {
        raceRepository.deleteById(id);
    }

    @Override
    public Race updateRace(Long id, Race race) {
        var ra = raceRepository.findById(id);
        if (ra.isPresent()) {
            var courentRace = ra.get();
            courentRace.setName(race.getName());
            courentRace.setDescription(race.getDescription());
            courentRace.setCattleList(race.getCattleList());
            return raceRepository.save(courentRace);
        } else {
            return null;
        }
    }
}
