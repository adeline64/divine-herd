package com.herd.divineherd_back.controller;

import com.herd.divineherd_back.entity.*;
import com.herd.divineherd_back.service.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api/users")
public class UserController {

    private UserService userService;

    private RoleService roleService;

    private FarmService farmService;

    public UserController(UserService userService, RoleService roleService, FarmService farmService) {
        this.userService = userService;
        this.roleService = roleService;
        this.farmService = farmService;
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @GetMapping()
    public Iterable<User> readUser() {
        return userService.readUser();
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @GetMapping("/{id}")
    public Optional<User> readOneUser(@PathVariable Long id) {
        return userService.readOneUser(id);
    }

    @PostMapping()
    public User createUser(@RequestBody User user) {
        return userService.createUser(user);
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @DeleteMapping("/{id}")
    public void deleteUser(@PathVariable Long id) {
        userService.deleteUser(id);
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @PutMapping("/{id}")
    public User updateUser(@PathVariable Long id, @RequestBody User user) {
        return userService.updateUser(id, user);
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @PutMapping("/{idUser}/roles/{idRole}")
    private User changeRole(@PathVariable("idUser") final Long idUser, @PathVariable("idRole") final Long idRole) {
        Optional<Role> roleOptional = roleService.readOneRole(idRole);
        Optional <User>userOptional = userService.readOneUser(idUser);

        if (roleOptional.isPresent() && userOptional.isPresent()){
            Role role = roleOptional.get();
            User user = userOptional.get();
            user.getRoleList().add(role);
            role.getUserList().add(user);
            return userService.updateUser(idUser, user);
        } else {
            return null;
        }
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @PutMapping("/{idUser}/farms/{idFarm}")
    private User gestionFarm(@PathVariable("idUser") final Long idUser, @PathVariable("idFarm") final Long idFarm) {
        Optional<Farm> farmOptional = farmService.readOneFarm(idFarm);
        Optional <User>userOptional = userService.readOneUser(idUser);

        if (farmOptional.isPresent() && userOptional.isPresent()){
            Farm farm = farmOptional.get();
            User user = userOptional.get();
            user.setFarm(farm);
            return userService.updateUser(idUser, user);
        } else {
            return null;
        }
    }
}
