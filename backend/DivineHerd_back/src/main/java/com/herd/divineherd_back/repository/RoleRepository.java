package com.herd.divineherd_back.repository;

import com.herd.divineherd_back.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<Role,Long> {
    @Query("SELECT n FROM Role n WHERE n.name = :name")
    public Role findByName(String name);
}
