package com.herd.divineherd_back.service;

import com.herd.divineherd_back.entity.Unit;

import java.util.Optional;

public interface UnitService {
    Iterable<Unit> readUnit();

    Optional<Unit> readOneUnit(Long id);

    Unit createUnit(Unit unit);
}
