package com.herd.divineherd_back.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "cow")
@Data
public class Cow extends Cattle {
    @Column(name = "number_of_dead_calf", nullable = false)
    private int numberOfDeadCalf;
    @Column(name = "number_of_calf_born", nullable = false)
    private int numberOfCalfBorn;

    @OneToMany(mappedBy = "cow")
    private List<MilkQuality>milkQualityList = new ArrayList<>();

    @OneToMany(mappedBy = "cow")
    private List<MilkQuantity>milkQuantityList = new ArrayList<>();

    @JsonIgnore
    @OneToMany(mappedBy = "cow")
    private List<Calf> calfList = new ArrayList<>();

    public Cow(){}

    public Cow(Long id, String name, String color, Date dateOfBirth, Date dateOfDeath, Boolean horn, String numberJob, String numberPaper, List<Race> raceList, Farm farm) {
        super(id, name, color, dateOfBirth, dateOfDeath, horn, numberJob, numberPaper, raceList, farm);
    }
}
