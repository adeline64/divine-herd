package com.herd.divineherd_back.service;

import com.herd.divineherd_back.entity.Role;

import java.util.Optional;

public interface RoleService {
    Iterable<Role> readRole();

    Optional<Role> readOneRole(Long id);
}
