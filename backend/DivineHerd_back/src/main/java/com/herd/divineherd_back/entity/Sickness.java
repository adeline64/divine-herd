package com.herd.divineherd_back.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "sickness")
@Data
public class Sickness {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Temporal(TemporalType.DATE)
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Column(name = "start_date")
    private Date startDate;

    @Temporal(TemporalType.DATE)
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Column(name = "end_date")
    private Date endDate;

    @Column(nullable = false, name = "disease_name")
    private String diseaseName;

    @Column(nullable = false, name = "discard_milk")
    private Boolean discardMilk;

    @Column(name="description", columnDefinition="TEXT")
    private String description;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "cattle_id")
    private Cattle cattle;

    @ManyToMany
    @JoinTable(name = "medication_sickness",
            joinColumns = @JoinColumn(name = "sickness_id"),
            inverseJoinColumns = @JoinColumn(name = "medication_id")
    )
    private List<Medication> medicationList = new ArrayList<>();

    public Sickness() {
    }

    public Sickness(Long id, Date startDate, Date endDate, String diseaseName, Boolean discardMilk, String description, List<Medication> medicationList) {
        this.id = id;
        this.startDate = startDate;
        this.endDate = endDate;
        this.diseaseName = diseaseName;
        this.discardMilk = discardMilk;
        this.description = description;
        this.medicationList = medicationList;
    }
}
