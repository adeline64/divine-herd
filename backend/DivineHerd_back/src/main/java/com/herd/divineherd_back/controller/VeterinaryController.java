package com.herd.divineherd_back.controller;

import com.herd.divineherd_back.entity.Veterinary;
import com.herd.divineherd_back.service.VeterinaryService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RequestMapping("api/personals/veterinaries")
@RestController
public class VeterinaryController  {

    private VeterinaryService veterinaryService;

    public VeterinaryController(VeterinaryService veterinaryService) {
        this.veterinaryService = veterinaryService;
    }
    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @GetMapping()
    public Iterable<Veterinary> readVeterinary() {
        return veterinaryService.readVeterinary();
    }
    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @GetMapping("/{id}")
    public Optional<Veterinary> readOneVeterinary(@PathVariable Long id) {
        return veterinaryService.readOneVeterinary(id);
    }
    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @PostMapping()
    public Veterinary createVeterinary(@RequestBody Veterinary veterinary) {
        return veterinaryService.createVeterinary(veterinary);
    }
    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @DeleteMapping("/{id}")
    public void deleteVeterinary(@PathVariable Long id) {
        veterinaryService.deleteVeterinary(id);
    }
    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @PutMapping("/{id}")
    public Veterinary updateVeterinary(@PathVariable Long id, @RequestBody Veterinary veterinary) {
        return veterinaryService.updateVeterinary(id, veterinary);
    }



}
