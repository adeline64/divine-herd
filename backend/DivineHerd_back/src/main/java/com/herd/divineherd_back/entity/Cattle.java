package com.herd.divineherd_back.entity;

import javax.validation.constraints.Size;
import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.persistence.*;
import lombok.Data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "cattle")
@Data
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class Cattle {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(nullable = false, name = "color")
    private String color;

    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm")
    @Column(nullable = false, name = "date_of_birth")
    private Date dateOfBirth;

    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm")
    @Column(name = "date_of_death")
    private Date dateOfDeath;

    @Column(nullable = false, name = "horn")
    private Boolean horn;

    @Column(name = "number_job", unique = true, length = 4)
    @Size(max = 4)
    private String numberJob;

    @Column(name = "number_paper", unique = true, length = 10)
    @Size(max = 10)
    private String numberPaper;

    @ManyToMany
    @JoinTable(name = "food_cattle",
            joinColumns = @JoinColumn(name = "food_id"),
            inverseJoinColumns = @JoinColumn(name = "cattle_id")
    )
    private List<Food> foodList = new ArrayList<>();

    @ManyToMany
    @JoinTable(name = "race_cattle",
            joinColumns = @JoinColumn(name = "cattle_id"),
            inverseJoinColumns = @JoinColumn(name = "race_id")
    )
    private List<Race> raceList = new ArrayList<>();

    @ManyToMany
    @JoinTable(name = "medication_cattle",
            joinColumns = @JoinColumn(name = "cattle_id"),
            inverseJoinColumns = @JoinColumn(name = "medication_id")
    )
    private List<Medication> medicationList = new ArrayList<>();

    @ManyToOne
    @JoinColumn(name = "farm_id")
    private Farm farm;

    @OneToMany(mappedBy = "cattle")
    private List<Sickness>sicknessList = new ArrayList<>();

    @OneToMany(mappedBy = "cattle")
    private List<CattleVaccine> cattleVaccineList = new ArrayList<>();

    public Cattle() {
    }

    public Cattle(Long id, String name, String color, Date dateOfBirth, Date dateOfDeath, Boolean horn, String numberJob, String numberPaper, List<Race> raceList, Farm farm) {
        this.id = id;
        this.name = name;
        this.color = color;
        this.dateOfBirth = dateOfBirth;
        this.dateOfDeath = dateOfDeath;
        this.horn = horn;
        this.numberJob = numberJob;
        this.numberPaper = numberPaper;
        this.raceList = raceList;
        this.farm = farm;
    }
}
