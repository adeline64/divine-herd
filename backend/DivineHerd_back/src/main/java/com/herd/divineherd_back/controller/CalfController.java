package com.herd.divineherd_back.controller;

import com.herd.divineherd_back.entity.*;
import com.herd.divineherd_back.service.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api/cattles/calfs")
public class CalfController {

    private CalfService calfService;

    private FarmService farmService;

    private FoodService foodService;

    private RaceService raceService;

    private SicknessService sicknessService;

    private MedicationService medicationService;

    public CalfController(CalfService calfService, FarmService farmService, FoodService foodService, RaceService raceService, SicknessService sicknessService, MedicationService medicationService) {
        this.calfService = calfService;
        this.farmService = farmService;
        this.foodService = foodService;
        this.raceService = raceService;
        this.sicknessService = sicknessService;
        this.medicationService = medicationService;
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @GetMapping()
    public Iterable<Calf> readCalf() {
        return calfService.readCalf();
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @GetMapping("/{id}")
    public Optional<Calf> readOneCalf(@PathVariable Long id) {
        return calfService.readOneCalf(id);
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @PostMapping()
    public Calf createCalf(@RequestBody Calf calf) {
        return calfService.createCalf(calf);
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @DeleteMapping("/{id}")
    public void deleteCalf(@PathVariable Long id) {
        calfService.deleteCalf(id);
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @PutMapping("/{id}")
    public Calf updateCalf(@PathVariable Long id, @RequestBody Calf calf) {
        return calfService.updateCalf(id, calf);
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @PutMapping("/{idCalf}/farms/{idFarm}")
    private Calf farmCalf(@PathVariable("idCalf") final Long idCalf, @PathVariable("idFarm") final Long idFarm) {
        Optional<Calf> calfOptional = calfService.readOneCalf(idCalf);
        Optional <Farm>farmOptional = farmService.readOneFarm(idFarm);

        if (calfOptional.isPresent() && farmOptional.isPresent()){
            Calf calf = calfOptional.get();
            Farm farm = farmOptional.get();
            farm.getCattleList().add(calf);
            calf.setFarm(farm);
            return calfService.updateCalf(idCalf, calf);
        } else {
            return null;
        }
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @PutMapping("/{idCalf}/foods/{idFood}")
    private Calf foodCalf(@PathVariable("idCalf") final Long idCalf, @PathVariable("idFood") final Long idFood) {
        Optional<Calf> calfOptional = calfService.readOneCalf(idCalf);
        Optional <Food>foodOptional = foodService.readOneFood(idFood);

        if (calfOptional.isPresent() && foodOptional.isPresent()){
            Calf calf = calfOptional.get();
            Food food = foodOptional.get();
            food.getCattleList().add(calf);
            calf.getFoodList().add(food);
            return calfService.updateCalf(idCalf, calf);
        } else {
            return null;
        }
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @PutMapping("/{idCalf}/races/{idRace}")
    private Calf raceCalf(@PathVariable("idCalf") final Long idCalf, @PathVariable("idRace") final Long idRace) {
        Optional<Calf> calfOptional = calfService.readOneCalf(idCalf);
        Optional <Race>raceOptional = raceService.readOneRace(idRace);

        if (calfOptional.isPresent() && raceOptional.isPresent()){
            Calf calf = calfOptional.get();
            Race race = raceOptional.get();
            race.getCattleList().add(calf);
            calf.getRaceList().add(race);
            return calfService.updateCalf(idCalf, calf);
        } else {
            return null;
        }
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @PutMapping("/{idCalf}/sickness/{idSickness}")
    private Calf sicknessCalf(@PathVariable("idCalf") final Long idCalf, @PathVariable("idSickness") final Long idSickness) {
        Optional<Calf> calfOptional = calfService.readOneCalf(idCalf);
        Optional <Sickness>sicknessOptional = sicknessService.readOneSickness(idSickness);

        if (calfOptional.isPresent() && sicknessOptional.isPresent()){
            Calf calf = calfOptional.get();
            Sickness sickness = sicknessOptional.get();
            sickness.setCattle(calf);
            calf.getSicknessList().add(sickness);
            return calfService.updateCalf(idCalf, calf);
        } else {
            return null;
        }
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @PutMapping("/{idCalf}/medications/{idMedication}")
    private Calf medicationsCalf(@PathVariable("idCalf") final Long idCalf, @PathVariable("idMedication") final Long idMedication) {
        Optional<Calf> calfOptional = calfService.readOneCalf(idCalf);
        Optional <Medication>medicationOptional = medicationService.readOneMedication(idMedication);

        if (calfOptional.isPresent() && medicationOptional.isPresent()){
            Calf calf = calfOptional.get();
            Medication medication = medicationOptional.get();
            medication.getCattleList().add(calf);
            calf.getMedicationList().add(medication);
            return calfService.updateCalf(idCalf, calf);
        } else {
            return null;
        }
    }
}
