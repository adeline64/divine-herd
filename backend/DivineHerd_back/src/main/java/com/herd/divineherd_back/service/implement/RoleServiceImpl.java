package com.herd.divineherd_back.service.implement;

import com.herd.divineherd_back.entity.Role;
import com.herd.divineherd_back.repository.RoleRepository;
import com.herd.divineherd_back.service.RoleService;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class RoleServiceImpl implements RoleService {

    private RoleRepository roleRepository;

    public RoleServiceImpl(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @Override
    public Iterable<Role> readRole() {
        return roleRepository.findAll();
    }

    @Override
    public Optional<Role> readOneRole(Long id) {
        return roleRepository.findById(id);
    }
}
