package com.herd.divineherd_back.repository;

import com.herd.divineherd_back.entity.Bull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BullRepository extends JpaRepository<Bull,Long> {
}
