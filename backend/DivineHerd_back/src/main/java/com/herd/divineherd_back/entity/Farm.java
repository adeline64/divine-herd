package com.herd.divineherd_back.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Data;

import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "farm")
@Data
public class Farm {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @Column(nullable = false, name = "siren", length = 9)
    @Size(max = 9)
    private String siren;
    
    @Column(nullable = false, name = "siret", length = 14)
    @Size(max = 14)
    private String siret;
    
    @Column(nullable = false, name = "address")
    private String address;
    
    @Column(nullable = false, name = "zip_code",length = 5)
    @Size(max = 5)
    private String zipCode;
    
    @Column(nullable = false, name = "city")
    private String city;
    
    @ManyToMany
    @JoinTable(name = "farm_personal",
            joinColumns = @JoinColumn(name = "farm_id"),
            inverseJoinColumns = @JoinColumn(name = "personal_id")
    )
    private List<Personal> personalList = new ArrayList<>();

    @JsonIgnore
    @OneToMany(mappedBy = "farm")
    private List<Cattle> cattleList = new ArrayList<>();

    @JsonIgnore
    @OneToMany(mappedBy = "farm")
    private List<User> userList = new ArrayList<>();

    public Farm() {
    }

    public Farm(Long id, String siren, String siret, String address, String zipCode, String city, List<Personal> personalList) {
        this.id = id;
        this.siren = siren;
        this.siret = siret;
        this.address = address;
        this.zipCode = zipCode;
        this.city = city;
        this.personalList = personalList;
    }
}
