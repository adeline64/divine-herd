package com.herd.divineherd_back.enums;

public enum UnitName {
    MILLILITER,
    CENTILLITER,
    DECILITER,
    LITER,
    DECALITER,
    HECTOLITER,
    KILOLITER,
    MILLIGRAM,
    CENTIGRAM,
    DECIGRAM,
    GRAM,
    HECTOGRAM,
    DECAGRAM,
    KILOGRAM;
}
