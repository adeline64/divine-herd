package com.herd.divineherd_back.repository;

import com.herd.divineherd_back.entity.DairyTechnician;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DairyTechnicianRepository extends JpaRepository<DairyTechnician,Long> {
}
