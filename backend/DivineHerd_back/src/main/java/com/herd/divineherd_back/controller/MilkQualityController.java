package com.herd.divineherd_back.controller;

import com.herd.divineherd_back.entity.Cow;
import com.herd.divineherd_back.entity.MilkQuality;
import com.herd.divineherd_back.service.CowService;
import com.herd.divineherd_back.service.MilkQualityService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;
@RestController
@RequestMapping("/api/milkQualities")
public class MilkQualityController {

    private MilkQualityService milkQualityService;

    private CowService cowService;

    public MilkQualityController(MilkQualityService milkQualityService, CowService cowService) {
        this.milkQualityService = milkQualityService;
        this.cowService = cowService;
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @GetMapping()
    public Iterable<MilkQuality> readMilkQuality() {
        return milkQualityService.readMilkQuality();
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @GetMapping("/{id}")
    public Optional<MilkQuality> readOneMilkQuality(@PathVariable Long id) {
        return milkQualityService.readOneMilkQuantity(id);
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @PostMapping()
    public MilkQuality createMilkQuality(@RequestBody MilkQuality milkQuality) {
        return milkQualityService.createMilkQuality(milkQuality);
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @DeleteMapping("/{id}")
    public void deleteMilkQuality(@PathVariable Long id) {
        milkQualityService.deleteMilkQuality(id);
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @PutMapping("/{id}")
    public MilkQuality updateMilkQuality(@PathVariable Long id, @RequestBody MilkQuality milkQuality) {
        return milkQualityService.updateMilkQuality(id, milkQuality);
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @PutMapping("/{idMilkQuality}/cows/{idCow}")
    private MilkQuality milkQualityCow(@PathVariable("idMilkQuality") final Long idMilkQuality, @PathVariable("idCow") final Long idCow) {
        Optional<MilkQuality> milkQualityOptional = milkQualityService.readOneMilkQuantity(idMilkQuality);
        Optional <Cow>cowOptional = cowService.readOneCow(idCow);

        if (cowOptional.isPresent() && milkQualityOptional.isPresent()){
            Cow cow = cowOptional.get();
            MilkQuality milkQuality = milkQualityOptional.get();
            milkQuality.setCow(cow);
            cow.getMilkQualityList().add(milkQuality);
            return milkQualityService.updateMilkQuality(idMilkQuality, milkQuality);
        } else {
            return null;
        }
    }
}
