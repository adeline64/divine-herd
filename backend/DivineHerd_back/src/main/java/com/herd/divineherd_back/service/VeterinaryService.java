package com.herd.divineherd_back.service;

import com.herd.divineherd_back.entity.Veterinary;

import java.util.Optional;

public interface VeterinaryService {
    Iterable<Veterinary> readVeterinary();

    Optional<Veterinary> readOneVeterinary(Long id);

    Veterinary createVeterinary(Veterinary veterinary);

    void deleteVeterinary(Long id);

    Veterinary updateVeterinary(Long id, Veterinary veterinary);
}
