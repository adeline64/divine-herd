package com.herd.divineherd_back.service;

import com.herd.divineherd_back.entity.Medication;

import java.util.Optional;

public interface MedicationService {
    Iterable<Medication> readMedication();

    Optional<Medication> readOneMedication(Long id);

    Medication createMedication(Medication medication);

    void deleteMedication(Long id);

    Medication updateMedication(Long id, Medication medication);
}
