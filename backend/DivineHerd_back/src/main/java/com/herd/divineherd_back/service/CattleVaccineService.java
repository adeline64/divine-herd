package com.herd.divineherd_back.service;

import com.herd.divineherd_back.entity.CattleVaccine;

import java.util.Optional;

public interface CattleVaccineService
{
    Iterable<CattleVaccine> readCattleVaccine();

    Optional<CattleVaccine> readOneCattleVaccine(Long id);

    CattleVaccine createCattleVaccine(CattleVaccine cattleVaccine);

    void deleteCattleVaccine(Long id);

    CattleVaccine updateCattleVaccine(Long id, CattleVaccine cattleVaccine);
}
