package com.herd.divineherd_back.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.persistence.*;
import lombok.Data;

import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "user")
@Data
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false, name = "first_name")
    private String firstName;
    @Column(nullable = false, name = "last_name")
    private String lastName;
    @Temporal(TemporalType.DATE)
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Column(nullable = false, name = "date_of_birth")
    private Date dateOfBirth;
    @Column(nullable = false, name = "password")
    private String password;
    @Column(nullable = true, name = "telephone")
    private String telephone;
    @Column(nullable = false, name = "email", unique = true)
    private String email;
    @Column(nullable = false, name = "address")
    private String address;
    @Column(nullable = false, name = "zip_code",length = 5)
    @Size(max = 5)
    private String zipCode;
    @Column(nullable = false, name = "city")
    private String city;
    @ManyToMany
    @JoinTable(name = "user_role",
            joinColumns = @JoinColumn(name = "role_id",  referencedColumnName="ID"),
            inverseJoinColumns = @JoinColumn(name = "user_id",  referencedColumnName="ID")
    )
    private List<Role> roleList = new ArrayList<>();

    @ManyToOne
    @JoinColumn(name = "farm_id")
    private Farm farm;

    public User() {
    }

    public User(String firstName, String lastName, Date dateOfBirth, String password, String telephone, String email, String address, String zipCode, String city, List<Role> roleList) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.dateOfBirth = dateOfBirth;
        this.password = password;
        this.telephone = telephone;
        this.email = email;
        this.address = address;
        this.zipCode = zipCode;
        this.city = city;
        this.roleList = roleList;
    }

    public User(Long id, String firstName, String lastName, Date dateOfBirth, String password, String telephone, String email, String address, String zipCode, String city, List<Role> roleList, Farm farm) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.dateOfBirth = dateOfBirth;
        this.password = password;
        this.telephone = telephone;
        this.email = email;
        this.address = address;
        this.zipCode = zipCode;
        this.city = city;
        this.roleList = roleList;
        this.farm = farm;
    }
}
