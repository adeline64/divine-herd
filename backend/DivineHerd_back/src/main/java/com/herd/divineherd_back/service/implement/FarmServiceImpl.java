package com.herd.divineherd_back.service.implement;

import com.herd.divineherd_back.entity.Farm;
import com.herd.divineherd_back.repository.FarmRepository;
import com.herd.divineherd_back.service.FarmService;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class FarmServiceImpl implements FarmService {

    private FarmRepository farmRepository;

    public FarmServiceImpl(FarmRepository farmRepository) {
        this.farmRepository = farmRepository;
    }

    @Override
    public Iterable<Farm> readFarm() {
        return farmRepository.findAll();
    }

    @Override
    public Optional<Farm> readOneFarm(Long id) {
        return farmRepository.findById(id);
    }

    @Override
    public Farm createFarm(Farm farm) {
        return farmRepository.save(farm);
    }

    @Override
    public void deleteFarm(Long id) {
        farmRepository.deleteById(id);
    }

    @Override
    public Farm updateFarm(Long id, Farm farm) {
        var f = farmRepository.findById(id);
        if (f.isPresent()){
            var courentFarm = f.get();
            courentFarm.setSiren(farm.getSiren());
            courentFarm.setSiret(farm.getSiret());
            courentFarm.setAddress(farm.getAddress());
            courentFarm.setZipCode(farm.getZipCode());
            courentFarm.setCity(farm.getCity());
            courentFarm.setPersonalList(farm.getPersonalList());
            courentFarm.setCattleList(farm.getCattleList());
            courentFarm.setUserList(farm.getUserList());
            return farmRepository.save(courentFarm);
        }else {
            return null;
        }

    }
}
