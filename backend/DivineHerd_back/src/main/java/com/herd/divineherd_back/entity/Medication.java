package com.herd.divineherd_back.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "medication")
@Data
public class Medication {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = false, name = "name")
    private String name;

    @Column(name="description", columnDefinition="TEXT")
    private String description;

    @JsonIgnore
    @ManyToMany(mappedBy = "medicationList")
    private List<Cattle> cattleList = new ArrayList<>();

    @JsonIgnore
    @ManyToMany(mappedBy = "medicationList")
    private List<Sickness> sicknessesList = new ArrayList<>();

    public Medication() {
    }

    public Medication(Long id, String name, String description, List<Cattle> cattleList, List<Sickness> sicknessesList) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.cattleList = cattleList;
        this.sicknessesList = sicknessesList;
    }
}
