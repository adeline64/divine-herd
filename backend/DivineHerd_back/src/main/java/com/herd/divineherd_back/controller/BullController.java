package com.herd.divineherd_back.controller;

import com.herd.divineherd_back.entity.*;
import com.herd.divineherd_back.repository.CattleVaccineRepository;
import com.herd.divineherd_back.service.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api/cattles/bulls")
public class BullController {

    private BullService bullService;

    private FarmService farmService;

    private FoodService foodService;

    private RaceService raceService;

    private SicknessService sicknessService;

    private MedicationService medicationService;

    private VaccineService vaccineService;

    private CattleVaccineRepository cattleVaccineRepository;

    public BullController(BullService bullService, FarmService farmService, FoodService foodService, RaceService raceService, SicknessService sicknessService, MedicationService medicationService, VaccineService vaccineService, CattleVaccineRepository cattleVaccineRepository) {
        this.bullService = bullService;
        this.farmService = farmService;
        this.foodService = foodService;
        this.raceService = raceService;
        this.sicknessService = sicknessService;
        this.medicationService = medicationService;
        this.vaccineService = vaccineService;
        this.cattleVaccineRepository = cattleVaccineRepository;
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @GetMapping()
    public Iterable<Bull> readBull() {
        return bullService.readBull();
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @GetMapping("/{id}")
    public Optional<Bull> readOneBull(@PathVariable Long id) {
        return bullService.readOneBull(id);
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @PostMapping()
    public Bull createBull(@RequestBody Bull bull) {
        return bullService.createBull(bull);
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @DeleteMapping("/{id}")
    public void deleteBull(@PathVariable Long id) {
        bullService.deleteBull(id);
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @PutMapping("/{id}")
    public Bull updateBull(@PathVariable Long id, @RequestBody Bull bull) {
        return bullService.updateBull(id, bull);
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @PutMapping("/{idBull}/farms/{idFarm}")
    private Bull farmBull(@PathVariable("idBull") final Long idBull, @PathVariable("idFarm") final Long idFarm) {
        Optional<Bull> bullOptional = bullService.readOneBull(idBull);
        Optional <Farm>farmOptional = farmService.readOneFarm(idFarm);

        if (bullOptional.isPresent() && farmOptional.isPresent()){
            Bull bull = bullOptional.get();
            Farm farm = farmOptional.get();
            farm.getCattleList().add(bull);
            bull.setFarm(farm);
            return bullService.updateBull(idBull, bull);
        } else {
            return null;
        }
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @PutMapping("/{idBull}/foods/{idFood}")
    private Bull foodBull(@PathVariable("idBull") final Long idBull, @PathVariable("idFood") final Long idFood) {
        Optional<Bull> bullOptional = bullService.readOneBull(idBull);
        Optional <Food>foodOptional = foodService.readOneFood(idFood);

        if (bullOptional.isPresent() && foodOptional.isPresent()){
            Bull bull = bullOptional.get();
            Food food = foodOptional.get();
            food.getCattleList().add(bull);
            bull.getFoodList().add(food);
            return bullService.updateBull(idBull, bull);
        } else {
            return null;
        }
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @PutMapping("/{idBull}/races/{idRace}")
    private Bull raceBull(@PathVariable("idBull") final Long idBull, @PathVariable("idRace") final Long idRace) {
        Optional<Bull> bullOptional = bullService.readOneBull(idBull);
        Optional <Race>raceOptional = raceService.readOneRace(idRace);

        if (bullOptional.isPresent() && raceOptional.isPresent()){
            Bull bull = bullOptional.get();
            Race race = raceOptional.get();
            race.getCattleList().add(bull);
            bull.getRaceList().add(race);
            return bullService.updateBull(idBull, bull);
        } else {
            return null;
        }
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @PutMapping("/{idBull}/sickness/{idSickness}")
    private Bull sicknessBull(@PathVariable("idBull") final Long idBull, @PathVariable("idSickness") final Long idSickness) {
        Optional<Bull> bullOptional = bullService.readOneBull(idBull);
        Optional <Sickness>sicknessOptional = sicknessService.readOneSickness(idSickness);

        if (bullOptional.isPresent() && sicknessOptional.isPresent()){
            Bull bull = bullOptional.get();
            Sickness sickness = sicknessOptional.get();
            sickness.setCattle(bull);
            bull.getSicknessList().add(sickness);
            return bullService.updateBull(idBull, bull);
        } else {
            return null;
        }
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @PutMapping("/{idBull}/medications/{idMedication}")
    private Bull medicationsBull(@PathVariable("idBull") final Long idBull, @PathVariable("idMedication") final Long idMedication) {
        Optional<Bull> bullOptional = bullService.readOneBull(idBull);
        Optional <Medication>medicationOptional = medicationService.readOneMedication(idMedication);

        if (bullOptional.isPresent() && medicationOptional.isPresent()){
            Bull bull = bullOptional.get();
            Medication medication = medicationOptional.get();
            medication.getCattleList().add(bull);
            bull.getMedicationList().add(medication);
            return bullService.updateBull(idBull, bull);
        } else {
            return null;
        }
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @PutMapping("/{idBull}/vaccines/{idVaccine}")
    private Bull VaccinesBull(@PathVariable("idBull") final Long idBull, @PathVariable("idVaccine") final Long idVaccine) {
        Optional<Bull> bullOptional = bullService.readOneBull(idBull);
        Optional <Vaccine>vaccineOptional = vaccineService.readOneVaccine(idVaccine);

        if (bullOptional.isPresent() && vaccineOptional.isPresent()){
            Bull bull = bullOptional.get();
            Vaccine vaccine = vaccineOptional.get();
            //on crée une nouvelle instance
            CattleVaccine cattleVaccine = new CattleVaccine();
            // on associe le bétail et le vaccin
            cattleVaccine.setCattle(bull);
            cattleVaccine.setVaccine(vaccine);
            var cattleVaccineSave = cattleVaccineRepository.save(cattleVaccine);
            bull.getCattleVaccineList().add(cattleVaccineSave);
            return bullService.updateBull(idBull, bull);
        } else {
            return null;
        }
    }

}
