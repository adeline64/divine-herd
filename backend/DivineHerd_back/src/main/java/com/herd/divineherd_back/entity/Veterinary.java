package com.herd.divineherd_back.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Data;

@Entity
@Table(name = "veterinary")
@Data
public class Veterinary extends Personal {

    @Column(name = "clinic_name", nullable = false)
    private String clinicName;

    public Veterinary() {

    }
}
