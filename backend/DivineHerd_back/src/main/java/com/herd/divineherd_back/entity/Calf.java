package com.herd.divineherd_back.entity;

import com.herd.divineherd_back.enums.CalfStatus;
import jakarta.persistence.*;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Entity
@Table(name = "calf")
@Data
public class Calf extends Cattle {

    @Column(name="calf_status", nullable = false)
    @Enumerated(EnumType.STRING)
    CalfStatus calfStatus;

    @ManyToOne
    @JoinColumn(name = "cow_id")
    private Cow cow;

    public Calf() {
    }

    public Calf(Long id, String name, String color, Date dateOfBirth, Date dateOfDeath, Boolean horn, String numberJob, String numberPaper, List<Race> raceList, Farm farm, CalfStatus calfStatus, Cow cow) {
        super(id, name, color, dateOfBirth, dateOfDeath, horn, numberJob, numberPaper, raceList, farm);
        this.calfStatus = calfStatus;
        this.cow = cow;
    }
}
