package com.herd.divineherd_back.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "milk_quantity")
@Data
public class MilkQuantity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "number_morning")
    private float numberMorning;
    @Column(name = "number_afternoon")
    private float numberAfternoon;
    @Temporal(TemporalType.DATE)
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Column(nullable = false, name = "date_milk_quantity")
    private Date dateMilkQuantity;

    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "cow_id")
    private Cow cow;

    public MilkQuantity() {
    }

}
