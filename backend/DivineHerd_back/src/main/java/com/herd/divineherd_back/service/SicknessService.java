package com.herd.divineherd_back.service;

import com.herd.divineherd_back.entity.Sickness;

import java.util.Optional;

public interface SicknessService {
    Iterable<Sickness> readSickness();

    Optional<Sickness> readOneSickness(Long id);

    Sickness createSickness(Sickness sickness);

    void deleteSickness(Long id);

    Sickness updateSickness(Long id, Sickness sickness);
}
