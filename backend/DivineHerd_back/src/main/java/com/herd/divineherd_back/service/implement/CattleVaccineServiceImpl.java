package com.herd.divineherd_back.service.implement;

import com.herd.divineherd_back.entity.CattleVaccine;
import com.herd.divineherd_back.repository.CattleVaccineRepository;
import com.herd.divineherd_back.service.CattleVaccineService;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CattleVaccineServiceImpl implements CattleVaccineService {

    private CattleVaccineRepository cattleVaccineRepository;

    public CattleVaccineServiceImpl(CattleVaccineRepository cattleVaccineRepository) {
        this.cattleVaccineRepository = cattleVaccineRepository;
    }

    @Override
    public Iterable<CattleVaccine> readCattleVaccine() {
        return cattleVaccineRepository.findAll();
    }

    @Override
    public Optional<CattleVaccine> readOneCattleVaccine(Long id) {
        return cattleVaccineRepository.findById(id);
    }

    @Override
    public CattleVaccine createCattleVaccine(CattleVaccine cattleVaccine) {
        return cattleVaccineRepository.save(cattleVaccine);
    }

    @Override
    public void deleteCattleVaccine(Long id) {
        cattleVaccineRepository.deleteById(id);
    }

    @Override
    public CattleVaccine updateCattleVaccine(Long id, CattleVaccine cattleVaccine) {
        var CaVa = cattleVaccineRepository.findById(id);
        if (CaVa.isPresent()){
            var courentCattleVaccine = CaVa.get();
            courentCattleVaccine.setCattle(cattleVaccine.getCattle());
            courentCattleVaccine.setVaccine(cattleVaccine.getVaccine());
            courentCattleVaccine.setDateVaccine(cattleVaccine.getDateVaccine());
            return cattleVaccineRepository.save(courentCattleVaccine);
        }else {
            return null;
        }
    }
}
