package com.herd.divineherd_back.service.implement;

import com.herd.divineherd_back.entity.Role;
import com.herd.divineherd_back.entity.User;
import com.herd.divineherd_back.repository.UserRepository;
import com.herd.divineherd_back.repository.RoleRepository;
import com.herd.divineherd_back.service.UserService;
import org.springframework.stereotype.Service;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.*;

@Service
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;

    private RoleRepository roleRepository;

    public UserServiceImpl(UserRepository userRepository, RoleRepository roleRepository) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
    }

    @Override
    public List<User> readUser() {
        return userRepository.findAll();
    }

    @Override
    public Optional<User> readOneUser(Long id) {
        return userRepository.findById(id);
    }

    @Override
    public User createUser(User user) {
        Role userRole = roleRepository.findByName("USER");
        List<Role> roles = new ArrayList<>();
        roles.add(userRole);
        user.setRoleList(roles);

        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        user.setPassword(passwordEncoder.encode(user.getPassword()));

        return userRepository.save(user);
    }

    @Override
    public void deleteUser(Long id) {
        userRepository.deleteById(id);
    }

    @Override
    public User updateUser(Long id, User user) {
        var v = userRepository.findById(id);
        if (v.isPresent()){
            var courentUser = v.get();
            courentUser.setFirstName(user.getFirstName());
            courentUser.setLastName(user.getLastName());
            courentUser.setDateOfBirth(user.getDateOfBirth());
            courentUser.setPassword(user.getPassword());
            courentUser.setTelephone(user.getTelephone());
            courentUser.setAddress(user.getAddress());
            courentUser.setZipCode(user.getZipCode());
            courentUser.setCity(user.getCity());
            courentUser.setFarm(user.getFarm());
            courentUser.setRoleList(user.getRoleList());
            return userRepository.save(courentUser);
        }else {
            return null;
        }
    }
}
