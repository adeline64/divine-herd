package com.herd.divineherd_back.service;

import com.herd.divineherd_back.entity.Vaccine;

import java.util.Optional;

public interface VaccineService {

    Iterable<Vaccine> readVaccine();

    Optional<Vaccine> readOneVaccine(Long id);

    Vaccine createVaccine(Vaccine vaccine);

    void deleteVaccine(Long id);

    Vaccine updateVaccine(Long id, Vaccine vaccine);
}
