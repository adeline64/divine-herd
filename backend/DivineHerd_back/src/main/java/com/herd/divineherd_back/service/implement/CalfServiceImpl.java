package com.herd.divineherd_back.service.implement;

import com.herd.divineherd_back.entity.Calf;
import com.herd.divineherd_back.repository.CalfRepository;
import com.herd.divineherd_back.service.CalfService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CalfServiceImpl implements CalfService {

    private CalfRepository calfRepository;

    public CalfServiceImpl(CalfRepository calfRepository) {
        this.calfRepository = calfRepository;
    }

    @Override
    public List<Calf> readCalf() {
        return calfRepository.findAll();
    }

    @Override
    public Optional<Calf> readOneCalf(Long id) {
        return calfRepository.findById(id);
    }

    @Override
    public Calf createCalf(Calf calf) {
        return calfRepository.save(calf);
    }

    @Override
    public void deleteCalf(Long id) {
        calfRepository.deleteById(id);
    }

    @Override
    public Calf updateCalf(Long id, Calf calf) {
        var ca = calfRepository.findById(id);
        if (ca.isPresent()){
            var courentcalf = ca.get();
            courentcalf.setCalfStatus(calf.getCalfStatus());
            courentcalf.setName(calf.getName());
            courentcalf.setColor(calf.getColor());
            courentcalf.setDateOfBirth(calf.getDateOfBirth());
            courentcalf.setDateOfDeath(calf.getDateOfDeath());
            courentcalf.setHorn(calf.getHorn());
            courentcalf.setNumberJob(calf.getNumberJob());
            courentcalf.setNumberPaper(calf.getNumberPaper());
            courentcalf.setCow(calf.getCow());
            courentcalf.setFarm(calf.getFarm());
            courentcalf.setFoodList(calf.getFoodList());
            courentcalf.setRaceList(calf.getRaceList());
            courentcalf.setCattleVaccineList(calf.getCattleVaccineList());
            return calfRepository.save(courentcalf);
        }else {
            return null;
        }
    }
}
