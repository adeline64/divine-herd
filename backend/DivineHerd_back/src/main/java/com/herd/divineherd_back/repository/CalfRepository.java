package com.herd.divineherd_back.repository;

import com.herd.divineherd_back.entity.Calf;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CalfRepository extends JpaRepository<Calf,Long> {
}
