package com.herd.divineherd_back.service.implement;

import com.herd.divineherd_back.entity.Vaccine;
import com.herd.divineherd_back.repository.VaccineRepository;
import com.herd.divineherd_back.service.VaccineService;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class VaccineServiceImpl implements VaccineService {

    private VaccineRepository vaccineRepository;

    public VaccineServiceImpl(VaccineRepository vaccineRepository) {
        this.vaccineRepository = vaccineRepository;
    }

    @Override
    public Iterable<Vaccine> readVaccine() {
        return vaccineRepository.findAll();
    }

    @Override
    public Optional<Vaccine> readOneVaccine(Long id) {
        return vaccineRepository.findById(id);
    }

    @Override
    public Vaccine createVaccine(Vaccine vaccine) {
        return vaccineRepository.save(vaccine);
    }

    @Override
    public void deleteVaccine(Long id) {
        vaccineRepository.deleteById(id);
    }

    @Override
    public Vaccine updateVaccine(Long id, Vaccine vaccine) {
        var vac = vaccineRepository.findById(id);
        if (vac.isPresent()){
            var courentVaccine = vac.get();
            courentVaccine.setName(vaccine.getName());
            courentVaccine.setCattleVaccineList(vaccine.getCattleVaccineList());
            return vaccineRepository.save(courentVaccine);
        }else {
            return null;
        }
    }
}
