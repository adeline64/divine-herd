package com.herd.divineherd_back.controller;

import com.herd.divineherd_back.dto.LoginDto;
import com.herd.divineherd_back.entity.User;
import com.herd.divineherd_back.model.SecurityUser;
import com.herd.divineherd_back.service.TokenService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
public class AuthController {

    private final TokenService tokenService;

    private final AuthenticationManager authenticationManager;

    public AuthController(TokenService tokenService, AuthenticationManager authenticationManager) {
        this.tokenService = tokenService;
        this.authenticationManager = authenticationManager;
    }

    @GetMapping("/token")
    public ResponseEntity<?> token(Principal principal) {
        if(principal != null){
            // Code à exécuter si principal n'est pas null
            String email = principal.getName();
            return ResponseEntity.ok(email);
        }
        else{
            // Code à exécuter si principal est null
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }


    @PostMapping("/api/users/login")
    public ResponseEntity<String> login (@RequestBody LoginDto login, Principal principal) throws AuthenticationException {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(login.getEmail(), login.getPassword()));
        SecurityUser securityUser = (SecurityUser) authentication.getPrincipal();
        User user = securityUser.getUser();
        String token = tokenService.generateToken(authentication, user.getId());
        return ResponseEntity.ok(token);
    }

}
