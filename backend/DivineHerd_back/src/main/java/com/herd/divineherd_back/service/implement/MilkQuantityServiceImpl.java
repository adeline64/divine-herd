package com.herd.divineherd_back.service.implement;

import com.herd.divineherd_back.entity.MilkQuantity;
import com.herd.divineherd_back.repository.MilkQuantityRepository;
import com.herd.divineherd_back.service.MilkQuantityService;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class MilkQuantityServiceImpl implements MilkQuantityService {

    private MilkQuantityRepository milkQuantityRepository;

    public MilkQuantityServiceImpl(MilkQuantityRepository milkQuantityRepository) {
        this.milkQuantityRepository = milkQuantityRepository;
    }

    @Override
    public Iterable<MilkQuantity> readMilkQuantity() {
        return milkQuantityRepository.findAll();
    }

    @Override
    public Optional<MilkQuantity> readOneMilkQuantity(Long id) {
        return milkQuantityRepository.findById(id);
    }

    @Override
    public MilkQuantity createMilkQuantity(MilkQuantity milkQuantity) {
        return milkQuantityRepository.save(milkQuantity);
    }

    @Override
    public void deleteMilkQuantity(Long id) {
        milkQuantityRepository.deleteById(id);
    }

    @Override
    public MilkQuantity updateMilkQuantity(Long id, MilkQuantity milkQuantity) {
        var m = milkQuantityRepository.findById(id);
        if (m.isPresent()){
            var courentQuantity = m.get();
            courentQuantity.setNumberMorning(milkQuantity.getNumberMorning());
            courentQuantity.setNumberAfternoon(milkQuantity.getNumberAfternoon());
            courentQuantity.setDateMilkQuantity(milkQuantity.getDateMilkQuantity());
            courentQuantity.setCow(milkQuantity.getCow());
            return milkQuantityRepository.save(courentQuantity);
        }else {
            return null;
        }
    }
}
