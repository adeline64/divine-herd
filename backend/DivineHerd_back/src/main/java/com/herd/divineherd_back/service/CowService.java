package com.herd.divineherd_back.service;

import com.herd.divineherd_back.entity.Cow;

import java.util.Optional;

public interface CowService {
    Iterable<Cow> readCow();

    Optional<Cow> readOneCow(Long id);

    Cow createCow(Cow cow);

    void deleteCow(Long id);

    Cow updateCow(Long id, Cow cow);
}
