package com.herd.divineherd_back.service.implement;

import com.herd.divineherd_back.entity.Unit;
import com.herd.divineherd_back.repository.UnitRepository;
import com.herd.divineherd_back.service.UnitService;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UnitServiceImpl implements UnitService {

    private UnitRepository unitRepository;

    public UnitServiceImpl(UnitRepository unitRepository) {
        this.unitRepository = unitRepository;
    }

    @Override
    public Iterable<Unit> readUnit() {
        return unitRepository.findAll();
    }

    @Override
    public Optional<Unit> readOneUnit(Long id) {
        return unitRepository.findById(id);
    }

    @Override
    public Unit createUnit(Unit unit) {
        return unitRepository.save(unit);
    }
}
