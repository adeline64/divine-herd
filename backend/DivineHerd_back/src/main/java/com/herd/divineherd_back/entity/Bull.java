package com.herd.divineherd_back.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Entity
@Table(name = "bull")
@Data
public class Bull extends Cattle {

    public Bull() {
    }

    public Bull(Long id, String name, String color, Date dateOfBirth, Date dateOfDeath, Boolean horn, String numberJob, String numberPaper, List<Race> raceList, Farm farm) {
        super(id, name, color, dateOfBirth, dateOfDeath, horn, numberJob, numberPaper, raceList, farm);
    }
}
