package com.herd.divineherd_back;

import com.herd.divineherd_back.entity.Role;
import com.herd.divineherd_back.entity.User;
import com.herd.divineherd_back.repository.RoleRepository;
import com.herd.divineherd_back.repository.UserRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Date;
import java.util.List;

@SpringBootApplication
public class DivineHerdBackApplication {

	public static void main(String[] args) {
		SpringApplication.run(DivineHerdBackApplication.class, args);
	}
/*
	@Bean
	CommandLineRunner commandLineRunner(UserRepository users, RoleRepository roles,
										PasswordEncoder encoder) {
		return args -> {
			Role roleUser = roles.save(new Role("USER"));
			Role roleAdmin = roles.save(new Role("ADMIN"));
			users.save(new User("Marie","Bordenave", new Date(), encoder.encode("password"), "0756445677", "marie.bordenave@gmail.com", "3 rue de la promenade", "64000", "PAU", List.of(roleUser)));
			users.save(new User("Adeline","Flower", new Date(), encoder.encode("admin"), "6754778988", "adeline.pro64@gmail.com", "24 rue de la cèdre", "64000", "PAU", List.of(roleUser, roleAdmin)));
		};
		};

*/
	}



