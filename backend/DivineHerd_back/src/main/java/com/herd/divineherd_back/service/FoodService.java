package com.herd.divineherd_back.service;

import com.herd.divineherd_back.entity.Food;

import java.util.Optional;

public interface FoodService {
    Iterable<Food> readFood();

    Optional<Food> readOneFood(Long id);

    Food createFood(Food food);

    void deleteFood(Long id);

    Food updateFood(Long id, Food food);
}
