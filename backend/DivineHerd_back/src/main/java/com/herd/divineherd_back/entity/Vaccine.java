package com.herd.divineherd_back.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "vaccine")
@Data
public class Vaccine {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false, name = "name")
    private String name;

    @JsonIgnore
    @OneToMany(mappedBy = "vaccine")
    private List<CattleVaccine> cattleVaccineList = new ArrayList<>();

    public Vaccine() {
    }

}
