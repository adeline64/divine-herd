package com.herd.divineherd_back.service.implement;

import com.herd.divineherd_back.entity.Bull;
import com.herd.divineherd_back.repository.BullRepository;
import com.herd.divineherd_back.service.BullService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BullServiceImpl implements BullService {

    private BullRepository bullRepository;

    public BullServiceImpl(BullRepository bullRepository) {
        this.bullRepository = bullRepository;
    }

    @Override
    public List<Bull> readBull() {
        return bullRepository.findAll();
    }

    @Override
    public Optional<Bull> readOneBull(Long id) {
        return bullRepository.findById(id);
    }

    @Override
    public Bull createBull(Bull bull) {
        /*for (var race : bull.getRaceList()){
            System.out.println(race.getId());
        }*/
        return bullRepository.save(bull);
    }

    @Override
    public void deleteBull(Long id) {
        bullRepository.deleteById(id);
    }

    @Override
    public Bull updateBull(Long id, Bull bull) {
        var bu = bullRepository.findById(id);
        if (bu.isPresent()){
            var courentbull = bu.get();
            courentbull.setName(bull.getName());
            courentbull.setColor(bull.getColor());
            courentbull.setDateOfBirth(bull.getDateOfBirth());
            courentbull.setDateOfDeath(bull.getDateOfDeath());
            courentbull.setHorn(bull.getHorn());
            courentbull.setNumberJob(bull.getNumberJob());
            courentbull.setNumberPaper(bull.getNumberPaper());
            courentbull.setFarm(bull.getFarm());
            courentbull.setFoodList(bull.getFoodList());
            courentbull.setRaceList(bull.getRaceList());
            courentbull.setCattleVaccineList(bull.getCattleVaccineList());
            return bullRepository.save(courentbull);
        }else {
            return null;
        }
    }
}
