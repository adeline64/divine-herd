package com.herd.divineherd_back.service;

import com.herd.divineherd_back.entity.DairyTechnician;

import java.util.Optional;

public interface DairyTechnicianService {
    Iterable<DairyTechnician> readDairyTechnician();

    Optional<DairyTechnician> readOneDairyTechnician(Long id);

    DairyTechnician createDairyTechnician(DairyTechnician dairyTechnician);

    void deleteDairyTechnician(Long id);

    DairyTechnician updateDairyTechnician(Long id, DairyTechnician dairyTechnician);
}
