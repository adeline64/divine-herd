package com.herd.divineherd_back.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Data;

import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "personal")
@Data
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class Personal {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = false, name = "name_personal")
    private String namePersonal;
    @Column(nullable = false, name = "city")
    private String city;
    @Column(nullable = false, name = "address")
    private String address;
    @Column(nullable = false, name = "zip_code",length = 5)
    @Size(max = 5)
    private String zipCode;
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm")
    @Column(nullable = false, name = "passage_date")
    private Date passageDate;
    @Column(nullable = false, name = "passing_reason", columnDefinition="TEXT")
    private String passingReason;
    @JsonIgnore
    @ManyToMany(mappedBy = "personalList")
    private List<Farm> farmList = new ArrayList<>();
    public Personal() {
    }

    public Personal(Long id, String namePersonal, String city, String address, String zipCode, Date passageDate, String passingReason, List<Farm> farmList) {
        this.id = id;
        this.namePersonal = namePersonal;
        this.city = city;
        this.address = address;
        this.zipCode = zipCode;
        this.passageDate = passageDate;
        this.passingReason = passingReason;
        this.farmList = farmList;
    }
}
