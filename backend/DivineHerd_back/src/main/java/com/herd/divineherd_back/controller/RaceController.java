package com.herd.divineherd_back.controller;

import com.herd.divineherd_back.entity.Race;
import com.herd.divineherd_back.service.RaceService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api/races")
public class RaceController {

    private RaceService raceService;

    public RaceController(RaceService raceService) {
        this.raceService = raceService;
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @GetMapping()
    public Iterable<Race> readRace() {
        return raceService.readRace();
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @GetMapping("/{id}")
    public Optional<Race> readOneRace(@PathVariable Long id) {
        return raceService.readOneRace(id);
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @PostMapping()
    public Race createRace(@RequestBody Race race) {
        return raceService.createRace(race);
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @DeleteMapping("/{id}")
    public void deleteRace(@PathVariable Long id) {
        raceService.deleteRace(id);
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @PutMapping("/{id}")
    public Race updateRace(@PathVariable Long id, @RequestBody Race race) {
        return raceService.updateRace(id, race);
    }

}
