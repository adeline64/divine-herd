package com.herd.divineherd_back.repository;

import com.herd.divineherd_back.entity.MilkQuality;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MilkQualityRepository extends JpaRepository<MilkQuality,Long> {
}
