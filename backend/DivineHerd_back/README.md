# Divine Herd

## API Reference

# medication

**Get all medication**
----
Returns array json data.

* **URL**

      api/medications


* **url Params** : **Required**: None

    * **Data Params** : None

        * **Success Response:**

            * **Code:** 200 OK  
              **Content:**
          ```json
              [
                {
                  "id":1,
                  "name" : "BOVILIS BOVIGRIP",
                  "description": "réduire l'excrétion virale du virus Parainfluenza-3 bovin (PI-3)."
                },
                {
                  "id":2,
                  "name" : "BOVILIS® IBR Marker Inac",
                  "description": "immunisation active en vue de réduire l'intensité et la durée des signes cliniques (pyrexie) provoqués par le virus HVB-1 ainsi que la réplication et l'excrétion nasale du virus sauvage."
                }
              ]
          ```


* **Sample Call:**

  ```javascript
    fetch.ajax({
      url: "api/medications",
      dataType: "json",
      type : "GET",
      success : function(response) {
        console.log(response);
      }
    });
  ```        

---


**Get medication by id**
----
Returns json data about a single medication.

* **URL**

      api/medications/:id

* **Method:**

  `GET`

* **URL Params**

  **Required:**

  `id=[Long]`

* **Data Params**

  None

    * **Success Response:**

        * **Code:** 200  
          **Content:**
          ```json
                  {
                    "id":1,
                    "name" : "BOVILIS BOVIGRIP",
                    "description": "réduire l'excrétion virale du virus Parainfluenza-3 bovin (PI-3)."
                  }
          ```

* **Error Response:**

    * **Code:** 404 NOT FOUND  
      **Content:** `null`


* **Sample Call:**

  ```javascript
    fetch({
      url: "api/medications/1",
      dataType: "json",
      type : "GET",
      success : function(response) {
        console.log(response);
      }
    });
  ```

**Add medications**
----
Save a single medication in the data base.

* **URL**

      api/medications

* **Method:**

  `POST`

* **URL Params**

  None

* **Data Params**

  ```json
        {
          "name" : "CHORULON® 1500",
          "description": "stimulation de la libido."
        }
  ```

    * **Success Response:**

        * **Code:** 201 CREATED  
          **Content:**
          ```json
            {
              "id":3,
              "name" : "CHORULON® 1500",
              "description": "stimulation de la libido."
            }
          ```

* **Sample Call:**

  ```javascript
    fetch({
      url: "api/medications",
      dataType: "json",
      type : "POST",
      success : function(response) {
        console.log(response);
      }
    });
  ```

**Update medications**
----
Update json data about a single medication.

* **URL**

      api/medications/:id

* **Method:**

  ` PUT`

* **URL Params**

  **Required:**

  `id=[Long]`

* **Data Params**

  ```json
      {
        "name" : "CHORULON® 1500",
        "description": "stimulation de la libido"
      }
  ```

    * **Success Response:**

        * **Code:** 201 CREATED  
          **Content:**
          ```json
            {
               "id":3,
               "name" : "CHORULON® 1500",
               "description": "stimulation de la libido"
            }
          ```

* **Error Response:**

    * **Code:** 404 NOT FOUND  
      **Content:** `null`

* **Sample Call:**

  ```javascript
    fetch({
      url: "api/medications/3",
      dataType: "json",
      type : "PUT",
      success : function(response) {
        console.log(response);
      }
    });
  ```

**Delete medications**
----
Delete json data about a single medication.

* **URL**

      api/medication/:id

* **Method:**

  `DELETE`

* **URL Params**

  **Required:**

  `id=[Long]`

* **Data Params**

  None

* **Success Response:**

    * **Code:** 200 OK

* **Sample Call:**

  ```javascript
    fetch({
      url: "api/medications/1",
      dataType: "json",
      type : "DELETE",
      success : function(response) {
        console.log(response);
      }
    });
  ```

---

# sickness

**Get all sickness**
----
Returns array json data.

* **URL**

      api/sickness


* **url Params** : **Required**: None

    * **Data Params** : None

        * **Success Response:**

            * **Code:** 200 OK  
              **Content:**
          ```json
                  [
                      {
                          "id": 1,
                          "startDate": "2016-05-01",
                          "endDate": "2016-05-01",
                          "diseaseName": "virus orale",
                          "discardMilk": false,
                          "description": "doit être séparé du troupeau",
                          "medicationList": [
                              {
                              "id": 1,
                              "name": "HAPADEX®  Suspension orale 50 mg/mL",
                              "description": "nématodes gastro-intestinaux"
                              }
                          ]
                      },
                      {
                          "id": 2,
                          "startDate": "2016-05-01",
                          "endDate": "2016-05-01",
                          "diseaseName": "vérification de la présence du corps jaune",
                          "discardMilk": false,
                          "description": "doit être séparé du troupeau",
                          "medicationList": [
                              {
                              "id": 2,
                              "name": "ESTRUMATE® Flacon unidose",
                              "description": "induction de la lutéolyse permettant le déclenchement de l'œstrus et l'ovulation chez les femelles cyclées lors d'utilisation pendant le diœstrus."
                              }
                          ]
                      }
                  ]
          ```


* **Sample Call:**

  ```javascript
    fetch.ajax({
      url: "api/sickness",
      dataType: "json",
      type : "GET",
      success : function(response) {
        console.log(response);
      }
    });
  ```        

---


**Get sickness by id**
----
Returns json data about a single sickness.

* **URL**

      api/sickness/:id

* **Method:**

  `GET`

* **URL Params**

  **Required:**

  `id=[Long]`

* **Data Params**

  None

    * **Success Response:**

        * **Code:** 200  
          **Content:**
          ```json
                  {
                          "id": 1,
                          "startDate": "2016-05-01",
                          "endDate": "2016-05-01",
                          "diseaseName": "virus orale",
                          "discardMilk": false,
                          "description": "doit être séparé du troupeau",
                          "medicationList": [
                              {
                              "id": 1,
                              "name": "HAPADEX®  Suspension orale 50 mg/mL",
                              "description": "nématodes gastro-intestinaux"
                              }
                          ]
                  }
          ```

* **Error Response:**

    * **Code:** 404 NOT FOUND  
      **Content:** `null`


* **Sample Call:**

  ```javascript
    fetch({
      url: "api/sickness/1",
      dataType: "json",
      type : "GET",
      success : function(response) {
        console.log(response);
      }
    });
  ```

**Add sickness**
----
Save a single sickness in the data base.

* **URL**

      api/sickness

* **Method:**

  `POST`

* **URL Params**

  None

* **Data Params**

  ```json
        {
                          "startDate": "2016-05-01",
                          "endDate": "2016-05-01",
                          "diseaseName": "virus orale",
                          "discardMilk": 0,
                          "description": "doit être séparé du troupeau",
                          "medicationList": [
                              {
                              "id": 1
                              }
                          ]
        }
  ```

    * **Success Response:**

        * **Code:** 201 CREATED  
          **Content:**
          ```json
            {
                          "id": 1,
                          "startDate": "2016-05-01",
                          "endDate": "2016-05-01",
                          "diseaseName": "virus orale",
                          "discardMilk": false,
                          "description": "doit être séparé du troupeau",
                          "medicationList": [
                              {
                              "id": 1,
                              "name": null,
                              "description": null
                              }
                          ]
            }
          ```

* **Sample Call:**

  ```javascript
    fetch({
      url: "api/sickness",
      dataType: "json",
      type : "POST",
      success : function(response) {
        console.log(response);
      }
    });
  ```

**Update sickness**
----
Update json data about a single sickness.

* **URL**

      api/sickness/:id

* **Method:**

  ` PUT`

* **URL Params**

  **Required:**

  `id=[Long]`

* **Data Params**

  ```json
      {
                          "startDate": "2016-05-01",
                          "endDate": "2016-05-01",
                          "diseaseName": "virus orale",
                          "discardMilk": 1,
                          "description": "doit être séparé du troupeau",
                          "medicationList": [
                              {
                                "id": 1
                              }
                          ]
            }
  ```

    * **Success Response:**

        * **Code:** 201 CREATED  
          **Content:**
          ```json
            {
                          "id": 1,
                          "startDate": "2016-05-01",
                          "endDate": "2016-05-01",
                          "diseaseName": "virus orale",
                          "discardMilk": true,
                          "description": "doit être séparé du troupeau",
                          "medicationList": [
                              {
                                  "id": 1,
                                  "name": "HAPADEX®  Suspension orale 50 mg/mL",
                                  "description": "nématodes gastro-intestinaux"
                              }
                          ]
          }
          ```

* **Error Response:**

    * **Code:** 404 NOT FOUND  
      **Content:** `null`

* **Sample Call:**

  ```javascript
    fetch({
      url: "api/sickness/1",
      dataType: "json",
      type : "PUT",
      success : function(response) {
        console.log(response);
      }
    });
  ```

**Delete sickness**
----
Delete json data about a single sickness.

* **URL**

      api/sickness/:id

* **Method:**

  `DELETE`

* **URL Params**

  **Required:**

  `id=[Long]`

* **Data Params**

  None

* **Success Response:**

    * **Code:** 200 OK

* **Sample Call:**

  ```javascript
    fetch({
      url: "api/sickness/1",
      dataType: "json",
      type : "DELETE",
      success : function(response) {
        console.log(response);
      }
    });
  ```

# veterinary

**Get all veterinary**
----
Returns array json data.

* **URL**

      api/personals/veterinaries


* **url Params** : **Required**: None

    * **Data Params** : None

        * **Success Response:**

            * **Code:** 200 OK  
              **Content:**
          ```json
                  [
                      {
                          "id": 1,
                          "namePersonal": "julie Pierre",
                          "city": "Toulouse",
                          "address": "269 Rte de Narbonne",
                          "zipCode": 31400,
                          "passageDate": "2016-05-02 22:00",
                          "passingReason": "vache malade",
                          "clinicalName": "Clinique Vétérinaire Tolosane"
                      },
                      {
                          "id": 2,
                          "namePersonal": "Martine Dupont",
                          "city": "Toulouse",
                          "address": "8 Av. Léon Viala",
                          "zipCode": 31400,
                          "passageDate": "2016-05-02 22:00",
                          "passingReason": "vache blessée",
                          "clinicalName": "Clinique Vétérinaire de l'Orangeraie"
                      }
                  ]
          ```


* **Sample Call:**

  ```javascript
    fetch.ajax({
      url: "api/personals/veterinaries",
      dataType: "json",
      type : "GET",
      success : function(response) {
        console.log(response);
      }
    });
  ```        

---


**Get veterinary by id**
----
Returns json data about a single veterinary.

* **URL**

      api/personals/veterinaries/:id

* **Method:**

  `GET`

* **URL Params**

  **Required:**

  `id=[Long]`

* **Data Params**

  None

    * **Success Response:**

        * **Code:** 200  
          **Content:**
          ```json
                  {
                    "id": 1,
                    "namePersonal": "julie Pierre",
                    "city": "Toulouse",
                    "address": "269 Rte de Narbonne",
                    "zipCode": 31400,
                    "passageDate": "2016-05-02 22:00",
                    "passingReason": "vache malade",
                    "clinicalName": "Clinique Vétérinaire Tolosane"
                   }
          ```

* **Error Response:**

    * **Code:** 404 NOT FOUND  
      **Content:** `null`


* **Sample Call:**

  ```javascript
    fetch({
      url: "api/personals/veterinaies/1",
      dataType: "json",
      type : "GET",
      success : function(response) {
        console.log(response);
      }
    });
  ```

**Add veterinary**
----
Save a single veterinary in the data base.

* **URL**

      api/personnals/veterinaries

* **Method:**

  `POST`

* **URL Params**

  None

* **Data Params**

  ```json
        {
          "namePersonal": "Pierre Martin",
          "city": "Toulouse",
          "address": "269 Rte de Narbonne",
          "zipCode": 31400,
          "passageDate": "2016-05-02 22:00",
          "passingReason": "vache malade",
          "clinicalName": "Clinique Vétérinaire Tolosane"
         }
  ```

    * **Success Response:**

        * **Code:** 201 CREATED  
          **Content:**
          ```json
            {
              "id": 3,
              "namePersonal": "Pierre Martin",
              "city": "Toulouse",
              "address": "269 Rte de Narbonne",
              "zipCode": 31400,
              "passageDate": "2016-05-02 22:00",
              "passingReason": "vache malade",
              "clinicalName": "Clinique Vétérinaire Tolosane"
            }
          ```

* **Sample Call:**

  ```javascript
    fetch({
      url: "api/personals/veterinaries",
      dataType: "json",
      type : "POST",
      success : function(response) {
        console.log(response);
      }
    });
  ```

**Update veterinary**
----
Update json data about a single veterinary.

* **URL**

      api/personals/veterinaries/:id

* **Method:**

  ` PUT`

* **URL Params**

  **Required:**

  `id=[Long]`

* **Data Params**

  ```json
        {
          "namePersonal": "Pierre Martin",
          "city": "Toulouse",
          "address": "269 Route de Narbonne",
          "zipCode": 31400,
          "passageDate": "2016-05-02 22:00",
          "passingReason": "vache malade",
          "clinicalName": "Clinique Vétérinaire Tolosane"
        }
  ```

    * **Success Response:**

        * **Code:** 201 CREATED  
          **Content:**
          ```json
            {
              "id": 3,
              "namePersonal": "Pierre Martin",
              "city": "Toulouse",
              "address": "269 Route de Narbonne",
              "zipCode": 31400,
              "passageDate": "2016-05-02 22:00",
              "passingReason": "vache malade",
              "clinicalName": "Clinique Vétérinaire Tolosane"
            }
          ```

* **Error Response:**

    * **Code:** 404 NOT FOUND  
      **Content:** `null`

* **Sample Call:**

  ```javascript
    fetch({
      url: "api/personnals/veterinaries/3",
      dataType: "json",
      type : "PUT",
      success : function(response) {
        console.log(response);
      }
    });
  ```

**Delete veterinary**
----
Delete json data about a single veterinary.

* **URL**

      api/personals/veterinaries/:id

* **Method:**

  `DELETE`

* **URL Params**

  **Required:**

  `id=[Long]`

* **Data Params**

  None

* **Success Response:**

    * **Code:** 200 OK

* **Sample Call:**

  ```javascript
    fetch({
      url: "api/personals/veterinaries/1",
      dataType: "json",
      type : "DELETE",
      success : function(response) {
        console.log(response);
      }
    });
  ```

# Dairy Technicians

**Get all dairy technicians**
----
Returns array json data.

* **URL**

      api/personals/dairyTechnicians


* **url Params** : **Required**: None

    * **Data Params** : None

        * **Success Response:**

            * **Code:** 200 OK  
              **Content:**
          ```json
                  [
                      {
                          "id": 1,
                          "namePersonal": "Pierre Michel",
                          "city": "Toulouse",
                          "address": "28 Av. Antoine de Saint-Exupéry",
                          "zipCode": 31000,
                          "passageDate": "2016-05-02 22:00",
                          "passingReason": "Vérification annuel",
                          "dairyName": "Deux Chavanne Fromager Affineur"
                      },
                      {
                          "id": 2,
                          "namePersonal": "Martine Dupont",
                          "city": "Toulouse",
                          "address": "41 Pl. des Carmes",
                          "zipCode": 31000,
                          "passageDate": "2016-05-02 22:00",
                          "passingReason": "vérification du lait",
                          "dairyName": "Sena"
                      }
                  ]
          ```


* **Sample Call:**

  ```javascript
    fetch.ajax({
      url: "api/personals/dairyTechnicians",
      dataType: "json",
      type : "GET",
      success : function(response) {
        console.log(response);
      }
    });
  ```        

---


**Get dairyTechnicians by id**
----
Returns json data about a single dairyTechnicians.

* **URL**

      api/personals/dairyTechnicians/:id

* **Method:**

  `GET`

* **URL Params**

  **Required:**

  `id=[Long]`

* **Data Params**

  None

    * **Success Response:**

        * **Code:** 200  
          **Content:**
          ```json
                  {
                          "id": 2,
                          "namePersonal": "Martine Dupont",
                          "city": "Toulouse",
                          "address": "41 Pl. des Carmes",
                          "zipCode": 31000,
                          "passageDate": "2016-05-02 22:00",
                          "passingReason": "vérification du lait",
                          "dairyName": "Sena"
                  }
          ```

* **Error Response:**

    * **Code:** 404 NOT FOUND  
      **Content:** `null`


* **Sample Call:**

  ```javascript
    fetch({
      url: "api/personals/dairyTechnicians/2",
      dataType: "json",
      type : "GET",
      success : function(response) {
        console.log(response);
      }
    });
  ```

**Add dairyTechnicians**
----
Save a single dairyTechnicians in the data base.

* **URL**

      api/personnals/dairyTechnicians

* **Method:**

  `POST`

* **URL Params**

  None

* **Data Params**

  ```json
        {
                          "namePersonal": "Marie Jean",
                          "city": "Toulouse",
                          "address": "41 Pl. des Carmes",
                          "zipCode": 31000,
                          "passageDate": "2016-05-02 22:00",
                          "passingReason": "vérification du lait",
                          "dairyName": "Sena"
                  }
  ```

    * **Success Response:**

        * **Code:** 201 CREATED  
          **Content:**
          ```json
            {
              "id": 3,
              "namePersonal": "Marie Jean",
              "city": "Toulouse",
              "address": "269 Rte de Narbonne",
              "zipCode": 31400,
              "passageDate": "2016-05-02 22:00",
              "passingReason": "vérification du lait",
              "dairyName": "Sena"
          }
          ```

* **Sample Call:**

  ```javascript
    fetch({
      url: "api/personals/dairyTechnicians",
      dataType: "json",
      type : "POST",
      success : function(response) {
        console.log(response);
      }
    });
  ```

**Update dairyTechnicians**
----
Update json data about a single dairyTechnicians.

* **URL**

      api/personals/dairyTechnicians/:id

* **Method:**

  ` PUT`

* **URL Params**

  **Required:**

  `id=[Long]`

* **Data Params**

  ```json
        {
              "namePersonal": "Martine Jean",
              "city": "Toulouse",
              "address": "269 Rte de Narbonne",
              "zipCode": 31400,
              "passageDate": "2016-05-02 22:00",
              "passingReason": "vérification du lait",
              "dairyName": "Sena"
  }
  ```

    * **Success Response:**

        * **Code:** 201 CREATED  
          **Content:**
          ```json
            {
              "id": 3,
              "namePersonal": "Martine Jean",
              "city": "Toulouse",
              "address": "269 Rte de Narbonne",
              "zipCode": 31400,
              "passageDate": "2016-05-02 22:00",
              "passingReason": "vérification du lait",
              "dairyName": "Sena"
          }
          ```

* **Error Response:**

    * **Code:** 404 NOT FOUND  
      **Content:** `null`

* **Sample Call:**

  ```javascript
    fetch({
      url: "api/personnals/dairyTechnicians/3",
      dataType: "json",
      type : "PUT",
      success : function(response) {
        console.log(response);
      }
    });
  ```

**Delete dairyTechnicians**
----
Delete json data about a single dairyTechnicians.

* **URL**

      api/personals/dairyTechnicians/:id

* **Method:**

  `DELETE`

* **URL Params**

  **Required:**

  `id=[Long]`

* **Data Params**

  None

* **Success Response:**

    * **Code:** 200 OK

* **Sample Call:**

  ```javascript
    fetch({
      url: "api/personals/dairyTechnicians/1",
      dataType: "json",
      type : "DELETE",
      success : function(response) {
        console.log(response);
      }
    });
  ```

# race

**Get all race**
----
Returns array json data.

* **URL**

      api/races


* **url Params** : **Required**: None

    * **Data Params** : None

        * **Success Response:**

            * **Code:** 200 OK  
              **Content:**
          ```json
                  [
                      {
                          "id": 1,
                          "name": "Blonde d'Aquitaine",
                          "description": "vache à viande"
                      },
                      {
                          "id": 2,
                          "name": "Brune des Alpes",
                          "description": "lait"
                      }
                  ]
          ```


* **Sample Call:**

  ```javascript
    fetch.ajax({
      url: "api/races",
      dataType: "json",
      type : "GET",
      success : function(response) {
        console.log(response);
      }
    });
  ```        

---


**Get race by id**
----
Returns json data about a single race.

* **URL**

      api/races/:id

* **Method:**

  `GET`

* **URL Params**

  **Required:**

  `id=[Long]`

* **Data Params**

  None

    * **Success Response:**

        * **Code:** 200  
          **Content:**
          ```json
                  {
                    "id": 2,
                     "name": "Brune des Alpes",
                     "description": "lait"
                  }
          ```

* **Error Response:**

    * **Code:** 404 NOT FOUND  
      **Content:** `null`


* **Sample Call:**

  ```javascript
    fetch({
      url: "api/races/2",
      dataType: "json",
      type : "GET",
      success : function(response) {
        console.log(response);
      }
    });
  ```

**Add race**
----
Save a single race in the data base.

* **URL**

      api/races

* **Method:**

  `POST`

* **URL Params**

  None

* **Data Params**

  ```json
        {
          "name": "Guernsey",
          "description": "lait"
        }
  ```

    * **Success Response:**

        * **Code:** 201 CREATED  
          **Content:**
          ```json
            {
              "id": 3,
              "name": "Guernsey",
              "description": "lait"
            }
          ```

* **Sample Call:**

  ```javascript
    fetch({
      url: "api/races",
      dataType: "json",
      type : "POST",
      success : function(response) {
        console.log(response);
      }
    });
  ```

**Update race**
----
Update json data about a single race.

* **URL**

      api/races/:id

* **Method:**

  ` PUT`

* **URL Params**

  **Required:**

  `id=[Long]`

* **Data Params**

  ```json
        {
          "name": "Blonde d'Aquitaine",
          "description": "viande"
        }
  ```

    * **Success Response:**

        * **Code:** 201 CREATED  
          **Content:**
          ```json
            {
              "id": 1,
              "name": "Blonde d'Aquitaine",
              "description": "viande"
            }
          ```

* **Error Response:**

    * **Code:** 404 NOT FOUND  
      **Content:** `null`

* **Sample Call:**

  ```javascript
    fetch({
      url: "api/races/1",
      dataType: "json",
      type : "PUT",
      success : function(response) {
        console.log(response);
      }
    });
  ```

**Delete races**
----
Delete json data about a single races.

* **URL**

      api/races/:id

* **Method:**

  `DELETE`

* **URL Params**

  **Required:**

  `id=[Long]`

* **Data Params**

  None

* **Success Response:**

    * **Code:** 200 OK

* **Sample Call:**

  ```javascript
    fetch({
      url: "api/races/1",
      dataType: "json",
      type : "DELETE",
      success : function(response) {
        console.log(response);
      }
    });
  ```

# unit

**Get all unit**
----
Returns array json data.

* **URL**

      api/units


* **url Params** : **Required**: None

    * **Data Params** : None

        * **Success Response:**

            * **Code:** 200 OK  
              **Content:**
          ```json
                  [
                      {
                          "id":1,
                          "unitName": "centiliter"
                      },
                      {
                          "id":2,
                          "unitName": "kilogram"
                      }
                  ]
          ```


* **Sample Call:**

  ```javascript
    fetch.ajax({
      url: "api/units",
      dataType: "json",
      type : "GET",
      success : function(response) {
        console.log(response);
      }
    });
  ```        

---


**Get unit by id**
----
Returns json data about a single unit.

* **URL**

      api/units/:id

* **Method:**

  `GET`

* **URL Params**

  **Required:**

  `id=[Long]`

* **Data Params**

  None

    * **Success Response:**

        * **Code:** 200  
          **Content:**
          ```json
                  {
                          "id":2,
                          "unitName": "kilogram"
                      }
          ```

* **Error Response:**

    * **Code:** 404 NOT FOUND  
      **Content:** `null`


* **Sample Call:**

  ```javascript
    fetch({
      url: "api/units/2",
      dataType: "json",
      type : "GET",
      success : function(response) {
        console.log(response);
      }
    });
  ```

**Add unit**
----
Save a single unit in the data base.

* **URL**

      api/units

* **Method:**

  `POST`

* **URL Params**

  None

* **Data Params**

  ```json
        {
                          "unitName": "gram"
                      }
  ```

    * **Success Response:**

        * **Code:** 201 CREATED  
          **Content:**
          ```json
            {
              "id":3,
                          "unitName": "gram"
                      }
          ```

* **Sample Call:**

  ```javascript
    fetch({
      url: "api/units",
      dataType: "json",
      type : "POST",
      success : function(response) {
        console.log(response);
      }
    });
  ```

**Update unit**
----
Update json data about a single unit.

* **URL**

      api/units/:id

* **Method:**

  ` PUT`

* **URL Params**

  **Required:**

  `id=[Long]`

* **Data Params**

  ```json
      {
                          "unitName": "decigram"
                      }
  ```

    * **Success Response:**

        * **Code:** 201 CREATED  
          **Content:**
          ```json
            {
               "id":3,
                          "unitName": "decigram"
                      }
          ```

* **Error Response:**

    * **Code:** 404 NOT FOUND  
      **Content:** `null`

* **Sample Call:**

  ```javascript
    fetch({
      url: "api/units/3",
      dataType: "json",
      type : "PUT",
      success : function(response) {
        console.log(response);
      }
    });
  ```

**Delete unit**
----
Delete json data about a single unit.

* **URL**

      api/units/:id

* **Method:**

  `DELETE`

* **URL Params**

  **Required:**

  `id=[Long]`

* **Data Params**

  None

* **Success Response:**

    * **Code:** 200 OK

* **Sample Call:**

  ```javascript
    fetch({
      url: "api/units/1",
      dataType: "json",
      type : "DELETE",
      success : function(response) {
        console.log(response);
      }
    });
  ```

---

# food

**Get all food**
----
Returns array json data.

* **URL**

      api/foods


* **url Params** : **Required**: None

    * **Data Params** : None

        * **Success Response:**

            * **Code:** 200 OK  
              **Content:**
          ```json
                  [
                      {
                          "id":1,
                          "name":"foin",
                          "foodType" : "solid",
                          "description" : "beaucoup car bonne qualitée",
                          "unit": {
                            "id" : 1,
                            "unitName": "kilogram"
                          }
                      },
                      {
                          "id":2,
                          "name": "eau",
                          "foodType" : "liquid",
                          "description" : "beaucoup car bonne qualitée",
                          "unit": {
                            "id" : 2,
                            "unitName": "litter"
                          }
                      }
                  ]
          ```


* **Sample Call:**

  ```javascript
    fetch.ajax({
      url: "api/foods",
      dataType: "json",
      type : "GET",
      success : function(response) {
        console.log(response);
      }
    });
  ```        

---


**Get food by id**
----
Returns json data about a single food.

* **URL**

      api/foods/:id

* **Method:**

  `GET`

* **URL Params**

  **Required:**

  `id=[Long]`

* **Data Params**

  None

    * **Success Response:**

        * **Code:** 200  
          **Content:**
          ```json
                  {
                          "id":2,
                          "name": "eau",
                          "foodType" : "liquid",
                          "description" : "beaucoup car bonne qualitée",
                          "unit": {
                            "id" : 2,
                            "unitName": "litter"
                          }
                      }
          ```

* **Error Response:**

    * **Code:** 404 NOT FOUND  
      **Content:** `null`


* **Sample Call:**

  ```javascript
    fetch({
      url: "api/foods/2",
      dataType: "json",
      type : "GET",
      success : function(response) {
        console.log(response);
      }
    });
  ```

**Add food**
----
Save a single food in the data base.

* **URL**

      api/foods

* **Method:**

  `POST`

* **URL Params**

  None

* **Data Params**

  ```json
        {
                          "name": "lait",
                          "foodType" : "liquid",
                          "description" : "beaucoup car bonne qualitée",
                          "unit": {
                            "id" : 2,
                            "unitName": "litter"
                          }
                      }
  ```

    * **Success Response:**

        * **Code:** 201 CREATED  
          **Content:**
          ```json
            {
                          "id":3,
                          "name": "lait",
                          "foodType" : "liquid",
                          "description" : "beaucoup car bonne qualitée",
                          "unit": {
                            "id" : 2,
                            "unitName": "litter"
                          }
                      }
          ```

* **Sample Call:**

  ```javascript
    fetch({
      url: "api/foods",
      dataType: "json",
      type : "POST",
      success : function(response) {
        console.log(response);
      }
    });
  ```

**Update food**
----
Update json data about a single food.

* **URL**

      api/foods/:id

* **Method:**

  ` PUT`

* **URL Params**

  **Required:**

  `id=[Long]`

* **Data Params**

  ```json
      {
                          "name": "lait de vache",
                          "foodType" : "liquid",
                          "description" : "beaucoup car bonne qualitée",
                          "unit": {
                            "id" : 2,
                            "unitName": "litter"
                          }
                      }
  ```

    * **Success Response:**

        * **Code:** 201 CREATED  
          **Content:**
          ```json
            {
               "id":3,
                          "name": "lait de vache",
                          "foodType" : "liquid",
                          "description" : "beaucoup car bonne qualitée",
                          "unit": {
                            "id" : 2,
                            "unitName": "litter"
                          }
                      }
          ```

* **Error Response:**

    * **Code:** 404 NOT FOUND  
      **Content:** `null`

* **Sample Call:**

  ```javascript
    fetch({
      url: "api/foods/3",
      dataType: "json",
      type : "PUT",
      success : function(response) {
        console.log(response);
      }
    });
  ```

**Delete food**
----
Delete json data about a single food.

* **URL**

      api/foods/:id

* **Method:**

  `DELETE`

* **URL Params**

  **Required:**

  `id=[Long]`

* **Data Params**

  None

* **Success Response:**

    * **Code:** 200 OK

* **Sample Call:**

  ```javascript
    fetch({
      url: "api/foods/1",
      dataType: "json",
      type : "DELETE",
      success : function(response) {
        console.log(response);
      }
    });
  ```

---

# Milk Quality

**Get all milk quality**
----
Returns array json data.

* **URL**

      api/milkQualities


* **url Params** : **Required**: None

    * **Data Params** : None

        * **Success Response:**

            * **Code:** 200 OK  
              **Content:**
          ```json
                  [
                      {
                        "id": 1,
                        "fat": 1.5,
                        "protein": 1.0,
                        "dateMilkQuality": "2016-05-02"
                      },
                      {
                          "id": 2,
                          "fat": 2.5,
                          "protein": 1.8,
                          "dateMilkQuality": "2016-06-02"
                      }
                  ]
          ```


* **Sample Call:**

  ```javascript
    fetch.ajax({
      url: "api/milkQualities",
      dataType: "json",
      type : "GET",
      success : function(response) {
        console.log(response);
      }
    });
  ```        

---


**Get milkQualities by id**
----
Returns json data about a single milkQualities.

* **URL**

      api/milkQualities/:id

* **Method:**

  `GET`

* **URL Params**

  **Required:**

  `id=[Long]`

* **Data Params**

  None

    * **Success Response:**

        * **Code:** 200  
          **Content:**
          ```json
                  {
                          "id": 2,
                          "fat": 2.5,
                          "protein": 1.8,
                          "dateMilkQuality": "2016-06-02"
                  }
          ```

* **Error Response:**

    * **Code:** 404 NOT FOUND  
      **Content:** `null`


* **Sample Call:**

  ```javascript
    fetch({
      url: "api/milkQualities/2",
      dataType: "json",
      type : "GET",
      success : function(response) {
        console.log(response);
      }
    });
  ```

**Add milkQualities**
----
Save a single milkQualities in the data base.

* **URL**

      api/milkQualities

* **Method:**

  `POST`

* **URL Params**

  None

* **Data Params**

  ```json
        {
          "fat": 2.5,
          "protein": 1.8,
          "dateMilkQuality": "2016-06-02"
        }
  ```

    * **Success Response:**

        * **Code:** 201 CREATED  
          **Content:**
          ```json
            {
              "id": 3,
              "fat": 2.5,
              "protein": 1.8,
              "dateMilkQuality": "2016-06-02"
            }
          ```

* **Sample Call:**

  ```javascript
    fetch({
      url: "api/milkQualities",
      dataType: "json",
      type : "POST",
      success : function(response) {
        console.log(response);
      }
    });
  ```

**Update milkQualities**
----
Update json data about a single milkQualities.

* **URL**

      api/milkQualities/:id

* **Method:**

  ` PUT`

* **URL Params**

  **Required:**

  `id=[Long]`

* **Data Params**

  ```json
        {
              "fat": 3.5,
              "protein": 1.8,
              "dateMilkQuality": "2016-06-02"
        }
  ```

    * **Success Response:**

        * **Code:** 201 CREATED  
          **Content:**
          ```json
            {
              "id": 3,
              "fat": 3.5,
              "protein": 1.8,
              "dateMilkQuality": "2016-06-02"
            }
          ```

* **Error Response:**

    * **Code:** 404 NOT FOUND  
      **Content:** `null`

* **Sample Call:**

  ```javascript
    fetch({
      url: "api/milkQualities/3",
      dataType: "json",
      type : "PUT",
      success : function(response) {
        console.log(response);
      }
    });
  ```

**Delete milkQualities**
----
Delete json data about a single milkQualities.

* **URL**

      api/milkQualities/:id

* **Method:**

  `DELETE`

* **URL Params**

  **Required:**

  `id=[Long]`

* **Data Params**

  None

* **Success Response:**

    * **Code:** 200 OK

* **Sample Call:**

  ```javascript
    fetch({
      url: "api/milkQualities/1",
      dataType: "json",
      type : "DELETE",
      success : function(response) {
        console.log(response);
      }
    });
  ```

# Milk Quantity

**Get all milk Quantity**
----
Returns array json data.

* **URL**

      api/milkQuantities


* **url Params** : **Required**: None

    * **Data Params** : None

        * **Success Response:**

            * **Code:** 200 OK  
              **Content:**
          ```json
                  [
                      {
                          "id": 1,
                          "numberMorning": 34.0,
                          "numberAfternoon": 4.5,
                          "dateMilkQuantity": "2016-05-01"
                      },
                      {
                          "id": 2,
                          "numberMorning": 4.0,
                          "numberAfternoon": 4.5,
                          "dateMilkQuantity": "2016-05-01"
                      }
                  ]
          ```


* **Sample Call:**

  ```javascript
    fetch.ajax({
      url: "api/milkQuantities",
      dataType: "json",
      type : "GET",
      success : function(response) {
        console.log(response);
      }
    });
  ```        

---


**Get milkQuantities by id**
----
Returns json data about a single milkQuantities.

* **URL**

      api/milkQuantities/:id

* **Method:**

  `GET`

* **URL Params**

  **Required:**

  `id=[Long]`

* **Data Params**

  None

    * **Success Response:**

        * **Code:** 200  
          **Content:**
          ```json
                  {
                          "id": 2,
                          "numberMorning": 4.0,
                          "numberAfternoon": 4.5,
                          "dateMilkQuantity": "2016-05-01"
                  }
          ```

* **Error Response:**

    * **Code:** 404 NOT FOUND  
      **Content:** `null`


* **Sample Call:**

  ```javascript
    fetch({
      url: "api/milkQuantities/2",
      dataType: "json",
      type : "GET",
      success : function(response) {
        console.log(response);
      }
    });
  ```

**Add milkQuantities**
----
Save a single milkQuantities in the data base.

* **URL**

      api/milkQuantities

* **Method:**

  `POST`

* **URL Params**

  None

* **Data Params**

  ```json
        {
          "fat": 2.5,
          "protein": 1.8,
          "dateMilkQuality": "2016-06-02"
        }
  ```

    * **Success Response:**

        * **Code:** 201 CREATED  
          **Content:**
          ```json
            {
              "id": 3,
              "fat": 2.5,
              "protein": 1.8,
              "dateMilkQuality": "2016-06-02"
            }
          ```

* **Sample Call:**

  ```javascript
    fetch({
      url: "api/milkQualities",
      dataType: "json",
      type : "POST",
      success : function(response) {
        console.log(response);
      }
    });
  ```

**Update milkQualities**
----
Update json data about a single milkQualities.

* **URL**

      api/milkQualities/:id

* **Method:**

  ` PUT`

* **URL Params**

  **Required:**

  `id=[Long]`

* **Data Params**

  ```json
        {
              "fat": 3.5,
              "protein": 1.8,
              "dateMilkQuality": "2016-06-02"
        }
  ```

    * **Success Response:**

        * **Code:** 201 CREATED  
          **Content:**
          ```json
            {
              "id": 3,
              "fat": 3.5,
              "protein": 1.8,
              "dateMilkQuality": "2016-06-02"
            }
          ```

* **Error Response:**

    * **Code:** 404 NOT FOUND  
      **Content:** `null`

* **Sample Call:**

  ```javascript
    fetch({
      url: "api/milkQualities/3",
      dataType: "json",
      type : "PUT",
      success : function(response) {
        console.log(response);
      }
    });
  ```

**Delete milkQualities**
----
Delete json data about a single milkQualities.

* **URL**

      api/milkQualities/:id

* **Method:**

  `DELETE`

* **URL Params**

  **Required:**

  `id=[Long]`

* **Data Params**

  None

* **Success Response:**

    * **Code:** 200 OK

* **Sample Call:**

  ```javascript
    fetch({
      url: "api/milkQualities/1",
      dataType: "json",
      type : "DELETE",
      success : function(response) {
        console.log(response);
      }
    });
  ```

# Vaccines

**Get all vaccines**
----
Returns array json data.

* **URL**

      api/vaccines


* **url Params** : **Required**: None

    * **Data Params** : None

        * **Success Response:**

            * **Code:** 200 OK  
              **Content:**
          ```json
                  [
                      {
                          "id":1,
                          "name" : "BOVELA",
                          "vaccine": true
                      },
                      {
                          "id":2,
                          "name" : "virus Parainfluenza",
                          "vaccine": false
                      }
                  ]
          ```


* **Sample Call:**

  ```javascript
    fetch.ajax({
      url: "api/vaccines",
      dataType: "json",
      type : "GET",
      success : function(response) {
        console.log(response);
      }
    });
  ```        

---


**Get vaccine by id**
----
Returns json data about a single vaccine.

* **URL**

      api/vacciness/:id

* **Method:**

  `GET`

* **URL Params**

  **Required:**

  `id=[Long]`

* **Data Params**

  None

    * **Success Response:**

        * **Code:** 200  
          **Content:**
          ```json
                  {
                          "id":1,
                          "name" : "BOVELA",
                          "vaccine": true
                   }
          ```

* **Error Response:**

    * **Code:** 404 NOT FOUND  
      **Content:** `null`


* **Sample Call:**

  ```javascript
    fetch({
      url: "api/vaccines/1",
      dataType: "json",
      type : "GET",
      success : function(response) {
        console.log(response);
      }
    });
  ```

**Add vaccines**
----
Save a single vaccine in the data base.

* **URL**

      api/vaccines

* **Method:**

  `POST`

* **URL Params**

  None

* **Data Params**

  ```json
        {
          "id":3,
          "name" : "les bactéries Pasteurella",
          "vaccine": 1
        }
  ```

    * **Success Response:**

        * **Code:** 201 CREATED  
          **Content:**
          ```json
            {
              "id":3,
              "name" : "les bactéries Pasteurella",
              "vaccine": true
            }
          ```

* **Sample Call:**

  ```javascript
    fetch({
      url: "api/vaccines",
      dataType: "json",
      type : "POST",
      success : function(response) {
        console.log(response);
      }
    });
  ```

**Update vaccine**
----
Update json data about a single vaccine.

* **URL**

      api/vaccine/:id

* **Method:**

  ` PUT`

* **URL Params**

  **Required:**

  `id=[Long]`

* **Data Params**

  ```json
      {
        "name" : "les bactéries Pasteurella",
        "vaccine": 0
      }
  ```

    * **Success Response:**

        * **Code:** 201 CREATED  
          **Content:**
          ```json
            {
               "id":3,
               "name" : "les bactéries Pasteurella",
               "vaccine": false
            }
          ```

* **Error Response:**

    * **Code:** 404 NOT FOUND  
      **Content:** `null`

* **Sample Call:**

  ```javascript
    fetch({
      url: "api/vaccines/1",
      dataType: "json",
      type : "PUT",
      success : function(response) {
        console.log(response);
      }
    });
  ```

**Delete vaccines**
----
Delete json data about a single vaccine.

* **URL**

      api/vaccines/:id

* **Method:**

  `DELETE`

* **URL Params**

  **Required:**

  `id=[Long]`

* **Data Params**

  None

* **Success Response:**

    * **Code:** 200 OK

* **Sample Call:**

  ```javascript
    fetch({
      url: "api/vaccines/1",
      dataType: "json",
      type : "DELETE",
      success : function(response) {
        console.log(response);
      }
    });
  ```

---

# Farm

**Get all farm**
----
Returns array json data.

* **URL**

      api/farms


* **url Params** : **Required**: None

    * **Data Params** : None

        * **Success Response:**

            * **Code:** 200 OK  
              **Content:**
          ```json
                  [
                      {
                          "id": 1,
                          "siren": "345765897",
                          "siret": "34576589767540",
                          "address": "3 chemin du platane",
                          "zipCode": 64170,
                          "city": "Artix",
                          "personalList": []
                      },
                      {
                          "id": 2,
                          "siren": "327689768",
                          "siret": "32768976867546",
                          "address": "3 rue du bonheur",
                          "zipCode": 64170,
                          "city": "Artix",
                          "personalList": []
                      }
                  ]
          ```


* **Sample Call:**

  ```javascript
    fetch.ajax({
      url: "api/farms",
      dataType: "json",
      type : "GET",
      success : function(response) {
        console.log(response);
      }
    });
  ```        

---


**Get farm by id**
----
Returns json data about a single farm.

* **URL**

      api/farms/:id

* **Method:**

  `GET`

* **URL Params**

  **Required:**

  `id=[Long]`

* **Data Params**

  None

    * **Success Response:**

        * **Code:** 200  
          **Content:**
          ```json
                  {
                          "id": 2,
                          "siren": "327689768",
                          "siret": "32768976867546",
                          "address": "3 rue du bonheur",
                          "zipCode": 64170,
                          "city": "Artix",
                          "personalList": []
                      }
          ```

* **Error Response:**

    * **Code:** 404 NOT FOUND  
      **Content:** `null`


* **Sample Call:**

  ```javascript
    fetch({
      url: "api/farms/2",
      dataType: "json",
      type : "GET",
      success : function(response) {
        console.log(response);
      }
    });
  ```

**Add farms**
----
Save a single farm in the data base.

* **URL**

      api/farms

* **Method:**

  `POST`

* **URL Params**

  None

* **Data Params**

  ```json
        {
                          "siren": "658976845",
                          "siret": "658976845",
                          "address": "3 rue de la paix",
                          "zipCode": 64170,
                          "city": "Artix",
                          "personalList": []
                      }
  ```

    * **Success Response:**

        * **Code:** 201 CREATED  
          **Content:**
          ```json
              {
                          "id": 3,
                          "siren": "658976845",
                          "siret": "658976845",
                          "address": "3 rue de la paix",
                          "zipCode": 64170,
                          "city": "Artix",
                          "personalList": []
                      }
          ```

* **Sample Call:**

  ```javascript
    fetch({
      url: "api/farms",
      dataType: "json",
      type : "POST",
      success : function(response) {
        console.log(response);
      }
    });
  ```

**Update farm**
----
Update json data about a single farm.

* **URL**

      api/farms/:id

* **Method:**

  ` PUT`

* **URL Params**

  **Required:**

  `id=[Long]`

* **Data Params**

  ```json
        {
                          "siren": "658976845",
                          "siret": "658976845",
                          "address": "3 boulevard de la paix",
                          "zipCode": 64170,
                          "city": "Artix",
                          "personalList": []
                      }
  ```

    * **Success Response:**

        * **Code:** 201 CREATED  
          **Content:**
          ```json
            {
                          "id": 3,
                          "siren": "658976845",
                          "siret": "658976845",
                          "address": "3 boulevard de la paix",
                          "zipCode": 64170,
                          "city": "Artix",
                          "personalList": []
                      }
          ```

* **Error Response:**

    * **Code:** 404 NOT FOUND  
      **Content:** `null`

* **Sample Call:**

  ```javascript
    fetch({
      url: "api/farms/3",
      dataType: "json",
      type : "PUT",
      success : function(response) {
        console.log(response);
      }
    });
  ```

**Delete farm**
----
Delete json data about a single farm.

* **URL**

      api/farms/:id

* **Method:**

  `DELETE`

* **URL Params**

  **Required:**

  `id=[Long]`

* **Data Params**

  None

* **Success Response:**

    * **Code:** 200 OK

* **Sample Call:**

  ```javascript
    fetch({
      url: "api/farms/1",
      dataType: "json",
      type : "DELETE",
      success : function(response) {
        console.log(response);
      }
    });
  ```

**Update veterinary/farm**

Update json data about a single farm.

* **URL**

      api/farms/{idFarm}/veterinary/{idVeterinary}

* **Method:**

  `PUT`

* **URL Params**

  **Required:**

  `None`

* **Data Params**

  None

* **Success Response:**

    * **Code:** 200 OK

* **Error Response:**

    * **Code:** 404 NOT FOUND  
      **Content:** ` error : "La ferme n'a pas été modifié" `

* **Sample Call:**

  ```javascript
    fetch({
      url: "api/farms/1/veterinay/1",
      dataType: "json",
      type : "DELETE",
      success : function(response) {
        console.log(response);
      }
    });
  ```
  **Update farm/dairyTechnician**

Update json data about a single farm.

* **URL**

      api/farms/{idFarm}/dairyTechnician/{idDairyTechnician}

* **Method:**

  `PUT`

* **URL Params**

  **Required:**

  `None`

* **Data Params**

  None

* **Success Response:**

    * **Code:** 200 OK

* **Error Response:**

    * **Code:** 404 NOT FOUND  
      **Content:** ` error : "La ferme n'a pas été modifié" `

* **Sample Call:**

  ```javascript
    fetch({
      url: "api/farms/1/dairyTechnician/1",
      dataType: "json",
      type : "DELETE",
      success : function(response) {
        console.log(response);
      }
    });
  ```

# user

**Get all user**
----
Returns array json data.

* **URL**

      api/users


* **url Params** : **Required**: None

    * **Data Params** : None

        * **Success Response:**

            * **Code:** 200 OK  
              **Content:**
          ```json
                  [
                      {
                          "id": 1,
                          "firstName": "Julie",
                          "lastName": "Cabanot",
                          "dateOfBirth": "2012-03-04",
                          "password": "tooSre34*",
                          "telephone": "0354997622",
                          "email": "julie.cabanot@gmail.com",
                          "address": "3 rue de la promenade",
                          "zipCode": 64000,
                          "city": "PAU",
                          "roleList": [
                              {
                              "id": 2,
                              "name": "agriculteur"
                              }
                          ],
                          "farm": {
                              "id": 1,
                              "siren": "132764987",
                              "siret": "13276498706754",
                              "address": "10 rue de la victoire",
                              "zipCode": 64170,
                              "city": "Artix",
                              "personalList": []
                          }
                      },
                      {
                          "id": 2,
                          "firstName": "Jean",
                          "lastName": "Bordenave",
                          "dateOfBirth": "2002-03-04",
                          "password": "1ZEu437*",
                          "telephone": "0645328866",
                          "email": "jean@gmail.com",
                          "address": "3 rue du soleil",
                          "zipCode": 64170,
                          "city": "Labastide-cézeracq",
                          "roleList": [
                              {
                              "id": 2,
                              "name": "agriculteur"
                              }
                          ],
                          "farm": {
                              "id": 1,
                              "siren": "178905467",
                              "siret": "17890546723456",
                              "address": "3 rue du soleil",
                              "zipCode": 64170,
                              "city": "Labastide-cézeracq",
                              "personalList": []
                          }
                      }
                  ]
          ```


* **Sample Call:**

  ```javascript
    fetch.ajax({
      url: "api/users",
      dataType: "json",
      type : "GET",
      success : function(response) {
        console.log(response);
      }
    });
  ```        

---


**Get user by id**
----
Returns json data about a single user.

* **URL**

      api/users/:id

* **Method:**

  `GET`

* **URL Params**

  **Required:**

  `id=[Long]`

* **Data Params**

  None

    * **Success Response:**

        * **Code:** 200  
          **Content:**
          ```json
                  {
                          "id": 1,
                          "firstName": "Julie",
                          "lastName": "Cabanot",
                          "dateOfBirth": "2012-03-04",
                          "password": "tooSre34*",
                          "telephone": "0354997622",
                          "email": "julie.cabanot@gmail.com",
                          "address": "3 rue de la promenade",
                          "zipCode": 64000,
                          "city": "PAU",
                          "roleList": [
                              {
                              "id": 2,
                              "name": "agriculteur"
                              }
                          ],
                          "farm": {
                              "id": 1,
                              "siren": "132764987",
                              "siret": "13276498706754",
                              "address": "10 rue de la victoire",
                              "zipCode": 64170,
                              "city": "Artix",
                              "personalList": []
                          }
                      }
          ```

* **Error Response:**

    * **Code:** 404 NOT FOUND  
      **Content:** `null`


* **Sample Call:**

  ```javascript
    fetch({
      url: "api/users/1",
      dataType: "json",
      type : "GET",
      success : function(response) {
        console.log(response);
      }
    });
  ```

**Add user**
----
Save a single user in the data base.

* **URL**

      api/users

* **Method:**

  `POST`

* **URL Params**

  None

* **Data Params**

  ```json
        {
          "firstName": "Gilles",
          "lastName": "Cabanot",
          "dateOfBirth": "2005-03-04",
          "password": "tyu980ytT*M",
          "telephone": "0989768877",
          "email": "gilles.cabanot@gmail.com",
          "address": "3 rue de la campagne",
          "zipCode": 64000,
          "city": "PAU",
          "roleList": [
            {
              "id": 2
            }
          ],
          "farm": {
            "id": 1
          }
        }
  ```

    * **Success Response:**

        * **Code:** 201 CREATED  
          **Content:**
          ```json
            {
              "id": 3,
              "firstName": "Gilles",
              "lastName": "Cabanot",
              "dateOfBirth": "2005-03-04",
              "password": "tyu980ytT*M",
              "telephone": "0989768877",
              "email": "gilles.cabanot@gmail.com",
              "address": "3 rue de la campagne",
              "zipCode": 64000,
              "city": "PAU",
              "roleList": [
                {
                  "id": 2,
                  "name": "agriculteur"
                }
              ],
              "farm": {
                "id": 1,
                "siren": "132764987",
                "siret": "13276498706754",
                "address": "10 rue de la victoire",
                "zipCode": 64170,
                "city": "Artix",
                "personalList": []
              }
          }
          ```

* **Sample Call:**

  ```javascript
    fetch({
      url: "api/users",
      dataType: "json",
      type : "POST",
      success : function(response) {
        console.log(response);
      }
    });
  ```

**Update user**
----
Update json data about a single user.

* **URL**

      api/users/:id

* **Method:**

  ` PUT`

* **URL Params**

  **Required:**

  `id=[Long]`

* **Data Params**

  ```json
        {
              "firstName": "Gilles",
              "lastName": "Cabanot",
              "dateOfBirth": "2005-03-04",
              "password": "tyu980ytT*M",
              "telephone": "0989768877",
              "email": "gilles.cabanot@gmail.com",
              "address": "3 rue de la campagne",
              "zipCode": 64000,
              "city": "PAU",
              "roleList": [
                {
                  "id": 1
                }
              ],
              "farm": {
                "id": 1
              }
          }
  ```

    * **Success Response:**

        * **Code:** 201 CREATED  
          **Content:**
          ```json
            {
              "id": 3,
              "firstName": "Gilles",
              "lastName": "Cabanot",
              "dateOfBirth": "2005-03-04",
              "password": "tyu980ytT*M",
              "telephone": "0989768877",
              "email": "gilles.cabanot@gmail.com",
              "address": "3 rue de la campagne",
              "zipCode": 64000,
              "city": "PAU",
              "roleList": [
                {
                  "id": 1,
                  "name": "admin"
                }
              ],
              "farm": {
                  "id": 1,
                  "siren": null,
                  "siret": null,
                  "address": null,
                  "zipCode": 0,
                  "city": null,
                  "personalList": []
              }
          }
          ```

* **Error Response:**

    * **Code:** 404 NOT FOUND  
      **Content:** `null`

* **Sample Call:**

  ```javascript
    fetch({
      url: "api/users/3",
      dataType: "json",
      type : "PUT",
      success : function(response) {
        console.log(response);
      }
    });
  ```

**Delete user**
----
Delete json data about a single user.

* **URL**

      api/users/:id

* **Method:**

  `DELETE`

* **URL Params**

  **Required:**

  `id=[Long]`

* **Data Params**

  None

* **Success Response:**

    * **Code:** 200 OK

* **Sample Call:**

  ```javascript
    fetch({
      url: "api/users/1",
      dataType: "json",
      type : "DELETE",
      success : function(response) {
        console.log(response);
      }
    });
  ```

**Update user/role**

Update json data about a single user.

* **URL**

      api/users/{iduser}/roles/{idRole}

* **Method:**

  `PUT`

* **URL Params**

  **Required:**

  `None`

* **Data Params**

  None

* **Success Response:**

    * **Code:** 200 OK

* **Error Response:**

    * **Code:** 404 NOT FOUND  
      **Content:** ` error : "Le user n'a pas été modifié" `

* **Sample Call:**

  ```javascript
    fetch({
      url: "api/users/1/roles/1",
      dataType: "json",
      type : "DELETE",
      success : function(response) {
        console.log(response);
      }
    });
  ```

**Update user/farm**

Update json data about a single bull.

* **URL**

      api/users/{idUser}/farms/{idFarm}

* **Method:**

  `PUT`

* **URL Params**

  **Required:**

  `None`

* **Data Params**

  None

* **Success Response:**

    * **Code:** 200 OK

* **Error Response:**

    * **Code:** 404 NOT FOUND  
      **Content:** ` error : "Le user n'a pas été modifié" `

* **Sample Call:**

  ```javascript
    fetch({
      url: "api/users/1/farms/1",
      dataType: "json",
      type : "DELETE",
      success : function(response) {
        console.log(response);
      }
    });
  ```

# Bull

**Get all bulls**
----
Returns array json data.

* **URL**

      api/cattles/bulls


* **url Params** : **Required**: None

    * **Data Params** : None

        * **Success Response:**

            * **Code:** 200 OK  
              **Content:**
          ```json
                  [
                      {
                          "id":1,
                          "name":"Ponpon",
                          "color":"maron",
                          "dateOfBirth": "2022-01-01 09:30",
                          "dateOfDeath": null,
                          "horn": 1,
                          "numberJob": "9845",
                          "numberPaper": "2859609845",
                          "foodList" : [
                            {
                              "id":1,
                              "name": "eau",
                              "foodType": "liquid",
                              "description": "il faut limiter la quantitée d'eau",
                              "unit": 
                                {
                                  "id": 1,
                                  "unitName": "centiliter"
                                }
                            },
                            {
                              "id": 2,
                              "name": "foin",
                              "foodType": "solid",
                              "description": "nourriture à volonté",
                              "unit": 
                                {
                                  "id": 2,
                                  "unitName": "kilogram"
                                }
                            }
                          ],
                          "raceList": [
                            { 
                              "id":1,
                              "name": "Brun des Alpes",
                              "description": "La race Brune, originaire de Suisse, d'où le nom de Brune des Alpes"
                            }
                          ],
                          "medicationList": [
                            {
                              "id":1,
                              "name": "parafilariose bovine",
                              "description": "maladie des sueurs de sang"
                            }
                          ],
                          "farm" : 
                            {
                              "id":1,
                              "siren" : "123456789",
                              "siret" : "12345678967546",
                              "address" : "3 rue de la promenade",
                              "zipCode" : 34567,
                              "city": "Saint-Bruno-De-Montarville",
                              "personalList": [
                                {
                                  "id": 1,
                                  "namePersonal": "Jules Bordenave",
                                  "city": "Saint-Bruno-De-Montarville",
                                  "address": "78 rue des champs",
                                  "zipCode": 34567,
                                  "passageDate": "2016-05-02 22:00",
                                  "passingReason": "le taureau a de la fièvre",
                                  "clinicalName": "chez Jules"
                              }
                             ]
                            },
                          "sickness": 
                            {
                              "id":1,
                              "startDate": "2016-05-01",
                              "endDate": "2016-05-01",
                              "diseaseName": "grippe",
                              "discardMilk": false,
                              "description" : "il va vite s'en remettre",
                              "medicationList": [
                                {
                                  "id": 2,
                                  "name": "pommade",
                                  "description": "pommade pour soigner les cornes"
                                }
                              ]
                            },
                          "cattleVaccineList": [
                            {
                            }
                          ]
                      },
                      {
                          "id":2,
                          "name":"Neron",
                          "color":"maron",
                          "dateOfBirth": "2022-01-01 09:30",
                          "dateOfDeath": null,
                          "horn": 1,
                          "numberJob": "4356",
                          "numberPaper": "2859604356",
                          "foodList" : [
                            {
                              "id":1,
                              "name": "eau",
                              "foodType": "liquid",
                              "description": "il faut limiter la quantitée d'eau",
                              "unit": 
                                {
                                  "id": 1,
                                  "unitName": "centiliter"
                                }
                            },
                            {
                              "id": 2,
                              "name": "foin",
                              "foodType": "solid",
                              "description": "nourriture à volonté",
                              "unit": 
                                {
                                  "id": 2,
                                  "unitName": "kilogram"
                                }
                            }
                          ],
                          "raceList": [
                            { 
                              "id":1,
                              "name": "Brun des Alpes",
                              "description": "La race Brune, originaire de Suisse, d'où le nom de Brune des Alpes"
                            }
                          ],
                          "medicationList": [
                            {
                              "id":1,
                              "name": "parafilariose bovine",
                              "description": "maladie des sueurs de sang"
                            }
                          ],
                          "farm" : 
                            {
                              "id":1,
                              "siren" : "123456789",
                              "siret" : "12345678967546",
                              "address" : "3 rue de la promenade",
                              "zipCode" : 34567,
                              "city": "Saint-Bruno-De-Montarville",
                              "personalList": [
                                {
                                  "id": 1,
                                  "namePersonal": "Jules Bordenave",
                                  "city": "Saint-Bruno-De-Montarville",
                                  "address": "78 rue des champs",
                                  "zipCode": 34567,
                                  "passageDate": "2016-05-02 22:00",
                                  "passingReason": "le taureau a de la fièvre",
                                  "clinicalName": "chez Jules"
                              }
                             ]
                            },
                          "sickness": 
                            {
                              "id":1,
                              "startDate": "2016-05-01",
                              "endDate": "2016-05-01",
                              "diseaseName": "grippe",
                              "discardMilk": false,
                              "description" : "il va vite s'en remettre",
                              "medicationList": [
                                {
                                  "id": 2,
                                  "name": "pommade",
                                  "description": "pommade pour soigner les cornes"
                                }
                              ]
                            },
                          "cattleVaccineList": [
                            {
                            }
                          ]
                      }
                  ]
          ```

* **Sample Call:**

  ```javascript
    fetch.ajax({
      url: "api/cattles/bulls",
      dataType: "json",
      type : "GET",
      success : function(response) {
        console.log(response);
      }
    });
  ```        

---

**Get bull by id**
----
Returns json data about a single bull.

* **URL**

      api/cattles/bulls/:id

* **Method:**

  `GET`

* **URL Params**

  **Required:**

  `id=[Long]`

* **Data Params**

  None

    * **Success Response:**

        * **Code:** 200  
          **Content:**
          ```json
                  {
                        "id":1,
                        "name":"Ponpon",
                        "color":"maron",
                        "dateOfBirth": "2022-01-01 09:30",
                        "dateOfDeath": null,
                        "horn": 1,
                        "numberJob": "9845",
                        "numberPaper": "2859609845",
                        "foodList" : [
                          {
                            "id":1,
                            "name": "eau",
                            "foodType": "liquid",
                            "description": "il faut limiter la quantitée d'eau",
                            "unit": 
                              {
                                "id": 1,
                                "unitName": "centiliter"
                              }
                          },
                          {
                            "id": 2,
                            "name": "foin",
                            "foodType": "solid",
                            "description": "nourriture à volonté",
                            "unit": 
                              {
                                "id": 2,
                                "unitName": "kilogram"
                              }
                          }
                        ],
                        "raceList": [
                          { 
                            "id":1,
                            "name": "Brun des Alpes",
                            "description": "La race Brune, originaire de Suisse, d'où le nom de Brune des Alpes"
                          }
                        ],
                        "medicationList": [
                          {
                            "id":1,
                            "name": "parafilariose bovine",
                            "description": "maladie des sueurs de sang"
                          }
                        ],
                        "farm" : 
                          {
                            "id":1,
                            "siren" : "123456789",
                            "siret" : "12345678967546",
                            "address" : "3 rue de la promenade",
                            "zipCode" : 34567,
                            "city": "Saint-Bruno-De-Montarville",
                            "personalList": [
                              {
                                "id": 1,
                                "namePersonal": "Jules Bordenave",
                                "city": "Saint-Bruno-De-Montarville",
                                "address": "78 rue des champs",
                                "zipCode": 34567,
                                "passageDate": "2016-05-02 22:00",
                                "passingReason": "le taureau a de la fièvre",
                                "clinicalName": "chez Jules"
                            }
                           ]
                          },
                        "sickness": 
                          {
                            "id":1,
                            "startDate": "2016-05-01",
                            "endDate": "2016-05-01",
                            "diseaseName": "grippe",
                            "discardMilk": false,
                            "description" : "il va vite s'en remettre",
                            "medicationList": [
                              {
                                "id": 2,
                                "name": "pommade",
                                "description": "pommade pour soigner les cornes"
                              }
                            ]
                          },
                        "cattleVaccineList": [
                          {
                          }
                        ]
                    }
          ```

* **Error Response:**

    * **Code:** 404 NOT FOUND  
      **Content:** `null`


* **Sample Call:**

  ```javascript
    fetch({
      url: "api/cattles/bulls/1",
      dataType: "json",
      type : "GET",
      success : function(response) {
        console.log(response);
      }
    });
  ```

**Add bull**
----
Save a single bull in the data base.

* **URL**

      api/cattles/bulls

* **Method:**

  `POST`

* **URL Params**

  None

* **Data Params**

  ```json
    {
      "name":"Caliban",
      "color":"maron",
      "dateOfBirth": "2022-01-01 09:30",
      "dateOfDeath": "",
      "horn": 1,
      "numberJob": "9445",
      "numberPaper": "2859609445",
      "foodList" : [
        {
          "id":1
        },
        {
          "id": 2
        }
      ],
      "raceList": [
        { 
          "id":1
        }
      ],
      "medicationList": [
        {
          "id":1
        }
      ],
      "farm" : 
        {
          "id":1
        },
      "sickness": 
        {
          "id":1
        },
      "cattleVaccineList": [
        {
          "id":1
        }
      ]
    }
  ```

    * **Success Response:**

        * **Code:** 201 CREATED  
          **Content:**
          ```json
            {
                    "id":3,
                    "name":"Caliban",
                    "color":"maron",
                    "dateOfBirth": "2022-01-01 09:30",
                    "dateOfDeath": "",
                    "horn": 1,
                    "numberJob": "9845",
                    "numberPaper": "2859609845",
                    "foodList" : [
                      {
                        "id":1,
                        "name": null,
                        "foodType": null,
                        "description": null,
                        "unit": null
                      },
                      {
                       "id": 2,
                       "name": null,
                       "foodType": null,
                       "description": null,
                       "unit": null
                      }
                    ],
                    "raceList": [
                      { 
                        "id":1,
                        "name": null,
                        "description": null
                      }
                    ],
                    "medicationList": [
                      {
                        "id":1,
                        "name": null,
                        "description": null
                      }
                    ],
                    "farm" : 
                      {
                       "id":1,
                       "siren" : null,
                       "siret" : null,
                       "address" : null,
                       "zipCode" : null,
                       "city": null,
                       "personalList": []
                     },
                     "sickness": 
                      {
                       "id":1,
                       "startDate": null,
                       "endDate": null,
                       "diseaseName": null,
                       "discardMilk": false,
                       "description" : null,
                       "medicationList": []
                     },
                    "cattleVaccineList": [
                      {
                          "id": 1,
                          "dateVaccine": null,
                          "cattle": null,
                          "vaccine": null
                      }
                     ]
                    }
          ```

* **Sample Call:**

  ```javascript
    fetch({
      url: "api/cattles/bulls",
      dataType: "json",
      type : "POST",
      success : function(response) {
        console.log(response);
      }
    });
  ```

**Update bull**
----
Update json data about a single bull.

* **URL**

      api/cattles/bulls/:id

* **Method:**

  ` PUT`

* **URL Params**

  **Required:**

  `id=[Long]`

* **Data Params**

  ```json
    {
      "name":"Ponpon",
      "color":"maron",
      "dateOfBirth": "2022-01-01 09:30",
      "dateOfDeath": "",
      "horn": 1,
      "numberJob": "9845",
      "numberPaper": "2859609845",
      "foodList" : [
        {
          "id":1
        },
        {
          "id": 2
        }
      ],
      "raceList": [
        { 
          "id":1
        }
      ],
      "medicationList": [
        {
          "id":1
        }
      ],
      "farm" : 
        {
          "id":1
        },
      "sickness": 
        {
          "id":1
        },
      "cattleVaccineList": [
        {
          "id": 1
        }
      ]
    }
  ```

    * **Success Response:**

        * **Code:** 201 CREATED  
          **Content:**
          ```json
            {
                        "id":1,
                        "name":"Ponpon",
                        "color":"maron",
                        "dateOfBirth": "2022-01-01 09:30",
                        "dateOfDeath": null,
                        "horn": 1,
                        "numberJob": "9845",
                        "numberPaper": "2859609845",
                        "foodList" : [
                          {
                            "id":1,
                            "name": "eau",
                            "foodType": "liquid",
                            "description": "il faut limiter la quantitée d'eau",
                            "unit": 
                              {
                                "id": 1,
                                "unitName": "centiliter"
                              }
                          },
                          {
                            "id": 2,
                            "name": "foin",
                            "foodType": "solid",
                            "description": "nourriture à volonté",
                            "unit": 
                              {
                                "id": 2,
                                "unitName": "kilogram"
                              }
                          }
                        ],
                        "raceList": [
                          { 
                            "id":1,
                            "name": "Brun des Alpes",
                            "description": "La race Brune, originaire de Suisse, d'où le nom de Brune des Alpes"
                          }
                        ],
                        "medicationList": [
                          {
                            "id":1,
                            "name": "parafilariose bovine",
                            "description": "maladie des sueurs de sang"
                          }
                        ],
                        "farm" : 
                          {
                            "id":1,
                            "siren" : null,
                            "siret" : null,
                            "address" : null,
                            "zipCode" : 0,
                            "city": null,
                            "personalList": []
                          },
                        "sickness": 
                          {
                            "id":1,
                            "startDate": "2016-05-01",
                            "endDate": "2016-05-01",
                            "diseaseName": "grippe",
                            "discardMilk": false,
                            "description" : "il va vite s'en remettre",
                            "medicationList": [
                              {
                                "id": 2,
                                "name": "pommade",
                                "description": "pommade pour soigner les cornes"
                              }
                            ]
                          },
                        "cattleVaccineList": []
                    }
          ```

* **Error Response:**

    * **Code:** 404 NOT FOUND  
      **Content:** `null`

* **Sample Call:**

  ```javascript
    fetch({
      url: "api/cattles/bulls/1",
      dataType: "json",
      type : "PUT",
      success : function(response) {
        console.log(response);
      }
    });
  ```

**Delete bull**
----
Delete json data about a single bull.

* **URL**

      api/cattles/bulls/:id

* **Method:**

  `DELETE`

* **URL Params**

  **Required:**

  `id=[Long]`

* **Data Params**

  None

* **Success Response:**

    * **Code:** 200 OK

* **Sample Call:**

  ```javascript
    fetch({
      url: "api/cattles/bulls/1",
      dataType: "json",
      type : "DELETE",
      success : function(response) {
        console.log(response);
      }
    });
  ```

**Update bull/farm**

Update json data about a single bull.

* **URL**

      api/cattles/bulls/{idBull}/farms/{idFarm}

* **Method:**

  `PUT`

* **URL Params**

  **Required:**

  `None`

* **Data Params**

  None

* **Success Response:**

    * **Code:** 200 OK

* **Error Response:**

    * **Code:** 404 NOT FOUND  
      **Content:** ` error : "Le bull n'a pas été modifié" `

* **Sample Call:**

  ```javascript
    fetch({
      url: "api/cattles/bulls/1/farms/1",
      dataType: "json",
      type : "DELETE",
      success : function(response) {
        console.log(response);
      }
    });
  ```

**Update bull/food**

Update json data about a single bull.

* **URL**

      api/cattles/bulls/{idBull}/foods/{idFood}

* **Method:**

  `PUT`

* **URL Params**

  **Required:**

  `None`

* **Data Params**

  None

* **Success Response:**

    * **Code:** 200 OK

* **Error Response:**

    * **Code:** 404 NOT FOUND  
      **Content:** ` error : "Le bull n'a pas été modifié" `

* **Sample Call:**

  ```javascript
    fetch({
      url: "api/cattles/bulls/1/foods/1",
      dataType: "json",
      type : "DELETE",
      success : function(response) {
        console.log(response);
      }
    });
  ```

**Update bull/sickness**

Update json data about a single bull.

* **URL**

      api/cattles/bulls/{idBull}/sickness/{idSickness}

* **Method:**

  `PUT`

* **URL Params**

  **Required:**

  `None`

* **Data Params**

  None

* **Success Response:**

    * **Code:** 200 OK

* **Error Response:**

    * **Code:** 404 NOT FOUND  
      **Content:** ` error : "Le bull n'a pas été modifié" `

* **Sample Call:**

  ```javascript
    fetch({
      url: "api/cattles/bulls/1/sickness/1",
      dataType: "json",
      type : "DELETE",
      success : function(response) {
        console.log(response);
      }
    });
  ```

**Update bull/medication**

Update json data about a single bull.

* **URL**

      api/cattles/bulls/{idBull}/medications/{idMedication}

* **Method:**

  `PUT`

* **URL Params**

  **Required:**

  `None`

* **Data Params**

  None

* **Success Response:**

    * **Code:** 200 OK

* **Error Response:**

    * **Code:** 404 NOT FOUND  
      **Content:** ` error : "Le bull n'a pas été modifié" `

* **Sample Call:**

  ```javascript
    fetch({
      url: "api/cattles/bulls/1/medications/1",
      dataType: "json",
      type : "DELETE",
      success : function(response) {
        console.log(response);
      }
    });
  ```

---


# Cow

**Get all cows**
----
Returns array json data.

* **URL**

      api/cattles/cows


* **url Params** : **Required**: None

    * **Data Params** : None

        * **Success Response:**

            * **Code:** 200 OK  
              **Content:**
          ```json
                  [
                      {
                          "id":1,
                          "name":"Ponpon",
                          "color":"maron",
                          "dateOfBirth": "2022-01-01 09:30",
                          "dateOfDeath": null,
                          "horn": 1,
                          "numberJob": "9845",
                          "numberPaper": "2859609845",
                          "foodList" : [
                            {
                              "id":1,
                              "name": "eau",
                              "foodType": "liquid",
                              "description": "il faut limiter la quantitée d'eau",
                              "unit": 
                                {
                                  "id": 1,
                                  "unitName": "centiliter"
                                }
                            },
                            {
                              "id": 2,
                              "name": "foin",
                              "foodType": "solid",
                              "description": "nourriture à volonté",
                              "unit": 
                                {
                                  "id": 2,
                                  "unitName": "kilogram"
                                }
                            }
                          ],
                          "raceList": [
                            { 
                              "id":1,
                              "name": "Brun des Alpes",
                              "description": "La race Brune, originaire de Suisse, d'où le nom de Brune des Alpes"
                            }
                          ],
                          "medicationList": [
                            {
                              "id":1,
                              "name": "parafilariose bovine",
                              "description": "maladie des sueurs de sang"
                            }
                          ],
                          "farm" : 
                            {
                              "id":1,
                              "siren" : "123456789",
                              "siret" : "12345678967546",
                              "address" : "3 rue de la promenade",
                              "zipCode" : 34567,
                              "city": "Saint-Bruno-De-Montarville",
                              "personalList": [
                                {
                                  "id": 1,
                                  "namePersonal": "Jules Bordenave",
                                  "city": "Saint-Bruno-De-Montarville",
                                  "address": "78 rue des champs",
                                  "zipCode": 34567,
                                  "passageDate": "2016-05-02 22:00",
                                  "passingReason": "le taureau a de la fièvre",
                                  "clinicalName": "chez Jules"
                              }
                             ]
                            },
                          "sickness": 
                            {
                              "id":1,
                              "startDate": "2016-05-01",
                              "endDate": "2016-05-01",
                              "diseaseName": "grippe",
                              "discardMilk": false,
                              "description" : "il va vite s'en remettre",
                              "medicationList": [
                                {
                                  "id": 2,
                                  "name": "pommade",
                                  "description": "pommade pour soigner les cornes"
                                }
                              ]
                            },
                          "cattleVaccineList": [
                            {
                            }
                          ]
                      },
                      {
                          "id":2,
                          "name":"Neron",
                          "color":"maron",
                          "dateOfBirth": "2022-01-01 09:30",
                          "dateOfDeath": null,
                          "horn": 1,
                          "numberJob": "4356",
                          "numberPaper": "2859604356",
                          "foodList" : [
                            {
                              "id":1,
                              "name": "eau",
                              "foodType": "liquid",
                              "description": "il faut limiter la quantitée d'eau",
                              "unit": 
                                {
                                  "id": 1,
                                  "unitName": "centiliter"
                                }
                            },
                            {
                              "id": 2,
                              "name": "foin",
                              "foodType": "solid",
                              "description": "nourriture à volonté",
                              "unit": 
                                {
                                  "id": 2,
                                  "unitName": "kilogram"
                                }
                            }
                          ],
                          "raceList": [
                            { 
                              "id":1,
                              "name": "Brun des Alpes",
                              "description": "La race Brune, originaire de Suisse, d'où le nom de Brune des Alpes"
                            }
                          ],
                          "medicationList": [
                            {
                              "id":1,
                              "name": "parafilariose bovine",
                              "description": "maladie des sueurs de sang"
                            }
                          ],
                          "farm" : 
                            {
                              "id":1,
                              "siren" : "123456789",
                              "siret" : "12345678967546",
                              "address" : "3 rue de la promenade",
                              "zipCode" : 34567,
                              "city": "Saint-Bruno-De-Montarville",
                              "personalList": [
                                {
                                  "id": 1,
                                  "namePersonal": "Jules Bordenave",
                                  "city": "Saint-Bruno-De-Montarville",
                                  "address": "78 rue des champs",
                                  "zipCode": 34567,
                                  "passageDate": "2016-05-02 22:00",
                                  "passingReason": "le taureau a de la fièvre",
                                  "clinicalName": "chez Jules"
                              }
                             ]
                            },
                          "sickness": 
                            {
                              "id":1,
                              "startDate": "2016-05-01",
                              "endDate": "2016-05-01",
                              "diseaseName": "grippe",
                              "discardMilk": false,
                              "description" : "il va vite s'en remettre",
                              "medicationList": [
                                {
                                  "id": 2,
                                  "name": "pommade",
                                  "description": "pommade pour soigner les cornes"
                                }
                              ]
                            },
                          "cattleVaccineList": [
                            {
                            }
                          ]
                      }
                  ]
          ```

* **Sample Call:**

  ```javascript
    fetch.ajax({
      url: "api/cattles/bulls",
      dataType: "json",
      type : "GET",
      success : function(response) {
        console.log(response);
      }
    });
  ```        

---

**Get bull by id**
----
Returns json data about a single bull.

* **URL**

      api/cattles/bulls/:id

* **Method:**

  `GET`

* **URL Params**

  **Required:**

  `id=[Long]`

* **Data Params**

  None

    * **Success Response:**

        * **Code:** 200  
          **Content:**
          ```json
                  {
                        "id":1,
                        "name":"Ponpon",
                        "color":"maron",
                        "dateOfBirth": "2022-01-01 09:30",
                        "dateOfDeath": null,
                        "horn": 1,
                        "numberJob": "9845",
                        "numberPaper": "2859609845",
                        "foodList" : [
                          {
                            "id":1,
                            "name": "eau",
                            "foodType": "liquid",
                            "description": "il faut limiter la quantitée d'eau",
                            "unit": 
                              {
                                "id": 1,
                                "unitName": "centiliter"
                              }
                          },
                          {
                            "id": 2,
                            "name": "foin",
                            "foodType": "solid",
                            "description": "nourriture à volonté",
                            "unit": 
                              {
                                "id": 2,
                                "unitName": "kilogram"
                              }
                          }
                        ],
                        "raceList": [
                          { 
                            "id":1,
                            "name": "Brun des Alpes",
                            "description": "La race Brune, originaire de Suisse, d'où le nom de Brune des Alpes"
                          }
                        ],
                        "medicationList": [
                          {
                            "id":1,
                            "name": "parafilariose bovine",
                            "description": "maladie des sueurs de sang"
                          }
                        ],
                        "farm" : 
                          {
                            "id":1,
                            "siren" : "123456789",
                            "siret" : "12345678967546",
                            "address" : "3 rue de la promenade",
                            "zipCode" : 34567,
                            "city": "Saint-Bruno-De-Montarville",
                            "personalList": [
                              {
                                "id": 1,
                                "namePersonal": "Jules Bordenave",
                                "city": "Saint-Bruno-De-Montarville",
                                "address": "78 rue des champs",
                                "zipCode": 34567,
                                "passageDate": "2016-05-02 22:00",
                                "passingReason": "le taureau a de la fièvre",
                                "clinicalName": "chez Jules"
                            }
                           ]
                          },
                        "sickness": 
                          {
                            "id":1,
                            "startDate": "2016-05-01",
                            "endDate": "2016-05-01",
                            "diseaseName": "grippe",
                            "discardMilk": false,
                            "description" : "il va vite s'en remettre",
                            "medicationList": [
                              {
                                "id": 2,
                                "name": "pommade",
                                "description": "pommade pour soigner les cornes"
                              }
                            ]
                          },
                        "cattleVaccineList": [
                          {
                          }
                        ]
                    }
          ```

* **Error Response:**

    * **Code:** 404 NOT FOUND  
      **Content:** `null`


* **Sample Call:**

  ```javascript
    fetch({
      url: "api/cattles/bulls/1",
      dataType: "json",
      type : "GET",
      success : function(response) {
        console.log(response);
      }
    });
  ```

**Add bull**
----
Save a single bull in the data base.

* **URL**

      api/cattles/bulls

* **Method:**

  `POST`

* **URL Params**

  None

* **Data Params**

  ```json
    {
      "name":"Caliban",
      "color":"maron",
      "dateOfBirth": "2022-01-01 09:30",
      "dateOfDeath": "",
      "horn": 1,
      "numberJob": "9445",
      "numberPaper": "2859609445",
      "foodList" : [
        {
          "id":1
        },
        {
          "id": 2
        }
      ],
      "raceList": [
        { 
          "id":1
        }
      ],
      "medicationList": [
        {
          "id":1
        }
      ],
      "farm" : 
        {
          "id":1
        },
      "sickness": 
        {
          "id":1
        },
      "cattleVaccineList": [
        {
          "id":1
        }
      ]
    }
  ```

    * **Success Response:**

        * **Code:** 201 CREATED  
          **Content:**
          ```json
            {
                    "id":3,
                    "name":"Caliban",
                    "color":"maron",
                    "dateOfBirth": "2022-01-01 09:30",
                    "dateOfDeath": "",
                    "horn": 1,
                    "numberJob": "9845",
                    "numberPaper": "2859609845",
                    "foodList" : [
                      {
                        "id":1,
                        "name": null,
                        "foodType": null,
                        "description": null,
                        "unit": null
                      },
                      {
                       "id": 2,
                       "name": null,
                       "foodType": null,
                       "description": null,
                       "unit": null
                      }
                    ],
                    "raceList": [
                      { 
                        "id":1,
                        "name": null,
                        "description": null
                      }
                    ],
                    "medicationList": [
                      {
                        "id":1,
                        "name": null,
                        "description": null
                      }
                    ],
                    "farm" : 
                      {
                       "id":1,
                       "siren" : null,
                       "siret" : null,
                       "address" : null,
                       "zipCode" : null,
                       "city": null,
                       "personalList": []
                     },
                     "sickness": 
                      {
                       "id":1,
                       "startDate": null,
                       "endDate": null,
                       "diseaseName": null,
                       "discardMilk": false,
                       "description" : null,
                       "medicationList": []
                     },
                    "cattleVaccineList": [
                      {
                          "id": 1,
                          "dateVaccine": null,
                          "cattle": null,
                          "vaccine": null
                      }
                     ]
                    }
          ```

* **Sample Call:**

  ```javascript
    fetch({
      url: "api/cattles/bulls",
      dataType: "json",
      type : "POST",
      success : function(response) {
        console.log(response);
      }
    });
  ```

**Update bull**
----
Update json data about a single bull.

* **URL**

      api/cattles/bulls/:id

* **Method:**

  ` PUT`

* **URL Params**

  **Required:**

  `id=[Long]`

* **Data Params**

  ```json
    {
      "name":"Ponpon",
      "color":"maron",
      "dateOfBirth": "2022-01-01 09:30",
      "dateOfDeath": "",
      "horn": 1,
      "numberJob": "9845",
      "numberPaper": "2859609845",
      "foodList" : [
        {
          "id":1
        },
        {
          "id": 2
        }
      ],
      "raceList": [
        { 
          "id":1
        }
      ],
      "medicationList": [
        {
          "id":1
        }
      ],
      "farm" : 
        {
          "id":1
        },
      "sickness": 
        {
          "id":1
        },
      "cattleVaccineList": [
        {
          "id": 1
        }
      ]
    }
  ```

    * **Success Response:**

        * **Code:** 201 CREATED  
          **Content:**
          ```json
            {
                        "id":1,
                        "name":"Ponpon",
                        "color":"maron",
                        "dateOfBirth": "2022-01-01 09:30",
                        "dateOfDeath": null,
                        "horn": 1,
                        "numberJob": "9845",
                        "numberPaper": "2859609845",
                        "foodList" : [
                          {
                            "id":1,
                            "name": "eau",
                            "foodType": "liquid",
                            "description": "il faut limiter la quantitée d'eau",
                            "unit": 
                              {
                                "id": 1,
                                "unitName": "centiliter"
                              }
                          },
                          {
                            "id": 2,
                            "name": "foin",
                            "foodType": "solid",
                            "description": "nourriture à volonté",
                            "unit": 
                              {
                                "id": 2,
                                "unitName": "kilogram"
                              }
                          }
                        ],
                        "raceList": [
                          { 
                            "id":1,
                            "name": "Brun des Alpes",
                            "description": "La race Brune, originaire de Suisse, d'où le nom de Brune des Alpes"
                          }
                        ],
                        "medicationList": [
                          {
                            "id":1,
                            "name": "parafilariose bovine",
                            "description": "maladie des sueurs de sang"
                          }
                        ],
                        "farm" : 
                          {
                            "id":1,
                            "siren" : null,
                            "siret" : null,
                            "address" : null,
                            "zipCode" : 0,
                            "city": null,
                            "personalList": []
                          },
                        "sickness": 
                          {
                            "id":1,
                            "startDate": "2016-05-01",
                            "endDate": "2016-05-01",
                            "diseaseName": "grippe",
                            "discardMilk": false,
                            "description" : "il va vite s'en remettre",
                            "medicationList": [
                              {
                                "id": 2,
                                "name": "pommade",
                                "description": "pommade pour soigner les cornes"
                              }
                            ]
                          },
                        "cattleVaccineList": []
                    }
          ```

* **Error Response:**

    * **Code:** 404 NOT FOUND  
      **Content:** `null`

* **Sample Call:**

  ```javascript
    fetch({
      url: "api/cattles/bulls/1",
      dataType: "json",
      type : "PUT",
      success : function(response) {
        console.log(response);
      }
    });
  ```

**Delete bull**
----
Delete json data about a single bull.

* **URL**

      api/cattles/bulls/:id

* **Method:**

  `DELETE`

* **URL Params**

  **Required:**

  `id=[Long]`

* **Data Params**

  None

* **Success Response:**

    * **Code:** 200 OK

* **Sample Call:**

  ```javascript
    fetch({
      url: "api/cattles/bulls/1",
      dataType: "json",
      type : "DELETE",
      success : function(response) {
        console.log(response);
      }
    });
  ```

**Update bull/farm**

Update json data about a single bull.

* **URL**

      api/cattles/bulls/{idBull}/farms/{idFarm}

* **Method:**

  `PUT`

* **URL Params**

  **Required:**

  `None`

* **Data Params**

  None

* **Success Response:**

    * **Code:** 200 OK

* **Error Response:**

    * **Code:** 404 NOT FOUND  
      **Content:** ` error : "Le bull n'a pas été modifié" `

* **Sample Call:**

  ```javascript
    fetch({
      url: "api/cattles/bulls/1/farms/1",
      dataType: "json",
      type : "DELETE",
      success : function(response) {
        console.log(response);
      }
    });
  ```

**Update bull/food**

Update json data about a single bull.

* **URL**

      api/cattles/bulls/{idBull}/foods/{idFood}

* **Method:**

  `PUT`

* **URL Params**

  **Required:**

  `None`

* **Data Params**

  None

* **Success Response:**

    * **Code:** 200 OK

* **Error Response:**

    * **Code:** 404 NOT FOUND  
      **Content:** ` error : "Le bull n'a pas été modifié" `

* **Sample Call:**

  ```javascript
    fetch({
      url: "api/cattles/bulls/1/foods/1",
      dataType: "json",
      type : "DELETE",
      success : function(response) {
        console.log(response);
      }
    });
  ```

**Update bull/sickness**

Update json data about a single bull.

* **URL**

      api/cattles/bulls/{idBull}/sickness/{idSickness}

* **Method:**

  `PUT`

* **URL Params**

  **Required:**

  `None`

* **Data Params**

  None

* **Success Response:**

    * **Code:** 200 OK

* **Error Response:**

    * **Code:** 404 NOT FOUND  
      **Content:** ` error : "Le bull n'a pas été modifié" `

* **Sample Call:**

  ```javascript
    fetch({
      url: "api/cattles/bulls/1/sickness/1",
      dataType: "json",
      type : "DELETE",
      success : function(response) {
        console.log(response);
      }
    });
  ```

**Update bull/medication**

Update json data about a single bull.

* **URL**

      api/cattles/bulls/{idBull}/medications/{idMedication}

* **Method:**

  `PUT`

* **URL Params**

  **Required:**

  `None`

* **Data Params**

  None

* **Success Response:**

    * **Code:** 200 OK

* **Error Response:**

    * **Code:** 404 NOT FOUND  
      **Content:** ` error : "Le bull n'a pas été modifié" `

* **Sample Call:**

  ```javascript
    fetch({
      url: "api/cattles/bulls/1/medications/1",
      dataType: "json",
      type : "DELETE",
      success : function(response) {
        console.log(response);
      }
    });
  ```

---
