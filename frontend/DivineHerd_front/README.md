# Divine Herd

Divine Herd permet de faire la gestion de l'élevage familiale

## Installation

Les étapes d'installation pour installer votre projet sur une machine locale.

1. Faire l'installation avec :

```
npm install
```

2. Si tout ne c'est pas installé faite les lignes de commande suivantes :

```
npm install react react-dom
npm install react-router-dom
npm i json-server
npm install --save react-router-dom
npm react-bootstrap
npm install jwt-decode
npm install react-bootstrap
```

## Utilisation

Les étapes pour utiliser votre projet. Par exemple:

1. Lancer le projet avec la commande :

```
npm run dev
```

2. Utiliser les formulaires pour saisir des données
3. Utiliser les boutons pour soumettre les données

## Contribuer

Les instructions pour les personnes qui souhaitent contribuer à votre projet. Par exemple:

1. Fork le dépôt
2. Créer une branche pour vos modifications (git checkout -b my-feature)
3. Commit vos modifications (git commit -am 'ajouté une fonctionnalité')
4. Pusher la branche (git push origin my-feature)
5. Créer une demande de tirage
