import React, {useState} from "react";
import JsonContext from "./JsonContext";
import jwtDecode from "jwt-decode";

export default function TokenContextProvider(props: { children: string | number | boolean | React.ReactElement<any, string | React.JSXElementConstructor<any>> | React.ReactFragment | React.ReactPortal | null | undefined; }) {

    // création de composant qui reçoit des props

    const [token, setToken] = useState(localStorage.getItem("TOKEN"));
    const [userId, setUserId] = useState<string>();

    const handleSetToken = (token: string) => {
        setToken(token);
        const decodedToken = jwtDecode(token) as { userId: string };
        setUserId(decodedToken.userId);
    };

    // on initialise un state qui ne survit pas au raffraichissement de la page
    // on lui dit de ce baser sur ce que contient le local storage
    // et d'en tenir compte

    return (
        // @ts-ignore
        <JsonContext.Provider value={{ token, userId, setToken: handleSetToken }}>
            {props.children}
        </JsonContext.Provider>
    );
}
