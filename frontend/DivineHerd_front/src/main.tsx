import React, {useState} from 'react'
import ReactDOM from 'react-dom/client'
import {
    createBrowserRouter,
    Link,
    Outlet,
    RouterProvider}
from "react-router-dom";

import './index.css'
import './assets/css/Navbar.css'
import "./assets/css/Login.css"
import "./assets/css/BtnGestion.css"
import "./assets/css/bull.css"

// Partie Context
import TokenContextProvider from "./context/TokenContextProvider";

//Navbar Page
import PageNavbar from "./pages/PageNavbar";

//Components Files
import Logout from "./components/logout/Logout";
import Cow from "./components/gestion/cattle/cow/Cow";
import Calf from "./components/gestion/cattle/calf/Calf";
import Bull from "./components/gestion/cattle/bull/Bull";
import LoginForm from "./components/login/LoginForm";
import RegistrationForm from "./components/registration/RegistrationForm";
import Veterinary from "./components/gestion/personal/veterinary/Veterinary";
import Technician from "./components/gestion/personal/Technician/Technician";
import CattleForm from "./components/gestion/cattle/CattleForm";
import BullForm from "./components/gestion/cattle/bull/BullForm";
import BullList from "./components/gestion/cattle/bull/BullList";
import CalfForm from "./components/gestion/cattle/calf/CalfForm";
import CalfList from "./components/gestion/cattle/calf/CalfList";
import CowList from "./components/gestion/cattle/cow/CowList";
import PersonalForm from "./components/gestion/personal/PersonalForm";
import VeterinaryForm from "./components/gestion/personal/veterinary/VeterinaryForm";
import VeterinaryList from "./components/gestion/personal/veterinary/VeterinaryList";
import TechnicianForm from "./components/gestion/personal/Technician/TechnicianForm";
import TechnicianList from "./components/gestion/personal/Technician/TechnicianList";
import CowForm from "./components/gestion/cattle/cow/CowForm";

// Page Files
import GestionGeneral from "./pages/GestionGeneral";
import PageGestionCattle from "./pages/PageGestionCattle";
import PageGestionPersonalExterne from "./pages/PageGestionPersonalExterne";


const HeaderLayout = () => (
    <>
        <header>
            <PageNavbar />
        </header>
        <Outlet />
    </>
);

const router = createBrowserRouter([
    {
        element: <HeaderLayout />,
        children: [
            {
                path: "/",
                element: <LoginForm />
            },
            {
                path: "/gestion",
                element: <GestionGeneral />
            },
            {
                path: "/gestionCattle",
                element: <PageGestionCattle />
            },
            {
                path: "/gestionPersonalExtern",
                element: <PageGestionPersonalExterne />
            },
            {
                path: "/cattleForm",
                element: <CattleForm />
            },
            {
                path: "/cow/:id",
                element: <Cow />
            },
            {
                path: "/cowForm",
                element: <CowForm />
            },
            {
                path: "/cowList",
                element: <CowList />
            },
            {
                path: "/calf/:id",
                element: <Calf />
            },
            {
                path: "/calfForm",
                element: <CalfForm />
            },
            {
                path: "/calfList",
                element: <CalfList />
            },
            {
                path: "/bull/:id",
                element: <Bull />
            },
            {
                path: "/bullForm",
                element: <BullForm />
            },
            {
                path: "/bullList",
                element: <BullList />
            },
            {
                path: "/personalForm",
                element: <PersonalForm />
            },
            {
                path: "/veterinary/:id",
                element: <Veterinary />
            },
            {
                path: "/veterinaryForm",
                element: <VeterinaryForm />
            },
            {
                path: "/veterinaryList",
                element: <VeterinaryList />
            },
            {
                path: "/technician/:id",
                element: <Technician />
            },
            {
                path: "/technicianForm",
                element: <TechnicianForm />
            },
            {
                path: "/technicianList",
                element: <TechnicianList />
            },
            {
                path: "/registration",
                element: <RegistrationForm />
            },
            {
                path: "/logout",
                element: <Logout/>
            }
        ]

    }
])


ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <React.StrictMode>
      <TokenContextProvider>
    <RouterProvider router={router} />
      </TokenContextProvider>
  </React.StrictMode>,
)
