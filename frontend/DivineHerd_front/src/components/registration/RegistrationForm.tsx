import React, {useState} from 'react';
import axios from "axios";
import {useNavigate} from "react-router-dom";
import {Button, Col, Form} from "react-bootstrap";

export default function RegistrationForm() {

    const [firstName, setFirstName] = useState('')
    const [lastName, setLastName] = useState('')
    const [dateOfBirth, setDateOfBirth] = useState('')
    const [password, setPassword] = useState('')
    const [telephone, setTelephone] = useState('')
    const [email, setEmail] = useState('')
    const [address, setAddress] = useState('')
    const [zipCode, setZipCode] = useState('')
    const [city, setCity] = useState('')

    const navigate = useNavigate()

    function handleSubmit(event: { preventDefault: () => void; }) {
        event.preventDefault();

        const { VITE_SERVER_ADDRESS} = import.meta.env;

        if (firstName && lastName && dateOfBirth && telephone && email && address && zipCode && city) {
            axios
                .post(`${VITE_SERVER_ADDRESS}/api/users`, {
                    firstName,
                    lastName,
                    dateOfBirth,
                    password,
                    telephone,
                    email,
                    address,
                    zipCode,
                    city
                })
                .then((response) => {
                    navigate("/");
                })
                .catch((error) => {
                    if (error?.response?.status == 401) {
                        alert("Unauthorized access");
                    } else {
                        console.error(error);
                    }
                });
        } else {
            alert("Merci de vous inscrire")
            navigate("/registration")
        }
    }

    return (
        <Form className="app" id="stripe-login" method="POST" onSubmit={handleSubmit}>
            <Form.Group as={Col} md={6} controlId="formFirstName">
                <Form.Label>Prénom:&nbsp;</Form.Label>
                <Form.Control
                    type="string"
                    name="prenom"
                    placeholder="votre prenom"
                    value={firstName}
                    onChange={(event) => setFirstName(event.target.value)}
                />
            </Form.Group>
            <Form.Group as={Col} md={6} controlId="formLastName">
                <Form.Label>Nom:&nbsp;</Form.Label>
                <Form.Control
                    type="string"
                    name="nom"
                    placeholder="votre nom"
                    value={lastName}
                    onChange={(event) => setLastName(event.target.value)}
                />
            </Form.Group>
            <Form.Group as={Col} md={6} controlId="formDateOfBirth">
                <Form.Label>Date de naissance:&nbsp;</Form.Label>
                <Form.Control
                    type="date"
                    name="date_naissance"
                    placeholder="votre date de naissance"
                    value={dateOfBirth}
                    onChange={(event) => setDateOfBirth(event.target.value)}
                />
            </Form.Group>
            <Form.Group as={Col} md={6} controlId="formPassword">
                <Form.Label>Mot de passe:&nbsp;</Form.Label>
                <Form.Control
                    type="password"
                    name="password"
                    placeholder="*******"
                    value={password}
                    onChange={(event) => setPassword(event.target.value)}
                />
            </Form.Group>
            <Form.Group as={Col} md={6} controlId="formPhone">
                <Form.Label>Telephone:&nbsp;</Form.Label>
                <Form.Control
                    type="tel"
                    name="phone"
                    value={telephone}
                    onChange={(event) => setTelephone(event.target.value)}
                />
            </Form.Group>
            <Form.Group as={Col} md={6} controlId="formEmail">
                <Form.Label>Email:&nbsp;</Form.Label>
                <Form.Control
                    type="email"
                    name="email"
                    placeholder="user@example.com"
                    value={email}
                    onChange={(event) => setEmail(event.target.value)}
                />
            </Form.Group>
            <Form.Group as={Col} md={6} controlId="formAddress">
                <Form.Label>Adresse:&nbsp;</Form.Label>
                <Form.Control
                    type="string"
                    name="address"
                    value={address}
                    onChange={(event) => setAddress(event.target.value)}
                />
            </Form.Group>
            <Form.Group as={Col} md={6} controlId="formZipCode">
                <Form.Label>Code Postal:&nbsp;</Form.Label>
                <Form.Control
                    type="sting"
                    name="zipCode"
                    value={zipCode}
                    onChange={(event) => setZipCode(event.target.value)}
                />
            </Form.Group>
            <Form.Group as={Col} md={6} controlId="formCity">
                <Form.Label>Ville:&nbsp;</Form.Label>
                <Form.Control
                    type="string"
                    name="city"
                    value={city}
                    onChange={(event) => setCity(event.target.value)}
                />
            </Form.Group>

            <Button variant="primary" type="submit">
                S'inscrire
            </Button>
        </Form>
    )
}
