import React, {useState, useContext} from "react";
import axios from "axios";
import {useNavigate } from "react-router-dom"
import {Form, Button, Container, Row, Col} from "react-bootstrap";
import JsonContext from "../../context/JsonContext";
import jwtDecode, {JwtPayload} from "jwt-decode";

export default function LoginForm() {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const navigate = useNavigate()

    // @ts-ignore
    const {setToken, userId} = useContext(JsonContext);

    function handleSubmit(event: { preventDefault: () => void; }) {
        event.preventDefault();

        const { VITE_SERVER_ADDRESS} = import.meta.env;

        if (email && password) {
            axios
                .post(`${VITE_SERVER_ADDRESS}/api/users/login`, {
                    email,
                    password,
                })
                .then((response) => {
                    localStorage.setItem("TOKEN", response.data);
                    setToken(response.data) // j'enregistre le token dans le context de l'application

                    const decodedToken = jwtDecode(response.data) as { user_id: string };
                    const userId = decodedToken.user_id;

                    navigate("/gestion");
                })
                .catch((error) => {
                    if (error?.response?.status == 401) {
                        alert("Unauthorized access");
                    } else {
                        console.error(error);
                    }
                });
        } else {
            alert("Merci de vous connectez")
            navigate("/login")
        }
    }

    return (
        <Form className="app" id="stripe-login" method="POST" onSubmit={handleSubmit}>
            <Form.Group as={Col} md={6} controlId="formEmail">
                <Form.Label>Email:&nbsp;</Form.Label>
                <Form.Control
                    type="email"
                    name="email"
                    placeholder="user@example.com"
                    value={email}
                    onChange={(event) => setEmail(event.target.value)}
                />
            </Form.Group>

            <Form.Group as={Col} md={6} controlId="formPassword">
                <Form.Label>Password:&nbsp;</Form.Label>
                <Form.Control
                    type="password"
                    name="password"
                    placeholder="***********"
                    value={password}
                    onChange={(event) => setPassword(event.target.value)}
                />
            </Form.Group>

            <Button variant="primary" type="submit">
                Se connecter
            </Button>
        </Form>
    );
}