import React, {useContext, useEffect, useState} from "react";
import {useNavigate} from "react-router-dom";
import JsonContext from "../../../../context/JsonContext";
import axios from "axios";
import {Table} from "react-bootstrap";

interface Technician {
    id: number;
    namePersonal: string;
    city: string;
    address: string;
    zipCode: string;
    passageDate: boolean;
    passingReason: string;
    dairyName: string;
}

export default function TechnicianList() {

    const navigate = useNavigate();

    // On utilise le contexte pour accéder au token
    // @ts-ignore
    const {token} = useContext(JsonContext);

    const [technicians, setTechnicians] = useState<Technician[]>([]);

    useEffect(() => {
        // Si le token n'existe pas, l'utilisateur n'est pas connecté, on le redirige vers la page de connexion
        if (!token) {
            navigate("/");
            return;
        }

        const {VITE_SERVER_ADDRESS} = import.meta.env;

        axios
            .get(`${VITE_SERVER_ADDRESS}/api/personals/dairyTechnicians`, {
                headers: {Authorization: `Bearer ${token}`}, // On envoie le token dans les headers de la requête
            })
            .then((response) => {
                setTechnicians(response.data);
            })
            .catch((error) => {
                if (error?.response?.status === 401) {
                    alert("Unauthorized access");
                    navigate("/");
                } else {
                    console.error(error);
                }
            });
    }, [token, navigate]);

    const handleRowClick = (id: number) => {
        navigate(`/technician/${id}`);
    };

    return (
        <div className="TechnicianList">
            <h2>Liste des technicien Laitier :</h2>
            <Table striped bordered hover>
                <thead>
                <tr>
                    <th>Nom du Technicien</th>
                    <th>ville</th>
                    <th>Addresse</th>
                    <th>Code Postal</th>
                    <th>Date de passage</th>
                    <th>Raison du passage</th>
                    <th>Nom de la laiterie</th>
                </tr>
                </thead>
                <tbody>
                {technicians.map((technician) => (
                    <tr key={technician.id} onClick={() => handleRowClick(technician.id)}>
                        <td>{technician.namePersonal}</td>
                        <td>{technician.city}</td>
                        <td>{technician.address}</td>
                        <td>{technician.zipCode}</td>
                        <td>{technician.passageDate}</td>
                        <td>{technician.passingReason}</td>
                        <td>{technician.dairyName}</td>
                    </tr>
                ))}
                </tbody>
            </Table>
        </div>
    )
}