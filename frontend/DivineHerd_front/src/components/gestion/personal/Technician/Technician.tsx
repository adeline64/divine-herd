import React, {useContext, useEffect, useState} from "react";
import axios from "axios";
import {useNavigate, useParams} from "react-router-dom"
import JsonContext from "../../../../context/JsonContext";
import {Card, Col, Container, ListGroup, Row} from "react-bootstrap";

interface Technician {
    id: number;
    namePersonal: string;
    city: string;
    address: string;
    zipCode: string;
    passageDate: boolean;
    passingReason: string;
    dairyName: string;
}

export default function Technician() {

    const { id } = useParams<{ id: string }>();
    const navigate = useNavigate();

    // On utilise le contexte pour accéder au token
    // @ts-ignore
    const { token } = useContext(JsonContext);

    const [technicians, setTechnicians] = useState<Technician>();

    useEffect(() => {
        // Si le token n'existe pas, l'utilisateur n'est pas connecté, on le redirige vers la page de connexion
        if (!token) {
            navigate("/");
            return;
        }

        const { VITE_SERVER_ADDRESS } = import.meta.env;

        axios
            .get(`${VITE_SERVER_ADDRESS}/api/personals/dairyTechnicians/${id}`,
                {
                    headers: { Authorization: `Bearer ${token}` },
                    // On envoie le token dans les headers de la requête
                })
            .then((response) => {
                setTechnicians(response.data);
            })
            .catch((error) => {
                if (error?.response?.status === 401) {
                    alert("Unauthorized access");
                    navigate("/");
                } else {
                    console.error(error);
                }
            });
    }, [token, navigate, id]);

    if (!technicians) {
        return <div>Loading...</div>;
    }

    return (
        <Container className="mt-5">
            <Row>
                <Col>
                    <Card>
                        <Card.Header>
                            <Card.Title as="h2">{technicians.namePersonal}</Card.Title>
                        </Card.Header>
                        <Card.Body>
                            <ListGroup variant="flush">
                                <ListGroup.Item>
                                    <strong>Ville : </strong> {technicians.city}
                                </ListGroup.Item>
                                <ListGroup.Item>
                                    <strong>Adresse : </strong> {technicians.address}
                                </ListGroup.Item>
                                <ListGroup.Item>
                                    <strong>Code Postal : </strong> {technicians.zipCode}
                                </ListGroup.Item>
                                <ListGroup.Item>
                                    <strong>Date de passage : </strong> {technicians.passageDate}
                                </ListGroup.Item>
                                <ListGroup.Item>
                                    <strong>Raison de passage : </strong> {technicians.passingReason}
                                </ListGroup.Item>
                                <ListGroup.Item>
                                    <strong>Nom de laiterie : </strong> {technicians.dairyName}
                                </ListGroup.Item>
                            </ListGroup>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>
    );
}