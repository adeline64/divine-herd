import React, {useContext, useState} from 'react';
import axios from "axios";
import {useNavigate} from "react-router-dom";
import {Button, Col, Form} from "react-bootstrap";
import JsonContext from "../../../../context/JsonContext";


export default function TechnicianForm() {

    const [namePersonal, setNamePersonal] = useState('')
    const [city, setCity] = useState('')
    const [address, setAddress] = useState('')
    const [zipCode, setZipCode] = useState('')
    const [passageDate, setPassageDate] = useState('')
    const [passingReason, setPassingReason] = useState('')
    const [dairyName, setDairyName] = useState('')

    const navigate = useNavigate()
    // @ts-ignore
    const {token, setToken} = useContext(JsonContext);

    function handleSubmit(event: { preventDefault: () => void; }) {
        event.preventDefault();

        const { VITE_SERVER_ADDRESS} = import.meta.env;

        if (namePersonal && city && address && zipCode && passageDate && passingReason) {
            axios
                .post(`${VITE_SERVER_ADDRESS}/api/personals/dairyTechnicians`, {
                    namePersonal: namePersonal,
                    city: city,
                    address: address,
                    zipCode: zipCode,
                    passageDate: passageDate,
                    passingReason: passingReason,
                    dairyName: dairyName
                }, {
                    headers: {
                        Authorization: `Bearer ${token}`,
                    },
                })
                .then((response) => {
                    navigate("/technicianList");
                })
                .catch((error) => {
                    console.log(error)
                    if (error?.response?.status === 401) {
                        alert("Unauthorized access");
                    } else {
                        console.error(error);
                    }
                });
        } else {
            alert("Merci de remplir tous les champs");
            navigate("/technicianForm")
        }
    }

    return (
        <Form className="app" id="stripe-login" method="POST" onSubmit={handleSubmit}>
            <Form.Group as={Col} md={6} controlId="formNamePersonal">
                <Form.Label>Nom du personnel :&nbsp;</Form.Label>
                <Form.Control
                    type="string"
                    name="namePersonal"
                    value={namePersonal}
                    onChange={(event) => setNamePersonal(event.target.value)}
                />
            </Form.Group>
            <Form.Group as={Col} md={6} controlId="formCity">
                <Form.Label>Ville:&nbsp;</Form.Label>
                <Form.Control
                    type="string"
                    name="city"
                    value={city}
                    onChange={(event) => setCity(event.target.value)}
                />
            </Form.Group>
            <Form.Group as={Col} md={6} controlId="formAddress">
                <Form.Label>Adresse:&nbsp;</Form.Label>
                <Form.Control
                    type="string"
                    name="address"
                    value={address}
                    onChange={(event) => setAddress(event.target.value)}
                />
            </Form.Group>
            <Form.Group as={Col} md={6} controlId="formDateOfDeath">
                <Form.Label>Code Postal:&nbsp;</Form.Label>
                <Form.Control
                    type="string"
                    name="zipCode"
                    value={zipCode}
                    onChange={(event) => setZipCode(event.target.value)}
                />
            </Form.Group>
            <Form.Group as={Col} md={6} controlId="formDateOfDeath">
                <Form.Label>Date de passage:&nbsp;</Form.Label>
                <Form.Control
                    type="datetime-local"
                    name="passageDate"
                    value={passageDate}
                    onChange={(event) => setPassageDate(event.target.value)}
                />
            </Form.Group>
            <Form.Group as={Col} md={6} controlId="formPassingReason">
                <Form.Label>Numero de travail:&nbsp;</Form.Label>
                <Form.Control
                    type="string"
                    name="passingReason"
                    value={passingReason}
                    onChange={(event) => setPassingReason(event.target.value)}
                />
            </Form.Group>
            <Form.Group as={Col} md={6} controlId="formDairyName">
                <Form.Label>Raison:&nbsp;</Form.Label>
                <Form.Control
                    type="string"
                    name="dairyName"
                    value={dairyName}
                    onChange={(event) => setDairyName(event.target.value)}
                />
            </Form.Group>

            <Button variant="primary" type="submit">
                Inscrire
            </Button>
        </Form>

    );
}
