import React, {useState} from "react";
import {Link} from "react-router-dom";
import {Button, Container} from "react-bootstrap";

export default function PersonalForm() {



    return (
        <Container className="container_btn">
            <div className="d-flex justify-content-center btn">
                <Link to="/technicianForm">
                    <Button className="btn_link" variant="light">Formulaire technicien</Button>
                </Link>
                <Link to="/veterinaryForm">
                    <Button className="btn_link" variant="light">Formulaire vétérinaire</Button>
                </Link>
            </div>
        </Container>

    );
}
