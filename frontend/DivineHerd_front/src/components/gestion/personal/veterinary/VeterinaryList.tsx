import React, {useContext, useEffect, useState} from "react";
import {useNavigate} from "react-router-dom";
import JsonContext from "../../../../context/JsonContext";
import axios from "axios";
import {Table} from "react-bootstrap";

interface Veterinary {
    id: number;
    namePersonal: string;
    city: string;
    address: string;
    zipCode: string;
    passageDate: boolean;
    passingReason: string;
    clinicName: string;
}

export default function VeterinaryList() {
    const navigate = useNavigate();

    // On utilise le contexte pour accéder au token
    // @ts-ignore
    const {token} = useContext(JsonContext);

    const [veterinaries, setVeterinaries] = useState<Veterinary[]>([]);

    useEffect(() => {
        // Si le token n'existe pas, l'utilisateur n'est pas connecté, on le redirige vers la page de connexion
        if (!token) {
            navigate("/");
            return;
        }

        const {VITE_SERVER_ADDRESS} = import.meta.env;

        axios
            .get(`${VITE_SERVER_ADDRESS}/api/personals/veterinaries`, {
                headers: {Authorization: `Bearer ${token}`}, // On envoie le token dans les headers de la requête
            })
            .then((response) => {
                setVeterinaries(response.data);
            })
            .catch((error) => {
                if (error?.response?.status === 401) {
                    alert("Unauthorized access");
                    navigate("/");
                } else {
                    console.error(error);
                }
            });
    }, [token, navigate]);

    const handleRowClick = (id: number) => {
        navigate(`/veterinary/${id}`);
    };

    return (
        <div className="VeterinaryList">
            <h2>Liste des Veterinaires :</h2>
            <Table striped bordered hover>
                <thead>
                <tr>
                    <th>Nom du veterinaire</th>
                    <th>ville</th>
                    <th>Addresse</th>
                    <th>Code Postal</th>
                    <th>Date de passage</th>
                    <th>Raison du passage</th>
                    <th>Nom de la clinique</th>
                </tr>
                </thead>
                <tbody>
                {veterinaries.map((veterinary) => (
                    <tr key={veterinary.id} onClick={() => handleRowClick(veterinary.id)}>
                        <td>{veterinary.namePersonal}</td>
                        <td>{veterinary.city}</td>
                        <td>{veterinary.address}</td>
                        <td>{veterinary.zipCode}</td>
                        <td>{veterinary.passageDate}</td>
                        <td>{veterinary.passingReason}</td>
                        <td>{veterinary.clinicName}</td>
                    </tr>
                ))}
                </tbody>
            </Table>
        </div>
    )
}