import React, {useState} from "react";
import {Link} from "react-router-dom";
import {Button, Container} from "react-bootstrap";

export default function CattleForm() {


    return (
        <Container className="container_btn">
            <div className="d-flex justify-content-center btn">
                <Link to="/bullForm">
                    <Button className="btn_link" variant="light">Formulaire teaureau</Button>
                </Link>
                <Link to="/calfForm">
                    <Button className="btn_link" variant="light">Formulaire veau</Button>
                </Link>
                <Link to="/cowForm">
                    <Button className="btn_link" variant="light">Formulaire vache</Button>
                </Link>
            </div>
        </Container>

    );
}
