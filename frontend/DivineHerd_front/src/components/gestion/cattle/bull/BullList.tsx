import React, {useContext, useEffect, useState} from "react";
import {useNavigate} from "react-router-dom";
import axios from "axios";
import { Table } from 'react-bootstrap';
import JsonContext from "../../../../context/JsonContext";

interface Bull {
    id: number;
    name: string;
    color: string;
    dateOfBirth: string;
    dateOfDeath: string;
    horn: boolean;
    numberJob: string;
    numberPaper: string;
}

export default function BullList() {

    const navigate = useNavigate();

    // On utilise le contexte pour accéder au token
    // @ts-ignore
    const { token } = useContext(JsonContext);

    const [bulls, setBulls] = useState<Bull[]>([]);

    useEffect(() => {
        // Si le token n'existe pas, l'utilisateur n'est pas connecté, on le redirige vers la page de connexion
        if (!token) {
            navigate("/");
            return;
        }

        const { VITE_SERVER_ADDRESS } = import.meta.env;

        axios
            .get(`${VITE_SERVER_ADDRESS}/api/cattles/bulls`, {
                headers: { Authorization: `Bearer ${token}` }, // On envoie le token dans les headers de la requête
            })
            .then((response) => {
                setBulls(response.data);
            })
            .catch((error) => {
                if (error?.response?.status === 401) {
                    alert("Unauthorized access");
                    navigate("/");
                } else {
                    console.error(error);
                }
            });
    }, [token, navigate]);

    const handleRowClick = (id: number) => {
        navigate(`/bull/${id}`);
    };

    return (
        <div className="BullList">
            <h2>Liste des taureaux :</h2>
            <Table striped bordered hover>
                <thead>
                <tr>
                    <th>Nom</th>
                    <th>Couleur</th>
                    <th>Date de naissance</th>
                    <th>Date de décès</th>
                    <th>Cornes</th>
                    <th>Numéro de travail</th>
                    <th>Numéro de papier</th>
                </tr>
                </thead>
                <tbody>
                {bulls.map((bull) => (
                    <tr key={bull.id} onClick={() => handleRowClick(bull.id)}>
                        <td>{bull.name}</td>
                        <td>{bull.color}</td>
                        <td>{bull.dateOfBirth}</td>
                        <td>{bull.dateOfDeath}</td>
                        <td>{bull.horn ? 'Oui' : 'Non'}</td>
                        <td>{bull.numberJob}</td>
                        <td>{bull.numberPaper}</td>
                    </tr>
                ))}
                </tbody>
            </Table>
        </div>
    );
}
