import React, { useContext, useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import axios from "axios";
import { Container, Row, Col, Card, ListGroup } from "react-bootstrap";
import JsonContext from "../../../../context/JsonContext";

interface Bull {
    id: number;
    name: string;
    color: string;
    dateOfBirth: string;
    dateOfDeath: string;
    horn: boolean;
    numberJob: string;
    numberPaper: string;
}

export default function Bull() {

    const { id } = useParams<{ id: string }>();
    const navigate = useNavigate();

    // On utilise le contexte pour accéder au token
    // @ts-ignore
    const { token } = useContext(JsonContext);

    const [bull, setBull] = useState<Bull>();

    useEffect(() => {
        // Si le token n'existe pas, l'utilisateur n'est pas connecté, on le redirige vers la page de connexion
        if (!token) {
            navigate("/");
            return;
        }

        const { VITE_SERVER_ADDRESS } = import.meta.env;

        axios
            .get(`${VITE_SERVER_ADDRESS}/api/cattles/bulls/${id}`,
                {
                    headers: { Authorization: `Bearer ${token}` },
                    // On envoie le token dans les headers de la requête
                })
            .then((response) => {
                setBull(response.data);
            })
            .catch((error) => {
                if (error?.response?.status === 401) {
                    alert("Unauthorized access");
                    navigate("/");
                } else {
                    console.error(error);
                }
            });
    }, [token, navigate, id]);

    if (!bull) {
        return <div>Loading...</div>;
    }

    return (
        <Container className="mt-5">
            <Row>
                <Col>
                    <Card>
                        <Card.Header>
                            <Card.Title as="h2">{bull.name}</Card.Title>
                        </Card.Header>
                        <Card.Body>
                            <ListGroup variant="flush">
                                <ListGroup.Item>
                                    <strong>Couleur : </strong> {bull.color}
                                </ListGroup.Item>
                                <ListGroup.Item>
                                    <strong>Date de naissance : </strong> {bull.dateOfBirth}
                                </ListGroup.Item>
                                <ListGroup.Item>
                                    <strong>Date de décès : </strong> {bull.dateOfDeath}
                                </ListGroup.Item>
                                <ListGroup.Item>
                                    <strong>Cornes : </strong>{" "}
                                    {bull.horn ? "Oui" : "Non"}
                                </ListGroup.Item>
                                <ListGroup.Item>
                                    <strong>Numéro de travail : </strong> {bull.numberJob}
                                </ListGroup.Item>
                                <ListGroup.Item>
                                    <strong>Numéro de papier : </strong> {bull.numberPaper}
                                </ListGroup.Item>
                            </ListGroup>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>
    );
}