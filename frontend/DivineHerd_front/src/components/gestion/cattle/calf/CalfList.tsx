import React, {useContext, useEffect, useState} from "react";
import {useNavigate} from "react-router-dom";
import JsonContext from "../../../../context/JsonContext";
import axios from "axios";
import {Table} from "react-bootstrap";

enum calfStatusEnum {
    HEIFER = "HEIFER",
    CALVES = "CALVES",
}

interface Calf {
    id: number;
    name: string;
    color: string;
    dateOfBirth: string;
    dateOfDeath: string;
    horn: boolean;
    numberJob: string;
    numberPaper: string;
    calfStatus: calfStatusEnum;
}

export default function CalfList() {

// LISTE DES VEAU

    const navigate = useNavigate();

    // On utilise le contexte pour accéder au token
    // @ts-ignore
    const { token } = useContext(JsonContext);

    const [calfs, setCalfs] = useState<Calf[]>([]);

    useEffect(() => {
        // Si le token n'existe pas, l'utilisateur n'est pas connecté, on le redirige vers la page de connexion
        if (!token) {
            navigate("/");
            return;
        }

        const { VITE_SERVER_ADDRESS } = import.meta.env;

        axios
            .get(`${VITE_SERVER_ADDRESS}/api/cattles/calfs`, {
                headers: { Authorization: `Bearer ${token}` }, // On envoie le token dans les headers de la requête
            })
            .then((response) => {
                setCalfs(response.data);
            })
            .catch((error) => {
                if (error?.response?.status === 401) {
                    alert("Unauthorized access");
                    navigate("/");
                } else {
                    console.error(error);
                }
            });
    }, [token, navigate]);

    const handleRowClick = (id: number) => {
        navigate(`/calf/${id}`);
    };

    return (
        <div className="CalfList">
            <h2>Liste des Veaux :</h2>
            <Table striped bordered hover>
                <thead>
                <tr>
                    <th>Nom</th>
                    <th>Couleur</th>
                    <th>Date de naissance</th>
                    <th>Date de décès</th>
                    <th>Cornes</th>
                    <th>Numéro de travail</th>
                    <th>Numéro de papier</th>
                    <th>Sexe</th>
                </tr>
                </thead>
                <tbody>
                {calfs.map((calf) => (
                    <tr key={calf.id} onClick={() => handleRowClick(calf.id)}>
                        <td>{calf.name}</td>
                        <td>{calf.color}</td>
                        <td>{calf.dateOfBirth}</td>
                        <td>{calf.dateOfDeath ?? "-"}</td>
                        <td>{calf.horn ? 'Oui' : 'Non'}</td>
                        <td>{calf.numberJob}</td>
                        <td>{calf.numberPaper}</td>
                        <td>{calf.calfStatus}</td>
                    </tr>
                ))}
                </tbody>
            </Table>
        </div>
    )
}
