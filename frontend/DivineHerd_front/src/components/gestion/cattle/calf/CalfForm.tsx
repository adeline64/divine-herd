import React, {useContext, useState} from "react";
import {useNavigate} from "react-router-dom";
import JsonContext from "../../../../context/JsonContext";
import axios from "axios";
import {Button, Col, Form} from "react-bootstrap";

// Définition de l'énumération calfStatus
enum calfStatusEnum {
    HEIFER = "HEIFER",
    CALVES = "CALVES",
}

export default function CalfForm() {

// FORUMULAIRE INSCRIPTION VEAU
    const [name, setName] = useState('')
    const [color, setColor] = useState('')
    const [dateOfBirth, setDateOfBirth] = useState('')
    const [dateOfDeath, setDateOfDeath] = useState('')
    const [horn, setHorn] = useState<boolean>(false);
    const [numberJob, setNumberJob] = useState('')
    const [numberPaper, setNumberPaper] = useState('')
    const [calfStatus, setCalfStatus] = useState<calfStatusEnum>(calfStatusEnum.HEIFER);

    const navigate = useNavigate()

    // @ts-ignore
    const {token, setToken} = useContext(JsonContext);

    function handleSubmit(event: { preventDefault: () => void; }) {
        event.preventDefault();

        const { VITE_SERVER_ADDRESS} = import.meta.env;

        if (name && color && dateOfBirth && numberJob && numberPaper) {
            axios
                .post(`${VITE_SERVER_ADDRESS}/api/cattles/calfs`, {
                    name: name,
                    color: color,
                    dateOfBirth: dateOfBirth,
                    dateOfDeath: dateOfDeath,
                    horn: horn,
                    numberJob: numberJob,
                    numberPaper: numberPaper,
                    calfStatus: calfStatus
                }, {
                    headers: {
                        Authorization: `Bearer ${token}`,
                    },
                })
                .then((response) => {
                    navigate("/calfList");
                })
                .catch((error) => {
                    console.log(error)
                    if (error?.response?.status === 401) {
                        alert("Unauthorized access");
                    } else {
                        console.error(error);
                    }
                });
        } else {
            alert("Merci de remplir tous les champs");
            navigate("/calfForm")
        }
    }

    return (
        <Form className="app" id="stripe-login" method="POST" onSubmit={handleSubmit}>
            <Form.Group as={Col} md={6} controlId="formName">
                <Form.Label>Nom:&nbsp;</Form.Label>
                <Form.Control
                    type="string"
                    name="name"
                    placeholder="Nom du veau"
                    value={name}
                    onChange={(event) => setName(event.target.value)}
                />
            </Form.Group>
            <Form.Group as={Col} md={6} controlId="formLastName">
                <Form.Label>Couleur:&nbsp;</Form.Label>
                <Form.Control
                    type="string"
                    name="color"
                    placeholder="Couleur du veau"
                    value={color}
                    onChange={(event) => setColor(event.target.value)}
                />
            </Form.Group>
            <Form.Group as={Col} md={6} controlId="formDateOfBirth">
                <Form.Label>Date de naissance:&nbsp;</Form.Label>
                <Form.Control
                    type="datetime-local"
                    name="dateOfBirth"
                    placeholder="date de naissance"
                    value={dateOfBirth}
                    onChange={(event) => setDateOfBirth(event.target.value)}
                />
            </Form.Group>
            <Form.Group as={Col} md={6} controlId="formDateOfDeath">
                <Form.Label>Date de mort:&nbsp;</Form.Label>
                <Form.Control
                    type="datetime-local"
                    name="dateOfDeath"
                    placeholder="date de mort"
                    value={dateOfDeath}
                    onChange={(event) => setDateOfDeath(event.target.value)}
                />
            </Form.Group>
            <Form.Group as={Col} md={6} controlId="formHorn">
                <Form.Label>Corne:&nbsp;</Form.Label>
                <div>
                    <Form.Check
                        type="radio"
                        id="horn-oui"
                        label="oui"
                        value={"1"}
                        checked={horn===true}
                        onChange={(event) => setHorn(true)}
                    />
                    <Form.Check
                        type="radio"
                        id="horn-non"
                        label="non"
                        value={"0"}
                        checked={horn===false}
                        onChange={(event) => setHorn(false)}
                    />
                </div>
            </Form.Group>
            <Form.Group as={Col} md={6} controlId="formNumberJob">
                <Form.Label>Numero de travail:&nbsp;</Form.Label>
                <Form.Control
                    type="string"
                    name="numberJob"
                    placeholder="remplir le numéro à 4 chiffres"
                    value={numberJob}
                    onChange={(event) => setNumberJob(event.target.value)}
                />
            </Form.Group>
            <Form.Group as={Col} md={6} controlId="formNumberPaper">
                <Form.Label>Numero sur papier:&nbsp;</Form.Label>
                <Form.Control
                    type="string"
                    name="numberPaper"
                    placeholder="remplir le numéro à 10 chiffres"
                    value={numberPaper}
                    onChange={(event) => setNumberPaper(event.target.value)}
                />
            </Form.Group>
            <Form.Group as={Col} md={6} controlId="formNumberPaper">
                <Form.Label>Sexe:&nbsp;</Form.Label>
                <Form.Control as="select" name="calfStatus" value={calfStatus} onChange={(event) => setCalfStatus(event.target.value as calfStatusEnum)}>
                    <option value={calfStatusEnum.HEIFER}>Femelle</option>
                    <option value={calfStatusEnum.CALVES}>Mâle</option>
                </Form.Control>
            </Form.Group>

            <Button variant="primary" type="submit">
                Inscrire
            </Button>
        </Form>
    )
}
