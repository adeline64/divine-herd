import React, {useContext, useEffect, useState} from "react";
import axios from "axios";
import {useNavigate, useParams} from "react-router-dom"
import JsonContext from "../../../../context/JsonContext";
import {Card, Col, Container, ListGroup, Row} from "react-bootstrap";

interface Calf {
    id: number;
    name: string;
    color: string;
    dateOfBirth: string;
    dateOfDeath: string;
    horn: boolean;
    numberJob: string;
    numberPaper: string;
    //calfStatus :
}

export default function Calf() {

// FICHE DU VEAU

    const { id } = useParams<{ id: string }>();
    const navigate = useNavigate();

    // On utilise le contexte pour accéder au token
    // @ts-ignore
    const { token } = useContext(JsonContext);

    const [calf, setCalf] = useState<Calf>();

    useEffect(() => {
        // Si le token n'existe pas, l'utilisateur n'est pas connecté, on le redirige vers la page de connexion
        if (!token) {
            navigate("/");
            return;
        }

        const { VITE_SERVER_ADDRESS } = import.meta.env;

        axios
            .get(`${VITE_SERVER_ADDRESS}/api/cattles/calfs/${id}`,
                {
                    headers: { Authorization: `Bearer ${token}` },
                    // On envoie le token dans les headers de la requête
                })
            .then((response) => {
                setCalf(response.data);
            })
            .catch((error) => {
                if (error?.response?.status === 401) {
                    alert("Unauthorized access");
                    navigate("/");
                } else {
                    console.error(error);
                }
            });
    }, [token, navigate, id]);

    if (!calf) {
        return <div>Loading...</div>;
    }

    return (
        <Container className="mt-5">
            <Row>
                <Col>
                    <Card>
                        <Card.Header>
                            <Card.Title as="h2">{calf.name}</Card.Title>
                        </Card.Header>
                        <Card.Body>
                            <ListGroup variant="flush">
                                <ListGroup.Item>
                                    <strong>Couleur : </strong> {calf.color}
                                </ListGroup.Item>
                                <ListGroup.Item>
                                    <strong>Date de naissance : </strong> {calf.dateOfBirth}
                                </ListGroup.Item>
                                <ListGroup.Item>
                                    <strong>Date de décès : </strong> {calf.dateOfDeath}
                                </ListGroup.Item>
                                <ListGroup.Item>
                                    <strong>Cornes : </strong>{" "}
                                    {calf.horn ? "Oui" : "Non"}
                                </ListGroup.Item>
                                <ListGroup.Item>
                                    <strong>Numéro de travail : </strong> {calf.numberJob}
                                </ListGroup.Item>
                                <ListGroup.Item>
                                    <strong>Numéro de papier : </strong> {calf.numberPaper}
                                </ListGroup.Item>
                            </ListGroup>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>
    );
}
