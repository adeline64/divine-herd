import React, {useContext, useState} from "react";
import {useNavigate} from "react-router-dom";
import JsonContext from "../../../../context/JsonContext";
import axios from "axios";
import {Button, Col, Form} from "react-bootstrap";

export default function CowForm() {

// FORMULAIRE INSCRIPTION VACHE
    const [name, setName] = useState('')
    const [color, setColor] = useState('')
    const [dateOfBirth, setDateOfBirth] = useState('')
    const [dateOfDeath, setDateOfDeath] = useState('')
    const [horn, setHorn] = useState<boolean>(false);
    const [numberJob, setNumberJob] = useState('')
    const [numberPaper, setNumberPaper] = useState('')
    const [numberOfDeadCalf, setNumberOfDeadCalf] = useState(0)
    const [numberOfCalfBorn, setNumberOfCalfBorn] = useState(0)

    const navigate = useNavigate()

    // @ts-ignore
    const {token, setToken} = useContext(JsonContext);

    const handleInputChangeDead = (event: React.ChangeEvent<HTMLInputElement>) => {
        const parsedValue = parseInt(event.target.value, 10); // Convertit en nombre entier
        setNumberOfDeadCalf(parsedValue);
    };

    const handleInputChangeBorn = (event: React.ChangeEvent<HTMLInputElement>) => {
        const parsedValue = parseInt(event.target.value, 10); // Convertit en nombre entier
        setNumberOfCalfBorn(parsedValue);
    };

    function handleSubmit(event: { preventDefault: () => void; }) {
        event.preventDefault();

        const { VITE_SERVER_ADDRESS} = import.meta.env;

        if (name && color && dateOfBirth && numberJob && numberPaper) {
            axios
                .post(`${VITE_SERVER_ADDRESS}/api/cattles/cows`, {
                    name: name,
                    color: color,
                    dateOfBirth: dateOfBirth,
                    dateOfDeath: dateOfDeath,
                    horn: horn,
                    numberJob: numberJob,
                    numberPaper: numberPaper,
                    numberOfDeadCalf: numberOfDeadCalf,
                    numberOfCalfBorn: numberOfCalfBorn
                }, {
                    headers: {
                        Authorization: `Bearer ${token}`,
                    },
                })
                .then((response) => {
                    navigate("/cowList");
                })
                .catch((error) => {
                    console.log(error)
                    if (error?.response?.status === 401) {
                        alert("Unauthorized access");
                    } else {
                        console.error(error);
                    }
                });
        } else {
            alert("Merci de remplir tous les champs");
            navigate("/cowForm")
        }
    }

    return (
        <Form className="app" id="stripe-login" method="POST" onSubmit={handleSubmit}>
            <Form.Group as={Col} md={6} controlId="formName">
                <Form.Label>Nom:&nbsp;</Form.Label>
                <Form.Control
                    type="string"
                    name="name"
                    placeholder="Nom de la vache"
                    value={name}
                    onChange={(event) => setName(event.target.value)}
                />
            </Form.Group>
            <Form.Group as={Col} md={6} controlId="formLastName">
                <Form.Label>Couleur:&nbsp;</Form.Label>
                <Form.Control
                    type="string"
                    name="color"
                    placeholder="Couleur de la vache"
                    value={color}
                    onChange={(event) => setColor(event.target.value)}
                />
            </Form.Group>
            <Form.Group as={Col} md={6} controlId="formDateOfBirth">
                <Form.Label>Date de naissance:&nbsp;</Form.Label>
                <Form.Control
                    type="datetime-local"
                    name="dateOfBirth"
                    placeholder="date de naissance"
                    value={dateOfBirth}
                    onChange={(event) => setDateOfBirth(event.target.value)}
                />
            </Form.Group>
            <Form.Group as={Col} md={6} controlId="formDateOfDeath">
                <Form.Label>Date de mort:&nbsp;</Form.Label>
                <Form.Control
                    type="datetime-local"
                    name="dateOfDeath"
                    placeholder="date de mort"
                    value={dateOfDeath}
                    onChange={(event) => setDateOfDeath(event.target.value)}
                />
            </Form.Group>
            <Form.Group as={Col} md={6} controlId="formHorn">
                <Form.Label>Corne:&nbsp;</Form.Label>
                <div>
                    <Form.Check
                        type="radio"
                        id="horn-oui"
                        label="oui"
                        value={"1"}
                        checked={horn===true}
                        onChange={(event) => setHorn(true)}
                    />
                    <Form.Check
                        type="radio"
                        id="horn-non"
                        label="non"
                        value={"0"}
                        checked={horn===false}
                        onChange={(event) => setHorn(false)}
                    />
                </div>
            </Form.Group>
            <Form.Group as={Col} md={6} controlId="formNumberJob">
                <Form.Label>Numero de travail:&nbsp;</Form.Label>
                <Form.Control
                    type="string"
                    name="numberJob"
                    placeholder="remplir le numéro à 4 chiffres"
                    value={numberJob}
                    onChange={(event) => setNumberJob(event.target.value)}
                />
            </Form.Group>
            <Form.Group as={Col} md={6} controlId="formNumberPaper">
                <Form.Label>Numero sur papier:&nbsp;</Form.Label>
                <Form.Control
                    type="string"
                    name="numberPaper"
                    placeholder="remplir le numéro à 10 chiffres"
                    value={numberPaper}
                    onChange={(event) => setNumberPaper(event.target.value)}
                />
            </Form.Group>
            <Form.Group as={Col} md={6} controlId="formNumberOfDeadCalf">
                <Form.Label>Nombre de veau mort:&nbsp;</Form.Label>
                <Form.Control
                    type="int"
                    name="numberOfDeadCalf"
                    value={numberOfDeadCalf}
                    onChange={handleInputChangeDead}
                />
            </Form.Group>
            <Form.Group as={Col} md={6} controlId="formNumberOfCalfBorn">
                <Form.Label>Numero de veau né:&nbsp;</Form.Label>
                <Form.Control
                    type="int"
                    name="numberOfCalfBorn"
                    value={numberOfCalfBorn}
                    onChange={handleInputChangeBorn}
                />
            </Form.Group>

            <Button variant="primary" type="submit">
                Inscrire
            </Button>
        </Form>

    );
}
