import React, {useContext, useEffect, useState} from "react";
import {useNavigate} from "react-router-dom";
import JsonContext from "../../../../context/JsonContext";
import axios from "axios";
import {Table} from "react-bootstrap";

interface Cow {
    id: number;
    name: string;
    color: string;
    dateOfBirth: string;
    dateOfDeath?: string;
    horn: boolean;
    numberJob: string;
    numberPaper: string;
    numberOfDeadCalf: number;
    numberOfCalfBorn: number;
}

export default function CowList() {

// LISTE DES VACHES

    const navigate = useNavigate();

    // On utilise le contexte pour accéder au token
    // @ts-ignore
    const { token } = useContext(JsonContext);

    const [cows, setCows] = useState<Cow[]>([]);

    useEffect(() => {
        // Si le token n'existe pas, l'utilisateur n'est pas connecté, on le redirige vers la page de connexion
        if (!token) {
            navigate("/");
            return;
        }

        const { VITE_SERVER_ADDRESS } = import.meta.env;

        axios
            .get(`${VITE_SERVER_ADDRESS}/api/cattles/cows`, {
                headers: { Authorization: `Bearer ${token}` }, // On envoie le token dans les headers de la requête
            })
            .then((response) => {
                setCows(response.data);
            })
            .catch((error) => {
                if (error?.response?.status === 401) {
                    alert("Unauthorized access");
                    navigate("/");
                } else {
                    console.error(error);
                }
            });
    }, [token, navigate]);

    const handleRowClick = (id: number) => {
        navigate(`/cow/${id}`);
    };

    return (
        <div className="BullList">
            <h2>Liste des Vaches :</h2>
            <Table striped bordered hover>
                <thead>
                <tr>
                    <th>Nom</th>
                    <th>Couleur</th>
                    <th>Date de naissance</th>
                    <th>Date de décès</th>
                    <th>Cornes</th>
                    <th>Numéro de travail</th>
                    <th>Numéro de papier</th>
                    <th>Nombre de veau mort</th>
                    <th>Nombre de veau née</th>
                </tr>
                </thead>
                <tbody>
                {cows.map((cow) => (
                    <tr key={cow.id} onClick={() => handleRowClick(cow.id)}>
                        <td>{cow.name}</td>
                        <td>{cow.color}</td>
                        <td>{cow.dateOfBirth}</td>
                        <td>{cow.dateOfDeath ?? "-"}</td>
                        <td>{cow.horn ? 'Oui' : 'Non'}</td>
                        <td>{cow.numberJob}</td>
                        <td>{cow.numberPaper}</td>
                        <td>{cow.numberOfDeadCalf}</td>
                        <td>{cow.numberOfCalfBorn}</td>
                    </tr>
                ))}
                </tbody>
            </Table>
        </div>
    );
}
