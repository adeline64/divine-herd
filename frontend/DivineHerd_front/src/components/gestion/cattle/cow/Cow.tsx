import React, {useContext, useEffect, useState} from "react";
import axios from "axios";
import {useNavigate, useParams} from "react-router-dom"
import JsonContext from "../../../../context/JsonContext";
import {Card, Col, Container, ListGroup, Row} from "react-bootstrap";

interface Cow {
    id: number;
    name: string;
    color: string;
    dateOfBirth: string;
    dateOfDeath?: string;
    horn: boolean;
    numberJob: string;
    numberPaper: string;
    numberOfDeadCalf: number;
    numberOfCalfBorn: number;
}

export default function Cow() {

// DESCRIPTION VACHE

    const { id } = useParams<{ id: string }>();
    const navigate = useNavigate();

    // On utilise le contexte pour accéder au token
    // @ts-ignore
    const { token } = useContext(JsonContext);

    const [cow, setCow] = useState<Cow>();

    useEffect(() => {
        // Si le token n'existe pas, l'utilisateur n'est pas connecté, on le redirige vers la page de connexion
        if (!token) {
            navigate("/");
            return;
        }

        const { VITE_SERVER_ADDRESS } = import.meta.env;

        axios
            .get(`${VITE_SERVER_ADDRESS}/api/cattles/cows/${id}`,
                {
                    headers: { Authorization: `Bearer ${token}` },
                    // On envoie le token dans les headers de la requête
                })
            .then((response) => {
                setCow(response.data);
            })
            .catch((error) => {
                if (error?.response?.status === 401) {
                    alert("Unauthorized access");
                    navigate("/");
                } else {
                    console.error(error);
                }
            });
    }, [token, navigate, id]);

    if (!cow) {
        return <div>Loading...</div>;
    }

    return (
        <Container className="mt-5">
            <Row>
                <Col>
                    <Card>
                        <Card.Header>
                            <Card.Title as="h2">{cow.name}</Card.Title>
                        </Card.Header>
                        <Card.Body>
                            <ListGroup variant="flush">
                                <ListGroup.Item>
                                    <strong>Couleur : </strong> {cow.color}
                                </ListGroup.Item>
                                <ListGroup.Item>
                                    <strong>Date de naissance : </strong> {cow.dateOfBirth}
                                </ListGroup.Item>
                                <ListGroup.Item>
                                    <strong>Date de décès : </strong> {cow.dateOfDeath}
                                </ListGroup.Item>
                                <ListGroup.Item>
                                    <strong>Cornes : </strong>{" "}
                                    {cow.horn ? "Oui" : "Non"}
                                </ListGroup.Item>
                                <ListGroup.Item>
                                    <strong>Numéro de travail : </strong> {cow.numberJob}
                                </ListGroup.Item>
                                <ListGroup.Item>
                                    <strong>Numéro de papier : </strong> {cow.numberPaper}
                                </ListGroup.Item>
                                <ListGroup.Item>
                                    <strong>Nombre de veau mort : </strong> {cow.numberOfDeadCalf}
                                </ListGroup.Item>
                                <ListGroup.Item>
                                    <strong>Nombre de veau née : </strong> {cow.numberOfCalfBorn}
                                </ListGroup.Item>
                            </ListGroup>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>
    );
}
