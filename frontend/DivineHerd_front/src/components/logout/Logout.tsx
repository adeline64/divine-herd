import React from 'react';
import {useNavigate} from "react-router-dom";
export default function Logout() {

    const navigate = useNavigate();

    function handleSubmit () {
        localStorage.removeItem("TOKEN");
        navigate("/")
    }


    return (
        <div className= "Logout" onClick={handleSubmit}>

        </div>
    )
}
