import React, { useEffect, useState } from "react";
import {Link} from "react-router-dom";
import {Button, Container} from "react-bootstrap";

export default function PageGestionPersonalExterne() {

    return (
        <Container className="container_btn">
            <div className="d-flex justify-content-center btn">
                <Link to="/veterinary">
                    <Button className="btn_link" variant="light">Vétérinaire</Button>
                </Link>
                <Link to="/technician">
                    <Button className="btn_link" variant="light">Technicien laiterie</Button>
                </Link>
                <Link to="/personalForm">
                    <Button className="btn_link" variant="light">Formulaire</Button>
                </Link>
            </div>
        </Container>
    );
}
