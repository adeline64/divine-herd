import React, {useContext, useEffect, useState} from "react";
import { Link, useNavigate } from "react-router-dom";
import {Dropdown, Nav, Navbar, NavDropdown} from "react-bootstrap";
import logo from "../assets/image/logo.png"
import JsonContext from "../context/JsonContext";

// @ts-ignore
export default function PageNavbar() {

    let navigate = useNavigate()

    // @ts-ignore
    const { token, setToken } = useContext(JsonContext);

    function logout() {
        localStorage.removeItem("TOKEN");
        setToken(null);
        navigate("/");
    }

    return (

        <Navbar className="nav" expand="lg" bg="dark" variant="dark">
            <Navbar.Brand href="#home">
                <img src={logo} alt='logo' className="logo" />
            </Navbar.Brand>
            <Navbar.Toggle aria-controls="responsive-navbar-nav" />
            {token ? (
            <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className="ml-auto">
                        <NavDropdown title="Boeuf" id="collasible-nav-dropdown">
                            <NavDropdown.Item>
                                <Link to="/cowList" className="nav-link">
                                    Vache
                                </Link>
                            </NavDropdown.Item>
                            <NavDropdown.Item>
                                <Link to="/calfList" className="nav-link">
                                    Veau
                                </Link>
                            </NavDropdown.Item>
                            <NavDropdown.Item>
                                <Link to="/bullList" className="nav-link">
                                    Taureau
                                </Link>
                            </NavDropdown.Item>
                            <NavDropdown.Item>
                                <Link to="/cattleForm" className="nav-link">
                                    Formulaires
                                </Link>
                            </NavDropdown.Item>
                            <NavDropdown.Divider />
                        </NavDropdown>
                    </Nav>
                <Nav className="ml-auto">
                    <NavDropdown title="Personnel externe" id="collasible-nav-dropdown">
                        <NavDropdown.Item>
                            <Link to="/veterinaryList" className="nav-link">
                                Veterinaire
                            </Link>
                        </NavDropdown.Item>
                        <NavDropdown.Item>
                            <Link to="/technicianList" className="nav-link">
                                Technicien Laitier
                            </Link>
                        </NavDropdown.Item>
                        <NavDropdown.Item>
                            <Link to="/personalForm" className="nav-link">
                                Formulaires
                            </Link>
                        </NavDropdown.Item>
                        <NavDropdown.Divider />
                    </NavDropdown>
                </Nav>
                    <Link to="/" className="nav-link" onClick={logout}>
                        <span>Déconnexion</span>
                    </Link>
            </Navbar.Collapse>
                    ) : (
            <Navbar.Collapse id="responsive-navbar-nav">
                    <Link to="/" className="nav-link">
                        <span>Connexion</span>
                    </Link>
                    <Link to="/registration" className="nav-link">
                        <span>Inscription</span>
                    </Link>
            </Navbar.Collapse>
            )}
        </Navbar>
    );
}