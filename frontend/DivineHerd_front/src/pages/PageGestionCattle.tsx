import React, { useEffect, useState } from "react";
import {Link} from "react-router-dom";
import {Button, Container} from "react-bootstrap";

export default function PageGestionCattle() {

    return (
        <Container className="container_btn">
            <div className="d-flex justify-content-center btn">
                <Link to="/cowList">
                    <Button className="btn_link" variant="light">Vaches</Button>
                </Link>
                <Link to="/calfList">
                    <Button className="btn_link" variant="light">Veau</Button>
                </Link>
                <Link to="/bullList">
                    <Button className="btn_link" variant="light">Taureau</Button>
                </Link>
                <Link to="/cattleForm">
                    <Button className="btn_link" variant="light">Formulaire</Button>
                </Link>
            </div>
        </Container>
    );
}
