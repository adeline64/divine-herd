import React from "react";
import { Link } from "react-router-dom";
import { Button, Container } from "react-bootstrap";

export default function GestionGeneral() {
    return (
        <Container className="container_btn">
            <div className="d-flex justify-content-center btn">
                <Link to="/gestionCattle">
                    <Button className="btn_link" variant="light">Gestion des bovins</Button>
                </Link>
                <Link to="/gestionPersonalExtern">
                    <Button className="btn_link" variant="light">Gestion du personnel externe</Button>
                </Link>
            </div>
        </Container>
    );
}